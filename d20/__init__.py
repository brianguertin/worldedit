from sgl.core import path

path.add(path.dirname(__file__))

def add_module(p):
	from sgl.core import path
	
	p = path.find(p)
	path.add(p)
	try:
		p = path.find('powers.xml')
		if p:
			from d20.powers import Power
			Power.load_objects('powers.xml')
		
		p = path.find('weapons.xml')
		if p:
			from d20.weapons import Weapon
			Weapon.load_objects('weapons.xml')
			
		p = path.find('classes.xml')
		if p:
			from d20.classes import Class
			Class.load_objects('classes.xml')
			
	finally:
		path.pop()

def setup():
	path.add_user_dir('d20')
	try:
		mods = path.listdir('mods')
	except:
		mods = []
	for m in mods:
		add_module(m)
		
	# Only allow one call
	# FIXME: allow multiple calls, either clear and reload mods, or load only new mods
	global setup
	setup = lambda:None
