from d20.dice import Dice
from sgl.core.cache import StaticCache

class Weapon(StaticCache):
	
	def __str__(self):
		return self.name
			
	def init(self):
		self.name = ''
		self.category = ''
		self.group = ''
		self.prof = 0
		self.range = 0
		self.hands = 0
		self.weight = 0.0
		self.price = 0
		self.dice = Dice(0)
		self.ranged = False
		
	def load(self, x):
		x.query(self, 'name')
		x.query(self, 'category')
		x.query(self, 'group')
		x.query(self, 'prof')
		x.query(self, 'range')
		x.query(self, 'hands')
		x.query(self, 'weight')
		x.query(self, 'price')
		
		d = x.get('damage')
		if d:
			self.dice = Dice(d)
			
		self.ranged = self.group in ['crossbow', 'sling', 'bow'] # or self.range > 0
		
		if self.ranged:
			self.dice.mod = 'dex'
		else:
			self.dice.mod = 'str'
