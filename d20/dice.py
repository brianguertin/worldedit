import random

class _Dice:
	def __init__(self, dice, mod = '', bonus = 0):
		self.rolls = 0
		self.sides = 0
		self.weapon = 0
		self.bonus = bonus
		self.mod = mod # Bonus based on stat
		
		for die in str(dice).split('+'):
			if die.find('d') >= 0:
				rolls, sep, sides = die.partition('d')
				self.rolls = int(rolls or 1)
				self.sides = int(sides)
			elif die.endswith('w'):
				self.weapon += int(die[:-1])
			elif die in ['str','dex','con','int','wis','cha']:
				self.mod = self.mod or die
			else:
				self.bonus += int(die)
			
	def __repr__(self):
		r = []
		if self.weapon:
			r.append('%sw' % self.weapon)
		if self.rolls:
			r.append('%sd%s' % (self.rolls, self.sides))
		if self.bonus:
			r.append('%s' % self.bonus)
		if self.mod:
			r.append('%s' % self.mod)
		return '+'.join(r)
		
	def roll(self, char=None):
		sum = self.bonus
		for i in range(self.rolls):
			sum += random.randint(1, self.sides)
		if char:
			if self.weapon:
				w = char.default_weapon
				if w:
					for i in range(self.weapon):
						sum += w.dice.roll(char)
			if self.mod:
				sum += char.mod(self.mod)
		
		try:	
			log('%s (%s)' % (sum, self), type='dice')
		except NameError:
			pass
			
		return sum
		
def Dice(dmg, mod='', bonus=0):
	if hasattr(dmg, 'roll'):
		return dmg
	else:
		return _Dice(dmg, mod=mod, bonus=bonus)
