from d20.dice import Dice

class Encounter(object):
	""" Tracks initiative, organizes rounds """
	# TODO: surprise rounds
	
	def __init__(self, chars):
		self.round = None
		self.init = {}
		for c in chars:
			self.init[c] = Dice('d20').roll()
		self.sort()
		
	def _cmp_init(self, c1, c2):
		return -cmp(self.init[c1], self.init[c2])
		
	def sort(self):
		self.chars = sorted(self.init.keys(), self._cmp_init)
		
	def add_character(self, c):
		self.init[c] = Dice('d20').roll()
		self.sort()
		# Placed in current round?
		
	def remove_character(self, c):
		self.chars.remove(c)
		del self.init[c]
			
	def __iter__(self):
		while True:
			self.round = Round(self.chars)
			yield self.round
			
	@property
	def rounds(self):
		return iter(self)
		
	@property
	def turns(self):
		for round in self.rounds:
			for turn in round.turns:
				yield turn

class Round(object):
	""" Organizes character actions into individual turns """
	
	def __init__(self, chars):
		self.chars = chars
		
	def __iter__(self):
		for c in self.chars:
			yield Turn(c)
			
	@property
	def turns(self):
		return iter(self)

class Turn(object):
	""" Keeps track of actions taken/possible this turn for a specific creature """
	
	def __init__(self, actor):
		self.actor = actor
		self.moveActionTaken = False
		self.standardActionTaken = False
		self.minorActionTaken = False
		self.isWalking = False
		self.distanceMoved = 0
		self.freeActionsTaken = 0
		
	def action_taken(self, type_):
		return getattr(self, '%sActionTaken' % type_)
		
	def take_action(self, type_):
		return getattr(self, 'take_%s_action' % type_)()
		
	def take_standard_action(self):
		if not self.standardActionTaken:
			self.isWalking = False
			self.standardActionTaken = True
		else:
			raise Exception('Standard action already taken')
			
	def take_minor_action(self):
		if not self.minorActionTaken:
			self.isWalking = False
			self.minorActionTaken = True
		else:
			raise Exception('Minor action already taken')
			
	def take_move_action(self):
		if not self.moveActionTaken:
			self.moveActionTaken = True
		else:
			raise Exception('Minor action already taken')
			
	def take_free_action(self):
		self.freeActionsTaken += 1
		
	def walk(self, dist = 1):
		if not self.moveActionTaken:
			self.moveActionTaken = True
			self.isWalking = True
		if self.can_walk(dist):
			self.distanceMoved += dist
		else:
			raise Exception('Cannot walk %s spaces at this time (distance moved: %s, speed: %s)' % (dist, self.distanceMoved, self.actor.speed))

	def can_walk(self, dist = 1):
		return (self.isWalking or not self.moveActionTaken) and self.distanceMoved + dist <= self.actor.speed
