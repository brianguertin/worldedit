import sgl.core.path as path
import xml.etree.ElementTree as xml
from d20.powers import Power
from sgl.core.cache import StaticCache, CacheError

class Class(StaticCache):
	
	def init(self):
		self.name = ''
		
		# Health
		self.hp_first_level = 0
		self.hp_per_level = 0
		self.healing_surges = 0
		
		# Defenses
		self.fortitude = 0
		self.reflex = 0
		self.will = 0
		
		self.power = ''
		self.skill = ''
		
		self.powers = {} # Indexed by level
		self.feats = []
		self.skills = []
		
	def load(self, x):
		x.query(self, 'name')
		x.query(self, 'hp_first_level')
		x.query(self, 'hp_per_level')
		x.query(self, 'healing_surges')
		
		x.query(self, 'power')
		x.query(self, 'skill')
		
		xDef = x.find('defense')
		if xDef is not None:
			xDef.query(self, 'fortitude')
			xDef.query(self, 'reflex')
			xDef.query(self, 'will')
		
		for xSkill in x.findall('skill'):
			t = xSkill.text
			if t:
				self.skills.append(t)
				
		for xPowers in x.findall('powers'):
			lvl = int(xPowers.get('level'))
			if lvl not in self.powers:
				self.powers[lvl] = []
			for xPower in xPowers.findall('power'):
				t = xPower.text
				if t:
					try:
						self.powers[lvl].append(Power(t))
					except CacheError:
						pass
					except Exception, e:
						log('Loading %s: %s' % (t,e), type='d20.Power')
					
		for xFeat in x.findall('feat'):
			t = xFeat.text
			if t:
				self.feats.append(t)
			
		
