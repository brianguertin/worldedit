import sgl.core.path as path
from sgl.core.cache import Cache

class Trinket(object):
	__new__ = Cache(('d20', 'trinkets')).new
			
	def init(self):
		self.name = ''
		
	def load(self, x):
		x.query(self, 'name')
