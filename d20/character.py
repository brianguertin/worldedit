from d20.inventory import Inventory
from d20.classes import Class
from d20.dice import Dice
from d20.signals import onDamage, onHeal
from d20.powers import Power
from sgl.core import xml

class _Meta(object):pass

class CharacterBase(object):
	def __init__(self):
		self._first_class_applied = False
		self.name = ''
		self.level = 0
		self.classes = {} # Class and level
		self.powers = []
		self.feats = []
		
		self.hp = 0
		self.healing_surges = 0
		
		self.speed = 6
		
		self.stats = {
			'str':8,
			'dex':8,
			'con':8,
			'int':8,
			'wis':8,
			'cha':8,
		}
		
		self.saves = {
			'ac':10,
			'reflex':10,
			'fortitude':10,
			'will':10,
		}
		
	def __getstate__(self):
		d = self.__dict__.copy()
		d['powers'] = [obj.name for obj in d['powers']]
		return d
		
	def __setstate__(self, st):
		self.__dict__.update(st)
		self.powers = [Power(obj) for obj in self.powers]
		
	"""
	def __str__(self):
		return self.name
		
	def __repr__(self):
		return '<%s: %s>' % (self.__class__.name, self.name)
	"""
		
	def load(self, x):
		x.query(self, 'name')
		
		xAbil = x.find('abilities')
		if xAbil is not None:
			for k in self.stats.keys():
				try:
					self.stats[k] = int(xAbil.get(k))
				except:
					pass
					
		for xPath in x.findall('path'):
			cls = xPath.get('class')
			lvl = xPath.get('level')
			if cls and lvl:
				self.apply_class(cls, lvl)
				
		self.level = sum(self.classes.values())
		
		#log('Loaded %s: %s' % (self.name, self.__dict__))
		
	def apply_class(self, cls, lvl):
		lvl = int(lvl)
		if lvl < 1:
			return
		self.classes[cls] = lvl
		
		cls = Class(cls)
		
		# Apply base values if not set
		if not self._first_class_applied:
			self._first_class_applied = True
			self.hp = cls.hp_first_level + cls.hp_per_level * (lvl - 1)
			self.healing_surges = cls.healing_surges
		else:
			# Otherwise just add HP
			self.hp += cls.hp_per_level * lvl
			
		# Add Defenses
		self.saves['fortitude'] += cls.fortitude
		self.saves['reflex'] += cls.reflex
		self.saves['will'] += cls.will
		
		# Add Feats
		self.feats = list(set(self.feats + cls.feats))
		
		# Add free powers
		if 0 in cls.powers:
			self.powers = list(set(self.powers + cls.powers[0]))
			
		# FIXME: remove this, only here for testing
		for lvl, powers in cls.powers.items():
			if lvl == 0:
				continue
			self.powers = list(set(self.powers + powers))

		
	def save(self, x):
		pass
		
	def calculate_stats(self):
		# TODO: calculate all possible stats based on current class levels (ex. saves, free feats, free powers)
		pass
		
	def validate(self):
		# TODO: ensure that chose abilities, feats, powers, and stats are valid for this level/class
		pass
		
	def mod(self, attr):
		return int((self.stats[attr.lower()] - 10) // 2)

class Character(object):
	def __init__(self, filename = None):
		self.base = CharacterBase()
		self.inventory = Inventory()
		self.meta = _Meta()
		
		self.reset()
		
		if filename:
			self.load(filename)
		
	def load(self, x):
		x = xml.read(x)
		self.base = CharacterBase()
		self.inventory = Inventory()
		
		self.base.load(x)
		
		xEquip = x.find('equipment')
		if xEquip is not None:
			self.inventory.load(xEquip)
			
		self.reset()
		
	def reset(self):
		self.hp = self.base.hp
		self.temp_hp = 0
		self.healing_surges = self.base.healing_surges
		
		self.saves = self.base.saves.copy()
		
		self.powers = self.base.powers
		self.powers.sort()
		self.feats = self.base.feats
		
		self.speed = self.base.speed
		
		
		# FIXME: some of this can be moved up into Base
		
		# Apply stat-based modifiers
		self.hp += self.base.stats['con']
		self.healing_surges += self.mod('con')
		
		# FIXME: AC should depend on armor
		self.saves['ac'] += max(self.mod('dex'), self.mod('int'))
		
		self.saves['fortitude'] += max(self.mod('con'), self.mod('str'))
		self.saves['reflex'] += max(self.mod('dex'), self.mod('int')) # TODO: add shield bonus
		self.saves['will'] += max(self.mod('wis'), self.mod('cha'))
		
		#log('%s is ready: %s' % (self.base.name, self.__dict__), type='d20')
		
	@property
	def default_weapon(self):
		return self.inventory.right_hand or self.inventory.left_hand

	def mod(self, attr):
		return int((self.base.stats[attr.lower()] - 10) // 2)
		
	def is_proficient_with(self, weapon):
		return False
		
	def attack(self, target, atk, defense, weapon=None):
		""" Usage: char.attack(target, 'int', 'reflex') """
		df = target.saves[defense]
		bonus = self.base.level // 2
		
		if weapon and self.is_proficient_with(weapon):
			bonus += weapon.prof
			
		r = Dice('d20', mod=atk, bonus=bonus).roll(self)
			
		log('%s vs %s; %s vs %s' % (atk, defense, r, df), type='d20')
		
		return r >= df
		
	def heal(self, value, source=None):
		value = int(value)
		self.hp += value
		
		onHeal.emit(self, value, source)
		
		log('%s healed for %s hp' % (self.base.name or 'Creature', value), type='d20')
		
	def damage(self, value, type=None, source=None):
		value = int(value)
		self.hp -= value
		
		onDamage.emit(self, value, type, source)
		
		log('%s takes %s %s damage' % (self.base.name or 'Creature', value, type or 'normal'), type='d20')
