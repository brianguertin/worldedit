from sgl.core.signals import Signal

# onAfflict, onAilment, onInflict, onBuff ?
# onCure, onDebuff ?

onRoll = Signal()
''' dice(Dice), result(int) '''

onDamage = Signal()
''' char(Character), amount(int), type(str/None), source(Character/None) '''

onHeal = Signal()
''' char(Character), amount(int), source(Character/None) '''

onKnockback = Signal()
''' char(Character), target(Point), distance(int) '''

onAreaEffect = Signal()
''' point(Point), size(Point), type(str/None), source(Power/Weapon/None) '''

onPower = Signal()
''' char(Character), power(Power), info(None/varies) '''
