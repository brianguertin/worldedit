from d20.weapons import Weapon
from d20.trinkets import Trinket

class Inventory(object):
	def __init__(self):
		self.right_hand = None
		self.left_hand = None
		
	def load(self, x):
		# Looks for an item in each possible equipment slot and equips it
		for k in self.__dict__.keys():
			xE = x.find(k)
			if xE is not None:
				name = xE.get('name')
				typ = xE.get('type')
				self.equip(k, name, typ)
		
	def equip(self, part, object, typ=None):
		if typ == 'weapon':
			object = Weapon(object)
		elif typ == 'trinket':
			object = Trinket(object)
		setattr(self, part, object)
