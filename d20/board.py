from d20.signals import onKnockback
from sgl.core.tuples import Point
from sgl.core import xml
	

class Board(object):
	def __init__(self):
		onKnockback(self.handle_knockback)
		self.characters = []
		self.objects = []
		self.uid_count = 0
		self.map = None
		
	def add_object_metadata(self, obj):
		meta = obj.meta
		if not hasattr(meta, 'uid'):
			meta.uid = self.uid_count
			self.uid_count += 1
		if not hasattr(meta, 'position'):
			meta.position = Point()
		
	def add_character(self, char):
		self.add_object_metadata(char)
		self.characters.append(char)
		return char
		
	def add_object(self, obj):
		self.add_object_metadata(char)
		self.objects.append(obj)
		return obj
		
	def get_creatures_at_point(self, p):
		p = Point(p).floor
		return [a for a in self.characters if a.meta.position == p]
		
	def get_creatures_near(self, pos, range):
		pos = Point(pos).floor
		def is_in_range(a):
			p = a.meta.position
			dx = abs(pos.x - p.x)
			dy = abs(pos.y - p.y)
			return dx <= range and dy <= range
		return filter(is_in_range, self.characters)
		
	def handle_knockback(self, source, target, distance):
		s_pos = source.meta.position
		t_pos = target.meta.position
		r = Point()
		if s_pos.x < t_pos.x:
			r.x += distance
		elif s_pos.x > t_pos.x:
			r.x -= distance
		if s_pos.y < t_pos.y:
			r.y += distance
		elif s_pos.y > t_pos.y:
			r.y -= distance
		new_pos = t_pos + r
		if self.map.get_terrain(new_pos).is_walkable:
			target.meta.position = new_pos
		else:
			pass # FIXME: Should move as close to target as possible (in straight line)
		
class Terrain(object):
	class NotFound(Exception):
		pass
	def __init__(self, v):
		self.is_walkable = True
		self.is_difficult = False
		if v == 1:
			self.is_difficult = True
		elif v == 2:
			self.is_walkable = False
		
class Map(object):
	def __init__(self, filename=None):
		self.width = 0
		self.height = 0
		self.terrain = []
		if filename:
			self.load(xml.read(filename))
		
	def load(self, x):
		x.query(self, 'width')
		x.query(self, 'height')
		
		el = x.find('terrain')
		if el is not None:
			terrain = []
			for xRow in el.findall('row'):
				vals = map(int, xRow.get('values').split(','))
				if len(vals) != self.width:
					raise Exception('Not enough values for terrain row data')
				terrain.append(vals)
			if len(terrain) != self.height:
				raise Exception('Not enough rows for terrain data')
			#terrain.reverse()
			self.terrain = terrain
		
	def get_terrain(self, pos):
		try:
			x = int(pos[0])
			y = int(pos[1])
			return Terrain(self.terrain[y][x])
		except:
			raise Terrain.NotFound('Could not get terrain for %r' % pos)
