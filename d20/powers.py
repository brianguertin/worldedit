from sgl.core import compiler
from sgl.core.tuples import AABB
from sgl.core.cache import StaticCache
from sgl.core import path
from xml.etree import ElementTree as xml
from d20.dice import Dice
from d20.signals import onKnockback

_log = lambda s: log(s, type='d20')

class Result(object):
	def __init__(self):
		self.type = ''
		self.attack = ''
		self.defense = ''
		self.radius = 0
		
class DamageEffect:
	def __init__(self):
		self.dice = Dice(0)
		self.type = ''
		self.mod = ''
		
	def __repr__(self):
		return '<%s: %s%s>' % (self.__class__.__name__, self.dice, self.type and ' %s' % self.type or '')
		
	def load(self, x):
		x.query(self, 'type')
		v = x.get('value')
		if v:
			self.dice = Dice(v)
		x.query(self.dice, 'mod')
		
	def apply(self, caster, target):
		d = max(1, self.dice.roll(caster))
			
		target.damage(d, type=self.type)
		
class HealEffect:
	def __init__(self):
		self.dice = Dice(0)
		self.mod = ''
		
	def __repr__(self):
		return '<%s: %s>' % (self.__class__.__name__, self.dice)
		
	@property
	def type(self):
		return 'heal'
		
	def load(self, x):
		v = x.get('value')
		if v:
			self.dice = Dice(v)
		x.query(self, 'mod')
		
	def apply(self, caster, target):
		d = max(1, self.dice.roll(caster))
			
		target.heal(d)
		
class KnockbackEffect:
	def __init__(self):
		self.dice = Dice(0)
		
	def __repr__(self):
		return '<%s: %s>' % (self.__class__.__name__, self.dice)
		
	def load(self, x):
		v = x.get('value')
		if v:
			self.dice = Dice(v)
		
	def apply(self, caster, target):
		d = self.dice.roll(caster)
		onKnockback.emit(caster, target, d)
		
"""
class ScriptEffect:
	def __init__(self):
		self.script = None
		
	def load(self, x):
		self.script = compiler.compile_literal(x.text)
		
	def apply(self, caster, target):
		exec self.script in {'caster':caster,'target':target,'Dice':Dice}
"""

class Power(StaticCache):
	effects = {
		'damage':DamageEffect,
		'heal':HealEffect,
		'knockback':KnockbackEffect,
	}
	
	def __cmp__(self, o):
		return cmp(self.name, o.name)
		
	def __str__(self):
		return self.name
		
	"""
	def __repr__(self):
		return '<%s: %s>' % (self.__class__.name, self.name)
	"""
	
	def init(self):
		self.name = ''
		self.icon = ''
		self.action = 'standard'
		self.minrange = 0
		self.range = 1
		self.target = ''
		self.duration = ''
		self.resist = False
		self.harmless = False
		self.levels = {}
		self.components = {}
		self.results = []
		self.harmful = True
		
	def load(self, x):
		x.query(self, 'name')
		x.query(self, 'action')
		x.query(self, 'target')
		x.query(self, 'duration')
		x.query(self, 'harmless')
		x.query(self, 'resist')
		x.query(self, 'minrange')
		x.query(self, 'range')
		
		icon = x.get('icon')
		if icon:
			self.icon = path.find(icon)
		
		for xLevel in x.findall('level'):
			cls = xLevel.get('class')
			self.levels[cls] = int(xLevel.text)
			
		for xComp in x.findall('component'):
			type = xComp.get('type')
			self.components[type] = xLevel.text or True
			
		for xResult in x.findall('result'):
			t = Result()
			xResult.query(t, 'type')
			xResult.query(t, 'attack')
			xResult.query(t, 'defense')
			xResult.query(t, 'radius')
			effects = []
			
			for name,cls in Power.effects.items():
				for xD in xResult.findall(name):
					e = cls()
					e.load(xD)
					effects.append(e)
				
			self.results.append((t,effects))
			
		#_log('Spell loaded: %s (%s)' % (self.name, self.__dict__))
	
	@property
	def radius(self):
		return self.range
