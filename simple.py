#!/usr/bin/env python
from sgl.app.splash import do_splash
do_splash('splash.png')

from sgl.app.app import App, Event, Key, Mouse
from sgl.game.controller import Controller
from sgl.game.world import World
from sgl.game.actor import ActorDef
from sgl.core import path
import xml.etree.ElementTree as xml
from datetime import datetime
import os

class Basic:
	def run(self, world='world.xml'):
		path.add('entities')
		self.app = App()
		#self.app.options.find('display').set('width', 1024)
		#self.app.options.find('display').set('height', 768)
		#self.app.options.find('audio').set('enabled', False)
		self.app.init()
		
		
		self.reset(world)
		
		start_time = datetime.now()
		self.frames = 0
		
		self.app.run(self.main_loop)
		
		seconds = (datetime.now() - start_time).seconds
		
		log('%s frames in %s seconds. Frames per second: %s' % (self.frames, seconds, float(self.frames) / (seconds or 1)))
		
	def reset(self, source=None):
		self.w = World(source or self.w.source)
		self.a = self.w.getActorById('player')
		self.a.isDestroyable = False
		self.controller = Controller(self.a)
		
		self.z = 200
		
	def main_loop(self, frame):
		self.frames += 1
		for e in frame.events:
			if not (self.controller.input(e)):
				if e.type == Event.QUIT:
					frame.exit = True
				elif e.type == Event.KEYDOWN:
					k = e.key
					if k == Key.F10:
						frame.exit = True
					elif k == Key.R:
						self.reset()
					elif k == Key.BACKQUOTE:
						self.app.console.hidden = not self.app.console.hidden
				elif e.type == Event.MOUSEBUTTONDOWN:
					b = e.button
					if b == Mouse.WHEELUP:
						self.z *= 1.1
					elif b == Mouse.WHEELDOWN:
						self.z *= 0.9
				
		self.w.update(frame.ticks)
		self.w.draw(self.app.display, camera=self.a.get('position'), scale=self.z)

if __name__ == '__main__':
	try:
		import psyco
		psyco.full()
	except ImportError:
		pass
	import sys
	app = Basic()
	if len(sys.argv) > 1 and not sys.argv[1].startswith('--'):
		if sys.argv[1] == 'profile':
			import pstats, cProfile as profile
			profile.run('app.run()', 'profile.bin')
			p = pstats.Stats('profile.bin')
			p.strip_dirs().sort_stats('cumulative').print_stats(20)
		else:
			app.run(sys.argv[1])
	else:
		app.run()
