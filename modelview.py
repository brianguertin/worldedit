from sgl.app.app import *
from sgl.game.model import Model

class MyApp:
	def run(self):
		self.app = App()
		self.app.init()
		
		self.model = Model('gary')
		self.model.addAnim('walk')
		
		self.app.run(self.main_loop)
		
	def main_loop(self, frame):
		for e in frame.events:
			if e.type == Event.QUIT:
				frame.exit = True
				
		self.model.update(frame.ticks)
				
		d = self.app.display
		d.pushMatrix()
		d.translate((d.width / 2, d.height / 2))
		self.model.draw(d)
		d.popMatrix()
				
if __name__ == '__main__':
	app = MyApp()
	app.run()
