#!/usr/bin/env python

mods = ['PHB_4e']

from sgl.app.splash import do_splash
do_splash('splash.png')

from sgl.app.app import App, Event, Key, Mouse
from sgl.game.controller import Controller
from sgl.game.world import World
from sgl.game.actor import ActorDef
from sgl.core import path
from sgl.video.texture import Texture

from sgl.gui.controller import Controller as GuiController
from sgl.gui.theme import Theme
from sgl.gui.button import Button, ImageButton
from sgl.gui.label import Label
from sgl.gui.panel import Panel
from sgl.gui.box import Box, VBox, HBox

import xml.etree.ElementTree as xml
from datetime import datetime
import os

class ActionBarItem:
	def __init__(self, *args):
		self.icon, self.command = args
		
class ActionBarButton(ImageButton):
	def __init__(self, id, *args, **kargs):
		self.label = Theme.Global.font.render(str(id)[0])
		super(ActionBarButton, self).__init__(*args, **kargs)
		
	def draw(self, d):
		self.image.draw(d)
		
		d.pushMatrix()
		d.translate(self.position)
		d.translate((self.width - self.label.width / 2 - 2, self.label.height / 2 - 2))
		self.label.draw()
		d.popMatrix()

class ActionBar(HBox):
	def __init__(self):
		super(ActionBar, self).__init__()
		
		self.actor = None
		
		self.actions = [None] * 108
		
		self.add(ActionBarButton('1'))
		self.add(ActionBarButton('2'))
		self.add(ActionBarButton('3'))
		self.add(ActionBarButton('4'))
		self.add(ActionBarButton('5'))
		self.add(ActionBarButton('6'))
		self.add(ActionBarButton('7'))
		self.add(ActionBarButton('8'))
		self.add(ActionBarButton('9'))
		self.add(ActionBarButton('0'))
		self.add(ActionBarButton('-'))
		self.add(ActionBarButton('='))
		
		"""
		for i, btn in zip(range(len(self.children)), self.children):
			def fn():
				self.do(i)
			btn.onClick(fn)
		"""
		
	def set_item(self, index, icon, command):
		self.actions[index] = ActionBarItem(icon, command)
		self.reset_images()
		
	def reset_images(self):
		for i in range(11):
			action = self.actions[i]
			if action and action.icon:
				self.children[i].texture = action.icon
			else:
				self.children[i].texture = None
		
	def do(self, index):
		action = self.actions[index]
		if action:
			try:
				self.actor.emit(*action.command)
			except:
				pass
		else:
			log('No action bound at index %s' % index, type='ActionBar')
		
	def handleKeyDown(self, e):
		k = e.key
		if k == Key._1:
			self.do(0)
		elif k == Key._2:
			self.do(1)
		elif k == Key._3:
			self.do(2)
		elif k == Key._4:
			self.do(3)
		elif k == Key._5:
			self.do(4)
		elif k == Key._6:
			self.do(5)
		elif k == Key._7:
			self.do(6)
		elif k == Key._8:
			self.do(7)
		elif k == Key._9:
			self.do(8)
		elif k == Key._0:
			self.do(9)
		elif k == Key.MINUS:
			self.do(10)
		elif k == Key.EQUALS:
			self.do(11)

class Basic:
	def run(self, world='dnd_world.xml'):
		path.add('data/entities')
		path.add('mods')
		
		import d20
		for m in mods:
			d20.add_module(m)
		
		self.app = App()
		#self.app.options.find('display').set('width', 1024)
		#self.app.options.find('display').set('height', 768)
		#self.app.options.find('audio').set('enabled', False)
		self.app.init()
		
		self.setup_interface()
		
		self.reset(world)
		
		start_time = datetime.now()
		self.frames = 0
		
		self.app.run(self.main_loop)
		
		seconds = (datetime.now() - start_time).seconds
		
		log('%s frames in %s seconds. Frames per second: %s' % (self.frames, seconds, float(self.frames) / (seconds or 1)))
	
	def setup_interface(self):
		Theme.Global = self.theme = Theme()
		
		ICON_SIZE = 32
		self.actionbar = ActionBar()
		self.actionbar.size = ICON_SIZE * 12 + 2, ICON_SIZE + 2
		self.actionbar.x = self.app.display.width / 2 - self.actionbar.width / 2
		self.actionbar.y = self.app.display.height - self.actionbar.height
		self.actionbar.pack()
		
		panel = Panel()
		panel.size = self.app.display.size
		panel.add(self.actionbar)
		
		self.root = GuiController(panel)
		self.panel = panel
		
		# Non-Gui
		self.target_icon = Texture('target.png')
		
	def reset(self, source=None):
		self.z = 200
		self.w = World(source or self.w.source)
		
		self.a = self.w.getActorById('player')
		self.a.isDestroyable = False
		
		x = xml.ElementTree(file=path.find('characters/paul.xml')).getroot()
		self.a.do('load_character', x)
		
		self.actionbar.actor = self.a
		powers = self.a.get('powers')
		for i, p in zip(range(len(powers)), powers):
			self.actionbar.set_item(i, p.icon, ('+cast', p))
			
		
		self.controller = Controller(self.a)
		
		# Enemy
		x = xml.ElementTree(file=path.find('characters/enemy.xml')).getroot()
		self.w.getActorById('dummy').do('load_character', x)
		
		
	def update(self, frame):
		for e in frame.events:
			if not (self.root.input(e) or self.controller.input(e)):
				if e.type == Event.QUIT:
					frame.exit = True
				elif e.type == Event.KEYDOWN:
					k = e.key
					if k == Key.F10:
						frame.exit = True
					elif k == Key.R:
						self.reset()
					elif k == Key.G:
						x = self.a.get('ragdoll')
						adef = self.w.createActorDef(x)
						info = self.w.spawn(adef)
						#info.set('position', self.a.get('position'))
						info.set('position', (1,1.3))
					elif k == Key.BACKQUOTE:
						self.app.console.hidden = not self.app.console.hidden
					elif k == Key.P:
						for a in self.w.getActorsByClass('creature'):
							log('%s: %s hp' % (a, a.get('hp')))
				elif e.type == Event.MOUSEBUTTONDOWN:
					b = e.button
					if b == Mouse.WHEELUP:
						self.z *= 1.1
					elif b == Mouse.WHEELDOWN:
						self.z *= 0.9
				
		# Render world
		self.w.update(frame.ticks)
		self.controller.update()
		
	def draw(self):
		
		self.w.draw(self.app.display, camera=self.a.get('position'), scale=self.z)
		
		target = self.a.get('target')
		if target:
			pos = target.get('position')
			d = self.app.display
			d.pushMatrix()
			self.w.view.transform(d)
			d.translate(pos)
			d.drawTextureScaled(self.target_icon, (0.1,0.1))
			d.popMatrix()
		
		# Render interface
		self.theme.drawInterface(self.panel, self.app.display)
		
	def main_loop(self, frame):
		self.frames += 1
		self.draw()
		self.update(frame)

if __name__ == '__main__':
	try:
		import psyco
		psyco.full()
	except ImportError:
		pass
	import sys
	app = Basic()
	if len(sys.argv) > 1 and not sys.argv[1].startswith('--'):
		if sys.argv[1] == 'profile':
			import pstats, cProfile as profile
			profile.run('app.run()', 'profile.bin')
			p = pstats.Stats('profile.bin')
			p.strip_dirs().sort_stats('cumulative').print_stats(20)
		else:
			app.run(sys.argv[1])
	else:
		app.run()
