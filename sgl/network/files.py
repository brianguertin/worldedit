from __future__ import with_statement
from sgl.core.containers import TwoWayDict
import SocketServer, socket, os

class FileServer(object):
	def __init__(self, addr):
		self.uid_count = 0
		self.addr = addr
		self.files = TwoWayDict() # maps UID to filename and vise versa
		self.server = SocketServer.TCPServer(addr, self.make_handler)
		
	def make_handler(self, *args, **kargs):
		files = self.files
		class FileServeHandler(BaseFileServeHandler):
			def get_filename(self, uid):
				return files[uid]
		return FileServeHandler(*args, **kargs)
		
	def share(self, filename):
		if not os.path.exists(filename):
			raise Exception('File not found: "%s"' % filename)
		try:
			uid = self.files.inverse[filename]
		except KeyError:
			uid = self.uid_count + 1
			self.files[uid] = filename
			self.uid_count = uid

		return RemoteFile(self.addr, uid)

	def serve(self):
		self.server.serve_forever()

class BaseFileServeHandler(SocketServer.StreamRequestHandler):
	def handle(self):
		data = self.rfile.readline().strip()
		try:
			uid = int(data)
		except ValueError:
			self.wfile.write('0') # Not OK
			return
		try:
			filename = self.get_filename(uid)
		except KeyError:
			self.wfile.write('0') # Not OK
			return
		
		# otherwise, open the file ehre and share it back
		self.wfile.write('1') # OK
		with open(filename, 'rb') as infile:
			self.wfile.write(infile.read())
		
class RemoteFile(object):
	def __init__(self, addr = None, uid = None):
		self.addr = addr # (host, port)
		self.uid = uid
		
	def open(self):
		# Connect to server and request file by UID
		sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		sock.connect(self.addr)
		sock.send('%s\n' % self.uid)
		r = sock.recv(1)
		if r == '1':
			return sock.makefile('r')
		else:
			raise Exception('Error opening remote file')
