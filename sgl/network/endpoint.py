from packet import PacketID, Packet
import socket, struct, time, zlib

try:
	import cPickle as pickle
except ImportError:
	import pickle
	
MTU = 1024 # bytes
RESEND_AFTER = 500 # milliseconds
CONNECTION_TIMEOUT = 4000 # milliseconds
KEEPALIVE_INTERVAL = 900 # milliseconds
COMPRESS_THRESHOLD = 100 # packets larger then this will get gzipped

PACKET_HEADER_FMT = 'BBBBL' # Reliable, Ordered, Compressed, ID, UID
PACKET_HEADER_LEN = struct.calcsize(PACKET_HEADER_FMT)

ACK_HEADER_FMT = 'L'
ACK_HEADER_LEN = struct.calcsize(ACK_HEADER_FMT)

RELIABLE_DEFAULT = True
ORDERED_DEFAULT = False

getPacketFromData = Packet.fromData
getDataFromPacket = Packet.toData

class Endpoint(object):
	def __init__(self):
		self.addr = None
		self.sock = None
		self.outgoing = {}
		self.receivedUIDs = []
		self.lastPacketUIDSent = 0
		self.lastPacketUIDReceived = 0
		self.lastPacketReceivedTime = 0
		self.lastPacketSentTime = 0
		
	def is_alive(self):
		return get_time() - self.lastPacketReceivedTime < CONNECTION_TIMEOUT
		
	def revive(self):
		self.send(PacketID.KEEPALIVE, reliable = False)
		self.lastPacketReceivedTime = get_time()
		
	def disconnect(self):
		self.send(PacketID.DISCONNECT, reliable = True) # reliable, though unlikely caller will resend
		
	def send(self, id, data = None, reliable = RELIABLE_DEFAULT, ordered = ORDERED_DEFAULT):
		uid = self.lastPacketUIDSent + 1
		data = str(data and getDataFromPacket(data) or '')
		if len(data) >= COMPRESS_THRESHOLD:
			compressed = True
			data = zlib.compress(data)
		else:
			compressed = False
		data = struct.pack(PACKET_HEADER_FMT, reliable, ordered, compressed, id, uid) + data
		
		self.sendRawPacket(data)
		
		self.lastPacketUIDSent = uid
		if reliable:
			self.outgoing[uid] = OutgoingPacket(data)
			
	def sendRawPacket(self, data):
		if len(data) > MTU:
			raise Exception('Packet of length %s is greater than MTU of %s' % (len(data), MTU))
		self.sock.sendto(data, self.addr)
		self.lastPacketSentTime = get_time()
		
	def sendACK(self):
		# Reports received reliable packets
		count = len(self.receivedUIDs)
		if count > 0:
			fmt = ACK_HEADER_FMT + 'L' * count
			self.send(PacketID.ACK, struct.pack(fmt, count, *self.receivedUIDs), reliable = False)
			self.receivedUIDs = []
			
	def handleACK(self, data):
		count = struct.unpack(ACK_HEADER_FMT, data[:ACK_HEADER_LEN])[0]
		fmt = 'L' * count
		uid_list = struct.unpack(fmt, data[ACK_HEADER_LEN:])
		for id in uid_list:
			try:
				del self.outgoing[id]
			except KeyError:
				pass
		
	def update(self):
		self.sendACK()
		
		# Resend unconfirmed reliable packets
		t = get_time()
		for p in self.outgoing.values():
			if (t - p.lastSent) >= RESEND_AFTER:
				self.sendRawPacket(p.data)
				p.reset()
				
		if self.lastPacketSentTime + KEEPALIVE_INTERVAL < t:
			self.send(PacketID.KEEPALIVE, reliable = False)
			
	def receiveOne(self):
		try:
			while True:
				data, addr = self.sock.recvfrom(MTU)
				
				#if addr != self.addr:
				#	continue #log('Bad packet addr %s, should be %s' % (addr, self.addr))
				
				p = RawPacket(data, addr)
				self.lastPacketReceivedTime = get_time()
				
				if p.id == PacketID.ACK:
					self.handleACK(p.data)
					continue
					
				if p.reliable:
					self.receivedUIDs.append(p.uid)
				return p
		except socket.error:
			pass

			
class OutgoingPacket(object):
	def __init__(self, data):
		self.data = data
		self.reset()
		
	def reset(self):
		self.lastSent = get_time()

class RawPacket(object):
	def __init__(self, data = None, sender = None):
		self.sender = sender
		if data:
			self.reliable, self.ordered, self.compressed, self.id, self.uid = struct.unpack(PACKET_HEADER_FMT, data[:PACKET_HEADER_LEN])
			data = data[PACKET_HEADER_LEN:]
			if self.compressed:
				data = zlib.decompress(data)
			self.data = len(data) and getPacketFromData(data) or None
		else:
			self.reliable, self.ordered, self.id, self.uid, self.data = False, False, None, None, None
			
	def __iter__(self):
		return iter((self.id, self.data, self.sender))

def get_time():
	return int(time.time() * 1000)
