from endpoint import *
from sgl.network.util import get_local_ip

class Server(object):
	def __init__(self):
		self.sock = None
		self.peers = {}
	
	def listen(self, addr):
		if not isinstance(addr, tuple):
			addr = (get_local_ip(), int(addr))
		self.sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
		self.sock.setblocking(False)
		self.sock.bind(addr)
		self.peers = {}
		self.addr = addr
		
	def punch(self, addr):
		peer = Endpoint()
		peer.addr = addr
		
		peer.sock = self.sock
		#peer.sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
		#peer.sock.setblocking(False)
		
		peer.revive()
		peer.revive()
		#self.peers[addr] = peer
		
		return self.addr
		#return peer.sock.getsockname()
		
	def send(self, id, data = None, addr = None, reliable = RELIABLE_DEFAULT, ordered = ORDERED_DEFAULT):
		return self.getPeerByAddress(addr).send(id, data, reliable=reliable, ordered=ordered)
		
	def broadcast(self, id, data = None, reliable = RELIABLE_DEFAULT, ordered = ORDERED_DEFAULT):
		for peer in self.peers.values():
			peer.send(id, data, reliable=reliable, ordered=ordered)
		
	def getPeerByAddress(self, addr):
		return self.peers[addr]
		
	def receiveOne(self):
		try:
			while True:
				data, addr = self.sock.recvfrom(MTU)
				p = RawPacket(data, addr)
				
				if addr not in self.peers:
					if p.id == PacketID.CONNECT:
						if addr not in self.peers:
							peer = Endpoint()
							peer.addr = addr
							peer.sock = self.sock
							peer.send(PacketID.CONNECT, reliable=True)
							self.peers[addr] = peer
							log('New connection from %s:%s' % addr)
					else:
						log('Unconnected %s packet from %s: %s' % (PacketID.get_id_string(p.id), addr, self.peers))
						continue # ignore packets from non-peers
				else:
					peer = self.getPeerByAddress(addr)
					
				peer.lastPacketReceivedTime = get_time()
				
				if p.id == PacketID.ACK:
					peer.handleACK(p.data)
					continue
					
				if p.reliable:
					peer.receivedUIDs.append(p.uid)
					# TODO: handle ordered reliable packet (ugh..)
				elif p.ordered and p.uid < peer.lastPacketUIDReceived:
					continue # skip out of order un-reliable packet
					
				peer.lastPacketUIDReceived = p.uid
				return p
		except socket.error:
			pass
		
		
	def receive(self):
		packets = []
		
		for peer in self.peers.values():
			peer.update()
			
			# Handle punch-through connected sockets
			if peer.sock != self.sock:
				p = peer.receiveOne()
				while p is not None:
					packets.append(p)
					p = peer.receiveOne()
			
		p = self.receiveOne()
		while p is not None:
			packets.append(p)
			p = self.receiveOne()
			
		dead_peers = [] # addr, peer tuples
		for addr, peer in self.peers.items():
			if not peer.is_alive():
				dead_peers.append((addr, peer))
				
		for addr, peer in dead_peers:
			peer.disconnect()
			p = RawPacket(sender=addr)
			p.id = PacketID.TIMEOUT
			packets.append(p)
			del self.peers[addr]
			
		return packets
