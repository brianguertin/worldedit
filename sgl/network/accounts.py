from sgl.core import xml
from sgl.core import path
from sgl.core.containers import AttrDict
import hashlib

class AccountService(object):
	def __init__(self, dirname = 'accounts', dbname = 'accounts.xml'):
		self.dirname = dirname
		self.dbname = dbname
		self.accounts = {}
		
		dbfilename = path.join(self.dirname, self.dbname)
		if path.exists(dbfilename):
			self.load(dbfilename)
		
	def load(self, x):
		x = xml.read(x)
		for xAcc in x.findall('account'):
			id = (xAcc.get('id') or '').strip()
			if id:
				a = Account(self, id, (xAcc.get('pass') or '').strip())
				xMeta = xAcc.find('meta')
				if xMeta is not None:
					a.meta = AttrDict(xMeta.attrib)
				self.accounts[id] = a
			else:
				log('Account missing id')
			
	def create(self, id, password, save_immediately = True):
		id = id.strip()
		pwd = hashlib.md5(password).hexdigest()
		a = Account(self, id, pwd)
		self.accounts[id] = a
		if save_immediately:
			self.save()
		return a
		
	def get(self, id):
		return self.accounts[id]
		
	def exists(self, id):
		try:
			self.get(id)
			return True
		except KeyError:
			return False
		
	def login(self, id, password):
		u = self.get(id)
		if hashlib.md5(password).hexdigest() == u.password:
			return u
		
	def save(self):
		accounts = xml.Element('accounts')
		for k, a in self.accounts.items():
			acc = xml.Element('account')
			acc.set('id', a.id)
			acc.set('pass', a.password)
			if a.meta:
				meta = xml.Element('meta')
				for k, v in a.meta.items():
					meta.set(str(k), str(v))
				acc.append(meta)
			accounts.append(acc)
		
		etree = xml.ElementTree()
		etree._root = accounts
		filename = path.join(self.dirname, self.dbname)
		if not path.exists(path.dirname(filename)):
			path.makedirs(path.dirname(filename))
		etree.write(filename)

class Account(object):
	def __init__(self, service = None, id = None, password = None):
		self.service = service
		self.id = id
		self.password = password
		self.meta = AttrDict()
		
	def __repr__(self):
		return '<%s: %s>' % (self.__class__.__name__, self.id)
		
	@property
	def dir(self):
		return path.join(self.service.dirname, self.id)
		
	def store(self, filename):
		filename = path.join(self.dir, filename)
		dir = path.dirname(filename)
		if not path.exists(dir):
			path.makedirs(dir)
		return filename
