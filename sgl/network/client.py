from endpoint import *

class Client(Endpoint):
	def __init__(self):
		super(Client, self).__init__()
		self.is_connected = False
		self.largestPacket = 0
		self.packetsReceived = 0
		
	def connect(self, addr):
		self.is_connected = False
		self.sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
		self.sock.setblocking(False)
		self.addr = addr
		self.send(PacketID.CONNECT, reliable=True)
		
	def reconnect(self):
		self.is_connected = False
		self.send(PacketID.CONNECT, reliable=True)
		
	def shutdown(self):
		if self.is_connected:
			self.disconnect()
			self.is_connected = False
			log('Received %s packets, largest was %s bytes' % (self.packetsReceived, self.largestPacket))
		
	@property	
	def port(self):
		return self.sock.getsockname()[1]
		
	def receiveOne(self):
		try:
			while True:
				data, addr = self.sock.recvfrom(MTU)
				
				if self.is_connected and addr != self.addr:
					continue #log('Bad packet addr %s, should be %s' % (addr, self.addr))
					
				self.packetsReceived += 1
				self.largestPacket = max(self.largestPacket, len(data))
				p = RawPacket(data, addr)
				self.lastPacketReceivedTime = get_time()
				
				if p.id == PacketID.ACK:
					self.handleACK(p.data)
					continue
					
				if p.reliable:
					self.receivedUIDs.append(p.uid)
				
				if p.id == PacketID.CONNECT:
					self.is_connected = True
					self.addr = addr
				return p
		except socket.error:
			pass
		
	def receive(self):
		self.update()
		
		packets = []
		p = self.receiveOne()
		while p is not None:
			packets.append(p)
			p = self.receiveOne()
			
		if self.is_connected and not self.is_alive():
			self.shutdown()
			p = RawPacket(sender=self.addr)
			p.id = PacketID.TIMEOUT
			packets.append(p)
		return packets
