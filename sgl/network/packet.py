try:
	import cPickle as pickle
except ImportError:
	import pickle



class Packet(dict):
	def __init__(self, *args, **kargs):
		dict.__init__(self, *args, **kargs)
		
	def __str__(self):
		return pickle.dumps(dict(self))
		
	def __getattr__(self, name):
		return self[name]
		
	def __setattr__(self, name, val):
		self[name] = val
		
	@staticmethod
	def fromData(data):
		r = pickle.loads(data)
		if isinstance(r, dict):
			r = Packet(r)
		return r
			
	@staticmethod
	def toData(packet):
		return pickle.dumps(packet)

class PacketID:
	PACKET_IDS = {
		# Connections
		'CONNECT':1,
		'DISCONNECT':2,
		'TIMEOUT':3,
		'KEEPALIVE':4,
		'ACK':5,
		'PUNCH_REQUEST':6, # NAT punch-through request
		'PUNCH_RESPONSE':7, # NAT punch-through response
		
		# Accounts
		'LOGIN':10,
		'REGISTER':11,
		'INVALID':12,
		
		# Lobby
		'ANNOUNCE':20,
		'DENOUNCE':21,
		'QUERY':22,
		'HOST':23,
		'JOIN':24,
		'LEAVE':25,
		'OPTION':26,
		'START':27,
		'DISBAND':28,
		'READY':29,
		
		# Chat
		'MESSAGE':30,
		
		# Game
		'COMMAND':40,
		'GAMESTATE':41,
		'UPDATE':42,
		'CREATE':43,
		'DESTROY':44,
	}
	PACKET_IDS = dict((k,v) for k,v in PACKET_IDS.iteritems())
	REVERSE_PACKET_IDS = dict((v,k) for k, v in PACKET_IDS.iteritems())

	@classmethod
	def get_id_string(cls, id):
		return cls.REVERSE_PACKET_IDS[id]
		
	@classmethod	
	def get_string_id(cls, id):
		return cls.PACKET_IDS[id]
		
for k, v in PacketID.PACKET_IDS.iteritems():
	setattr(PacketID, k, v)
