from sgl.core.decorators import deferred

@deferred
def get_local_ip():
	import socket
	try:
		s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
		s.connect(('someridiculousdomain.notcom', 80))
		return s.getsockname()[0]
	except socket.gaierror:
		return '127.0.0.1'
	
@deferred
def get_public_ip():
	import urllib2, re
	ip = urllib2.urlopen("http://whatismyip.com/automation/n09230945.asp").read()
	if re.match('(\d{1,3}\.){3}\d{1,3}', ip):
		return ip
	else:
		return get_local_ip()
