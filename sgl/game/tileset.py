from sgl.video.texture import *
#from pygame import *
from xml.etree.ElementTree import ElementTree
from sgl.core import path
from sgl.core.tuples import Point

class Tile(object):
	def __init__(self):
		self.texture = None
		self.vertices = []
		self.isSolid = True
		self.isPlatform = False

class Tileset(object):
	_cache = {}
	def __new__(cls, filename):
		f = path.find(filename)
		if f in Tileset._cache:
			return Tileset._cache[f]
		else:
			self = object.__new__(Tileset)
			log('Loading: "%s"' % f, type='Tileset')
			self._init(f)
			Tileset._cache[f] = self
			return self
			
	def _init(self, f):
		""" Accepts tileset.xml files directly, or a directory containing tileset.xml """
		
		self.filename = f
		self.texture = None
		self.tiles = []
		self.width = 1.0
		self.height = 1.0
		
		if path.isdir(f):
			dir = f
			f = path.join(dir, 'tileset.xml')
		else:
			dir = path.dirname(f)
			
		try:
			path.add(dir)
			xTileset = ElementTree(file=f).getroot()
			xTileset.query(self, 'width')
			xTileset.query(self, 'height')
			filename = xTileset.get('filename')
			
			if filename:
				info = _Info(xTileset)
				self.info = info
				
				# Crop border
				tset_tex = Texture.fromFilename(path.find(filename), colorkey=info.colorkey)
				tset_tex = tset_tex.region((info.borderleft, info.bordertop),(None,None))
				self.texture = tset_tex
					
				p = info.spacing
				tw = info.tilewidth + p
				th = info.tileheight + p
				spanx = int(tset_tex.width // tw)
				spany = int(tset_tex.height // th)
				
				for y in range(spany):
					for x in range(spanx):
						x1, y1 = x * tw, y * th
						tex = tset_tex.region((x1, y1), (x1+tw-p, y1+th-p))
						t = Tile()
						t.texture = tex
						self.tiles.append(t)
				
			for xTile in xTileset.findall('tile'):
				filename = xTile.get('filename')
				index = xTile.get('index')
				if filename:
					tex = Texture(filename)
					t = Tile()
					t.texture = tex
				elif index:
					try:
						t = self.tiles[int(index)]
					except:
						continue
				else:
					continue
				xTile.query(t, 'isSolid')
				xTile.query(t, 'isPlatform')
				for xVertex in xTile.findall('vertex'):
					p = Point()
					xVertex.query(p, 'x')
					xVertex.query(p, 'y')
					t.vertices.append(p)
				self.tiles.append(t)
				
		finally:
			path.pop()
	
	def select(self, point):
		""" Retrieve tile from tilemap given an absolute point between (0,0) and (1,1) """
		if not self.texture:
			raise Exception('Tilemap texture required for selection')
		elif point[0] < 0 or point[1] < 0 or point[0] > 1 or point[1] > 1:
			raise Exception('Tilemap selection must be between (0,0) and (1,1)')
			
		p = self.info.spacing
		tw = self.info.tilewidth + p
		th = self.info.tileheight + p
		spanx = int(self.texture.width // tw)
		
		x = self.texture.width * point[0] // tw
		y = self.texture.height * point[1] // th
		index = int(x + y * spanx)
		return index
		
class _Info(object):
	def __init__(self, x):
		self.bordertop = 0
		self.borderleft = 0
		self.spacing = 0
		self.tilewidth = 0
		self.tileheight = 0
		self.colorkey = None
		x.query(self, 'bordertop')
		x.query(self, 'borderleft')
		x.query(self, 'spacing')
		x.query(self, 'tilewidth')
		x.query(self, 'tileheight')
		
		colorkey = x.get('colorkey')
		if colorkey:
			self.colorkey = map(int, colorkey.split(','))
