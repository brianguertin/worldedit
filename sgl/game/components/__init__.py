__all__ = (
	'Aim',
	'BallPlatformerControlsComponent',
	'Physics',
	'CarryableComponent',
	'ControllableComponent',
	'D20Component',
	'DestroyAfterDuration',
	'DestroyOnCollisionComponent',
	'ExplodeComponent',
	'FaceTarget',
	'Health',
	'InflictDamageOnCollide',
	'ModelComponent',
	'NoCollideFriendly',
	'PointsComponent',
	'PositionComponent',
	'UsableComponent',
	'UsesObjectsComponent',
	'UseWeaponsComponent',
	'PlatformerControlsComponent',
	'TextureComponent',
	'TrackNearestEnemy',
	'ScriptComponent',
	'SpawnOnDestroyComponent',
	'SpriteComponent',
	'VehicleControlsComponent',
)

def register_builtin_components(w, reload_=True):
	for s in __all__:
		try:
			m = getattr(__import__('sgl.game.components', fromlist=[s]),s)
			if reload_:
				m = reload(m)
		except Exception, e:
			log('Error loading %s: %s' % (s, e))
			continue
			
		name = getattr(m, 'name', None)
		comp = getattr(m, 'Component', None)
		cdef = getattr(m, 'ComponentDef', None)
		manager = getattr(m, 'Manager', None)
		
		if not name:
			name = s.replace('Component','')
		
		if comp:
			w.addFactory(name, comp, cdef)
		
		if manager:
			w.addManagerFactory(name, manager)
