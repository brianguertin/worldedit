
class Component:
	def init(self):
		self._actor.do('add_collision_filter', self.isNotFriendly)
		
	def isNotFriendly(self, a1, a2):
		if a1.faction and a2.faction:
			if a1.faction == a2.faction:
				return False
			elif a2.faction in a1.faction.friends:
				return False
			elif a1.faction in a2.faction.friends:
				return False
		return True
