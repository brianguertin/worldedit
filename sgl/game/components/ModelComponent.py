from sgl.game.model import Skeleton, Skin, Model
from sgl.core.tuples import *
import math

class ComponentDef:
	def __init__(self):
		self.skeleton = None
		self.skin = None
		self.scale = 1.0
		self.offsetx = 0.0
		self.offsety = 0.0
		self.default = ''
		self.rotate = False
		
	def load(self, x):
		s = x.get('skeleton')
		if s:
			self.skeleton = Skeleton(s)
		s = x.get('skin')
		if s:
			self.skin = Skin(s)
			
		x.query(self, 'rotate')
		x.query(self, 'scale')
		x.query(self, 'default')
		x.query(self, 'x', 'offsetx')
		x.query(self, 'y', 'offsety')
		
	def create(self):
		c = Component()
		if not self.skeleton:
			raise Exception('Skeleton required for model but none specified')
			
		c.offset = Point(self.offsetx, self.offsety)
		c.scale = self.scale
		c.rotate = self.rotate
		
		c.model = Model()
		c.model.init(self.skeleton, self.skin)
		if self.default:
			c.model.addAnim(self.default)
			c.model.update(0)
		return c

class Component:
	def __init__(self):
		self.model = None
		self.offset = None
		self.rotate = False
		self.scale = 1.0
		self._actions = {
			'add_anim':0,
			'set_anim':0,
			'get_point':0,
			'set_model.rotate':'set_rotate',
			'get_model.offset':'get_offset',
			'set_model.joint.angle':'set_joint_angle',
			'get_model.joint.position':'get_joint_position',
			'get_ragdoll':0,
			'spawn_ragdoll':0,
		}
		
	def init(self):
		self.actor.connect('update', self.model.update)
		self.actor.connect('draw', self.draw)
		self.actor.connect('face', self.face)
		self.actor.connect('serialize', self.serialize)
		self.actor.connect('deserialize', self.deserialize)
		
	def serialize(self, d):
		anims = []
		for state in self.model.activeAnims:
			a = {}
			a['id'] = state.animation.id
			a['ticks'] = state.ticks
			a['current'] = state.current
			a['next'] = state.next
			a['group'] = state.group
			a['loops'] = state.num_loops
			a['loop'] = state.loop_counter
			a['mod'] = state.mod
			anims.append(a)
		d['anims'] = anims
		
	def deserialize(self, d):
		self.model.clearAnims()
		for a in d['anims']:
			state = self.model.addAnim(a['id'], a['loops'], a['group'])
			state.ticks = a['ticks']
			state.current = a['current']
			state.next = a['next']
			state.loop_counter = a['loops']
			state.mod = a['mod']
		self.model.update(0)
		
	def face(self, face):
		self.model.flipHorizontal = (face == 'left')
		
	def get_offset(self):
		return self.offset * self.scale
		
	def get_point(self, name, span=1.0):
		p = self.model.getBonePosition(name, span)
		r = p * self.scale
		r.orientation = p.orientation
		return r
		
	def set_rotate(self, v):
		self.rotate = bool(v)
		
	def get_joint_position(self, name):
		p = self.model.getBonePosition(name, span=0)
		r = p * self.scale
		return r
		
	def set_joint_angle(self, id, angle):
		b, st = self.model.getBoneAndState(id)
		st.rotation = angle
		
	def set_anim(self, anim, group=0):
		try:
			self.model.setAnim(anim, group=group)
		except Exception, e:
			log('Animation not found: "%s"' % anim, type='Model', level=1)
		
	def clear_anim(self):
		self.model.clearAnims()
		
	def add_anim(self, anim, loops = 0, group = 0):
		self.model.addAnim(anim, loops, group=group)
		
	def get_ragdoll(self):
		import xml.etree.ElementTree as xml
		_thickness = 0.01
		r = ''
		t = """
		<body id="%(id)s" position="%(pos)s">
			<polygon density="0.1">
				%(poly)s
			</polygon>
			%(tex)s
		</body>
		"""
		jt = """
		<joint type="revolute" enableLimit="true" lowerAngle="-1" upperAngle="1">
			<body1>%(body1)s</body1>
			<body2>%(body2)s</body2>
			<anchor x="%(x)s" y="%(y)s" />
		</joint>
		"""
		vt = '<vertex x="%s" y="%s"/>'
		for k, v in self.model.boneStates.items():
			startpos = self.get_point(k.id, span=0)
			pos = self.get_point(k.id)
			midpoint = self.get_point(k.id, span=0.5)
			relpos = (pos - startpos) / 2
			
			#print 'from ', pos, ' to ', startpos
			
			# Create Shape
			vec = Vector.fromAngle(pos.orientation - math.pi / 2)
			vec.length = _thickness
			poly = ''
			
			# FIXME: only works if facing right
			poly += vt % tuple(relpos - vec)
			poly += vt % tuple(-relpos - vec)
			poly += vt % tuple(-relpos + vec)
			poly += vt % tuple(relpos + vec)
			
			tex = ''
			try:
				skin = self.model.skin.images[k.id]
				if skin.image and skin.image.filename:
					w = skin.image.width * self.scale
					h = skin.image.height * self.scale
					a = pos.orientation
					scale = skin.scale
					tex = '<texture file="%s" width="%s" height="%s" angle="%s" scale="%s" />' % (skin.image.filename, w, h, a, scale)
			except KeyError:
				pass
			
			r += t % {'id':k.id, 'poly':poly, 'tex':tex, 'pos':midpoint}
			
			# Create Joints
			jdata = {'body1':k.id, 'x':pos.x, 'y':pos.y}
			for bone in k.bones:
				jdata['body2'] = bone.id
				r += jt % jdata
				
		r = '<entity><component type="Physics">%s</component></entity>' % r
		#print r
		return xml.fromstring(r)
		
	
	def spawn_ragdoll(self):
		doll = self.get_ragdoll()
		adef = self.actor.world.createActorDef(doll)
		info = self.actor.world.spawn(adef)
		info.set('position', self.actor.get('position'))
		
	def draw(self, d):
		d.pushMatrix()
		d.translate(self.actor.get('position'))
		d.scale(self.scale)
		d.translate(self.offset)
		if self.rotate:
			d.rotate(self.actor.get('angle'))
		self.model.draw(d)
		d.popMatrix()
