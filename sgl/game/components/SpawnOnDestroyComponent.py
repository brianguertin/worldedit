
class ComponentDef:
	def __init__(self):
		self.actordef = ''
		
	def load(self, x):
		x.query(self, 'actordef')
		
	def create(self):
		assert(self.actordef)
		c = Component()
		c.actordef = self.actordef
		return c
		
class Component:
	def __init__(self):
		self.actordef = ''
		
	def init(self):
		self.actor.connect('destroy', self.spawn)
	
	def spawn(self):
		info = self.actor.world.spawn(self.actordef)
		info.set('position', self.actor.get('position'))
		
