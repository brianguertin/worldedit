import math

class ComponentDef:
	def __init__(self):
		self.continuous = False
		
	def load(self, x):
		x.query(self, 'continuous')
		
	def create(self):
		c = Component()
		c.continuous = self.continuous
		return c

class Component:
	def init(self):
		if self.continuous:
			self.actor.connect('update', self.update)
		else:
			self.actor.connect('target_point', self.updateFace)
			
	def update(self, t):
		self.updateFace(self.actor.get('target_point'))
		
	def updateFace(self, point):
		pos = self.actor.get('position')
		angle = (point - (pos + self.actor.get('model.offset') + self.actor.get('model.joint.position', 'head'))).angle
		hangle = 0
	
		MAX_HEAD_ANGLE = 0.5
		
		if point.x < pos.x:
			self.actor.set('face', 'left')
			
			angle = 3.145927 - angle
			
			if angle > 0.5 and angle < 2:
				hangle = min(angle - 0.5, MAX_HEAD_ANGLE)
			elif angle < 5.7 and angle > 4:
				hangle = max(angle - 5.7, -MAX_HEAD_ANGLE)
				
		else:
			self.actor.set('face', 'right')
			
			if angle > 0.5 and angle < 2:
				hangle = min(angle - 0.5, MAX_HEAD_ANGLE)
			elif angle < -0.3:
				hangle = max(angle + 0.3, -MAX_HEAD_ANGLE)
				
		self.actor.set('model.joint.angle', 'head', hangle)
		self.actor.set('model.joint.angle', 'eye', angle)
