
class Component(object):
	def __init__(self):
		self.carrier = None
		self._actions = {
			'set_carrier':0,
			'get_carrier':0,
		}
		
	def set_carrier(self, v):
		self.carrier = v
		
	def get_carrier(self):
		return self.carrier
