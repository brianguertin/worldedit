
class ComponentDef:
	def __init__(self):
		self.damage = 1
		
	def load(self, x):
		x.query(self, 'damage')
		
	def create(self):
		c = Component()
		c.damage = self.damage
		return c
	
class Component:
	def init(self):
		self._actor.connect('collide', self.onCollide)
		
	def onCollide(self, other):
		try:
			other.do('damage', self.damage)
		except AttributeError:
			pass
