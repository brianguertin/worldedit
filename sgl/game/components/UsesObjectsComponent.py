
class Component:
	def init(self):
		self._actor.connect('+use', self.onUse)
		
	def onUse(self):
		closest = None
		closest_distance = None
		for a in self._actor.world.actors:
			if a != self._actor:
				try:
					range = a.get('usable_range')
				except:
					continue
				dist = (a.get('position') - self._actor.get('position')).length
				if dist <= range and (closest == None or dist < closest_range):
					closest = a
					closest_range = dist
		if closest:
			closest.emit('use', self._actor)
