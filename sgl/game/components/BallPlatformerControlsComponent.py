
# FIXME: MUST be event-driven to play nice over network!

class Component:
	def __init__(self):
		self.rot_speed = 20
		self.jump_power = 0.016
		self.accel = 0.15
		self.max = 1.6
		self.airresist = 0.02
		self.moveLeft = 0.0
		self.moveRight = 0.0
		self.sensor = 'ground-sensor'
		self.setAnim = lambda x:None
		
	def init(self):
		#self.sensor = self._actor.do('get_shape_by_name', self.sensor)
		self._actor.connect('+moveleft', self.onMoveLeft)
		self._actor.connect('-moveleft', self.onMoveLeft_r)
		self._actor.connect('+moveright', self.onMoveRight)
		self._actor.connect('-moveright', self.onMoveRight_r)
		self._actor.connect('+moveup', self.onMoveUp)
		self._actor.connect('update', self.update)
		try:
			self.setAnim = self.actor.action('set_anim')
		except:
			pass
		
	def onMoveUp(self):
		if self.is_grounded:
			self._actor.do('impulse', (0, self.jump_power))
		
	def onMoveLeft(self):
		self.moveLeft = 1.0
		
	def onMoveLeft_r(self):
		self.moveLeft = 0.0
		
	def onMoveRight(self):
		self.moveRight = 1.0
		
	def onMoveRight_r(self):
		self.moveRight = 0.0
		
	def update(self, ticks):
		if self.actor.disabled:
			return
		if self.is_grounded: # if grounded
			if self.moveLeft > 0:
				self._actor.do('set_rotation', self.rot_speed)
				self.setAnim('walk')
			elif self.moveRight > 0:
				self._actor.do('set_rotation', -self.rot_speed)
				self.setAnim('walk')
			else:
				self._actor.do('set_rotation', 0)
				self.setAnim('stand')
		else:
			self._actor.do('set_rotation', 0)
			v = self._actor.do('get_velocity')
			if self.moveLeft > 0:
				self.setAnim('walk')
				if -self.max < v.x:
					self._actor.do('set_velocity', (v.x - self.accel,v.y))
			elif self.moveRight > 0:
				self.setAnim('walk')
				if v.x < self.max:
					self._actor.do('set_velocity', (v.x + self.accel,v.y))
			else:
				if v.x < -self.airresist:
					self._actor.do('set_velocity', (v.x + self.airresist,v.y))
				elif v.x > self.airresist:
					self._actor.do('set_velocity', (v.x - self.airresist,v.y))
				self.setAnim('stand')

	@property
	def is_grounded(self):
		return self._actor.do('get_shape_by_name', self.sensor).userData.count > 0
