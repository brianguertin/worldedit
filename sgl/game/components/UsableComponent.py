
class ComponentDef:
	def __init__(self):
		self.range = 0.0
		
	def load(self, x):
		x.query(self, 'range')
		
	def create(self):
		r = Component()
		r.range = self.range
		return r

class Component:
	def __init__(self):
		self.range = 0.0
		self._actions = {
			'get_usable_range':0
		}
		
	def get_usable_range(self):
		return self.range
