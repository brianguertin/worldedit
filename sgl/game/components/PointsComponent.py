from sgl.core.tuples import Point
import math

class ComponentDef:
	def __init__(self):
		self.points = {
		}
		
	def load(self, x):
		for xP in x.findall('point'):
			id = xP.get('id')
			if id:
				p = Point()
				xP.query(p, 'x')
				xP.query(p, 'y')
				self.points[id] = p
		
	def create(self):
		c = Component()
		c.points = self.points
		return c
		
class Component:
	def __init__(self):
		self.points = {
		}
		self._actions = {
			'get_point':0
		}
		
	def get_point(self, name):
		if name not in self.points:
			return Point(0,0)
			
		p = self.points[name]
		
		# Flip horizontally
		if self.actor.get('face') == 'left':
			p = Point(-p.x, p.y)
			
		# Rotate by angle if available
		try:
			a = self.actor.get('angle')
			p = p.rotate(a)
		except:
			pass
			
		return p
