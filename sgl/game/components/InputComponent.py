from sgl.core.signals import Signal

class InputComponentDef:
	def create(self):
		return InputComponent()
		
class InputComponent:
	def __init__(self):
		self.sigCommand = Signal()
		self._signals = {'command':self.sigCommand}
		
	def command(self, cmd, *args, **kargs):
		self.sigCommand.emit(cmd, *args, **kargs)
		
