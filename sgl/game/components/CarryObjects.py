
class CarryObjectsComponent:
	def init(self):
		self.carried_object = None
		self._actor.connect('drop', self.drop)
		self._actor.connect('carry', self.carry)
		self._actor.connect('+action', self.primary)
		self._actor.connect('+secondary', self.secondary)
		
	def primary_action(self):
		if self.carried_object:
			self.carried_object.emit('+action')
			
	def secondary_action(self):
		if self.carried_object:
			self.carried_object.emit('+secondary')
		
	def drop(self):
		self.carried_object = None
		
	def carry(self, obj):
		self.carried_object = obj
