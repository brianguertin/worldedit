
class Component:
	def init(self):
		self._actor.connect('collide', self.onCollide)
		
	def onCollide(self, other):
		self._actor.destroy()
