from sgl.video.texture import Texture

class ComponentDef:
	def __init__(self):
		self.texture = None
		self.x = self.y = self.width = self.height = 0.0
		
	def load(self, x):
		tex = x.get('filename')
		if tex:
			self.texture = Texture(tex)
		x.query(self, 'x')
		x.query(self, 'y')
		x.query(self, 'width')
		x.query(self, 'height')
		
	def create(self):
		assert(self.texture)
		c = Component()
		c.texture = self.texture
		c.x = self.x
		c.y = self.y
		c.width = self.width or self.texture.width
		c.height = self.height or self.texture.height
		return c
		
class Component(object):
	def __init__(self):
		self.texture = None
		self.flip = True
		self.x = self.y = self.width = self.height = 0.0
		
	def init(self):
		self.actor.connect('draw', self.draw)
		
	def draw(self, d):
		d.pushMatrix()
		d.translate((self.x, self.y))
		d.translate(self.actor.get('position'))
		d.color((1.0,1.0,1.0))
		if self.flip and self.actor.get('face') == 'left':
			d.scale(-1,1)
		try:
			d.rotate(self.actor.get('angle'))
		except:
			pass
		d.drawTexture(self.texture, size=(self.width, self.height))
		d.popMatrix()
