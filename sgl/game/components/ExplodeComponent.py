
class ComponentDef:
	def __init__(self):
		self.range = 1
		self.knockback = 0.045
		self.damage = 25
		
	def load(self, x):
		x.query(self, 'range')
		x.query(self, 'knockback')
		x.query(self, 'damage')
		
	def save(self, x):
		x.record(self, 'range')
		x.record(self, 'knockback')
		x.record(self, 'damage')
		
	def create(self):
		c = Component()
		c.range = self.range
		c.knockback = self.knockback
		c.damage = self.damage
		return c

class Component:
	def __init__(self):
		self.range = 0
		self.knockback = 0
		self.damage = 0
		
	def init(self):
		self.range = self.range or 0.00000000000000001
		self.actor.connect('death', self.explode)
		
	def explode(self):
		self.actor.world.playSound('explode.wav')
		pos = self.actor.get('position')
		for a in self.actor.world.actors:
			if a == self.actor:
				continue
			try:
				apos = a.get('position')
			except:
				apos = None
			if apos:
				diff = apos - pos
				dist = diff.length
				if dist < self.range:
					mult = 1 - (dist / self.range)
					imp = diff.normal * self.knockback * mult
					a.attempt('impulse', imp)
					a.attempt('damage', self.damage * mult)
