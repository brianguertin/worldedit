from sgl.game.sprite import Sprite

class ComponentDef:
	def __init__(self):
		self.sprite = None
		self.scale = 0.0
		self.destroy = False
		self.width = 1.0
		self.height = 1.0
		
	def load(self, x):
		x.query(self, 'scale')
		x.query(self, 'width')
		x.query(self, 'height')
		x.query(self, 'destroy')
		filename = x.get('filename')
		self.sprite = filename
		
	def create(self):
		assert(self.sprite)
		c = Component()
		c.sprite = Sprite(self.sprite)
		c.onAnimEnd = c.sprite.onAnimEnd
		c.scale = self.scale
		c.destroy = self.destroy
		c.size = self.width, self.height
		return c
		
class Component:
	def __init__(self):
		self.sprite = None
		self.scale = 0.0
		self.size = (0,0)
		self.destroy = False
		
	def init(self):
		self.actor.connect('update', self.sprite.update)
		self.actor.connect('draw', self.draw)
		if self.destroy:
			self.sprite.onAnimEnd(self.actor.destroy)
		
	def draw(self, d):
		d.pushMatrix()
		d.translate(self.actor.get('position'))
		if self.scale:
			d.scale(self.scale)
			self.sprite.draw(d)
		else:
			d.drawObjectScaled(self.sprite, self.size)
		d.popMatrix()
		
