from sgl.core.tuples import Point

class Component:
	def __init__(self):
		self.position = Point(0,0)
		self.angle = 0.0
		self._actions = {
			'set_position':0,
			'get_position':0,
			'set_angle':0,
			'get_angle':0,
		}
		
	def get_position(self):
		return self.position
		
	def set_position(self, v):
		self.position = Point(v)

	def get_angle(self):
		return self.angle
		
	def set_angle(self, v):
		self.angle = float(v)
