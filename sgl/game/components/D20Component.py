from xml.etree.ElementTree import ElementTree
import os, random
from d20.dice import Dice
from d20.character import Character
from d20.powers import Power, DamageEffect
from d20.weapons import Weapon
from sgl.core.tuples import AABB
from sgl.video.texture import Texture

_log = lambda s: log(s, type='d20')

class ComponentDef:
	def __init__(self):
		self.destroy = False
		self.char = None
		
	def load(self, x):
		x.query(self, 'destroy')
		filename = x.get('filename')
		if filename:
			xChar = ElementTree(file=filename).getroot()
			char = Character()
			char.load(xChar)
			self.char = char
		
	def create(self):
		c = Component()
		c.destroy = self.destroy
		c.char = self.char or Character()
		return c
		
		
class Component:
	def __init__(self):
		self._actions = {
			'get_hp':0,
			'get_mod':0,
			'get_powers':0,
			'get_char':0,
			'damage':0,
			'heal':0,
			'load_character':0,
		}
		
	def init(self):
		self.char.reset()
		self.actor.connect('+action', self.attack)
		self.actor.connect('+cast', self.cast)
		
	def load_character(self, x):
		char = Character()
		char.load(x)
		self.char = char
		log('Loaded %s: %s' % (self.char.base.name, self.char.__dict__))
		
	def get_mod(self, attr):
		return self.char.mod(attr)
		
	def get_hp(self):
		return self.char.hp
		
	def get_powers(self):
		return self.char.powers
		
	def get_char(self):
		return self.char
		
	def heal(self, value, source=None):
		self.char.heal(value, source)
		
		p = self.actor.world.particles.text('+%s' % value, color=(0,255,0))
		p.pos = self.actor.get('position')
		p.life = 1000
		p.vel.y = 0.001
		
		#_log('%s heals %s hp' % (self.actor, value))
		
	def damage(self, value, type=None, source=None):
		self.char.damage(value, type, source)
		
		p = self.actor.world.particles.text('-%s' % value, color=(255,0,0))
		p.pos = self.actor.get('position')
		p.life = 1000
		p.vel.y = 0.001
		
		#_log('%s takes %s %s damage' % (self.actor, value, type or 'normal'))
		
		if self.destroy and self.char.hp < 0 and self.char.hp + value > 0:
			self.actor.destroy()
			
	def attack(self):
		w = self.char.inventory.right_hand
		if not isinstance(w, Weapon):
			w = self.char.inventory.left_hand
		if not isinstance(w, Weapon):
			w = Weapon('Unarmed')
			
		if w.ranged:
			_log('%s attacks with %s (ranged)' % (self.char.base.name, w))
			
			adef = self.actor.world.loadActorDef('entities/d20/missle.xml')
			info = self.actor.world.spawn(adef)
			info.do('ignore_collide', self.actor)
			
			# Set Position
			pos = self.actor.get('position')
			try:
				pos += self.actor.get('model.offset')
				#pos += self.actor.get('point', 'right-forearm')
			except:
				pass
			info.set('position', pos)
			
			# Set its initial velocity
			vel = (self.actor.get('target_point') - pos)
			vel = vel.normal * 6
			info.set('angle', vel.angle)
			info.set('velocity', vel)
			
			info.set('texture', Texture('arrow.png'))
				
			e = DamageEffect()
			e.dice = w.dice
			e.type = 'piercing'
			e.mod = 'dex'
				
			def add_comp(a):
				c = MissileComponent()
				c.caster = self.actor
				c.effects = [e]
				c.weapon = w
				c.attack = 'dex'
				c.defense = 'ac'
				a.add_component(c)
			info.onSpawn.connect(add_comp)
		else:
			_log('%s attacks with %s (melee)' % (self.char.base.name, w))
			
			target, char = self._get_melee_target()
			if target:
				if self.char.attack(char, 'str', 'ac', w):
					d = w.dice.roll(self.char)
					target.do('damage', d)
			
		
	def cast(self, power):
		if not isinstance(power, Power):
			power = Power(power)
		
		spell = SpellFactory(power)
		spell.caster = self
		spell.actor = self.actor
		spell.target = self.actor.get('target')
		spell.target_point = self.actor.get('target_point')
		if spell.is_valid():
			_log('%s performs %s' % (self.char.base.name, power))
			spell.spawn()
		else:
			_log('Invalid target for %s' % power)
		
	def _get_melee_target(self):
		# Get all actors within 1 square of attack direction
		pos = self.actor.get('position')
		aabb = AABB()
		aabb.lowerBound = pos.x, pos.y - 0.5
		aabb.upperBound = pos.x + 1, pos.y + 0.5
		
		if self.actor.get('face') == 'left':
			aabb.lowerBound.x = pos.x - 1
			aabb.upperBound.x = pos.x
		
		actors = self.actor.world.do('get_actors_in_aabb', aabb)
		
		# Attack the first actor found that isn't self (should it be the closest?)
		for a in actors:
			if a != self.actor:
				try:
					target = a.get('char')
					return a, target
				except AttributeError:
					pass
		else:
			return None, None
		
		
class SpellFactory(object):
	def __init__(self, base = None):
		self.base = base
		self.caster = None
		self.actor = None
		self.target = None
		self.target_point = None
		
	@property
	def char(self):
		return self.caster.char
		
	def is_valid(self):
		# TODO: take into account whether an object/creature is required for target
		t = self.base.target
		
		if t in ['close', 'self']:
			return True
			
		if t == 'melee':
			return self._is_valid_melee()
		elif t == 'ranged':
			return self._is_valid_ranged()
		elif t == 'weapon':
			return self._is_valid_melee() or self._is_valid_ranged()
		elif t == 'point':
			if self.actor.get('position').distance(self.target_point) <= self.base.range:
				return True
		elif t == 'creature':
			return True
		return False
		
	def _is_valid_melee(self):
		weapon = self.char.default_weapon
		if weapon and not weapon.ranged:
			if self.target:
				# TODO: take real melee range/reach into account
				dist = self.target.get('position').distance(self.actor.get('position'))
				if dist <= 1:
					return True
			else:
				return True
		return False
				
	def _is_valid_ranged(self):
		weapon = self.char.default_weapon
		if weapon and weapon.ranged:
			if self.target:
				dist = self.target.get('position').distance(self.actor.get('position'))
				if dist <= weapon.range:
					return True
			else:
				return True
		return False
		
	def spawn(self):
		for result, effects in self.base.results:
			typ = result.type
			if typ == 'missle':
				adef = self.actor.world.loadActorDef('entities/d20/missle.xml')
				info = self.actor.world.spawn(adef)
				info.do('ignore_collide', self.actor)
				
				# Set Position
				pos = self.actor.get('position')
				try:
					pos += self.actor.get('model.offset')
					#pos += self.actor.get('point', 'right-forearm')
				except:
					pass
				info.set('position', pos)
				
				# Set its initial velocity
				vel = (self.target_point - pos)
				vel = vel.normal * 6
				info.set('angle', vel.angle)
				info.set('velocity', vel)
				
				if self.base.texture:
					info.set('texture', self.base.texture)
					
				def add_comp(a):
					c = MissileComponent()
					c.caster = self.actor
					c.effects = effects
					c.attack = result.attack
					c.defense = result.defense
					a.add_component(c)
				info.onSpawn.connect(add_comp)
				
			elif typ == 'blast':
				# Apply effects to all actors within range, in attack direction
				pos = self.actor.get('position')
				aabb = AABB()
				aabb.lowerBound = pos.x, pos.y - 0.5
				aabb.upperBound = pos.x + result.range, pos.y + 0.5
				
				if self.actor.get('face') == 'left':
					aabb.lowerBound.x = pos.x - result.range
					aabb.upperBound.x = pos.x
				
				actors = self.actor.world.do('get_actors_in_aabb', aabb)
				
				for a in actors:
					if a != self.actor and a.classname == 'creature':
						if self.base.harmful and self.caster.char.attack(a.get('char'), result.attack, result.defense):
							for ef in effects:
								ef.apply(self.actor, a)
							
			elif typ == 'burst':
				# Apply effects to all actors within range, in both directions
				pos = self.actor.get('position')
				aabb = AABB()
				aabb.lowerBound = pos.x - result.range, pos.y - 0.5
				aabb.upperBound = pos.x + result.range, pos.y + 0.5
				
				actors = self.actor.world.do('get_actors_in_aabb', aabb)
				
				for a in actors:
					if a != self.actor and a.classname == 'creature':
						if self.base.harmful and self.caster.char.attack(a.get('char'), result.attack, result.defense):
							for ef in effects:
								ef.apply(self.actor, a)
				
			elif typ == 'buff':
				if self.target:
					if self.base.harmful and self.caster.get('char').attack(a.get('char'), result.attack, result.defense):
						for ef in effects:
							ef.apply(self.actor, self.target)
				else:
					_log('Target required for %s' % self.base.name)
					
			elif typ == 'melee':
				target, char = self.caster._get_melee_target()
				if target:
					# FIXME: does weapon proficiency count here?
					if self.caster.char.attack(char, result.attack, result.defense):
						for ef in effects:
							ef.apply(self.actor, target)
				
			else:
				raise Exception('Unknown result type %s' % typ)
				
class MissileComponent(object):
	def __init__(self):
		self.effects = None
		self.caster = None
		
		self.attack = None
		self.defense = None
		self.weapon = None
		
	def init(self):
		self.actor.connect('collide', self.collide)
		
	def collide(self, other):
		try:
			if self.caster.get('char').attack(other.get('char'), self.attack, self.defense, self.weapon):
				for effect in self.effects:
					effect.apply(self.caster, other)
		except AttributeError:
			pass
