from sgl.core.tuples import Point

class ComponentDef:
	def __init__(self):
		self.attach = False
		self.attachPoint = (0,0)
		
	def load(self, x):
		x.query(self, 'attach')
		x.query(self, 'attachPoint')
		
	def create(self):
		c = Component()
		c.attach = self.attach
		c.attachPoint = Point(self.attachPoint)
		return c

class Component:
	def __init__(self):
		self.attach = False
		self.controller = None
		self.attachPoint = (0,0)
		self.attachOffset = (0,0)
		
	def init(self):
		self.actor.connect('update', self.update)
		self.actor.connect('use', self.onEnter)
		self.actor.connect('+use', self.onExit)
		
	def update(self, t):
		if self.controller and self.attach:
			self.attachOffset = self.controller.get('model.offset')
			# FIXME: get attached location from model
			a = self.actor.get('angle')
			self.controller.set('position', self.actor.get('position') - self.attachOffset + self.attachPoint.rotate(a))
			self.controller.set('angle', a)
		
	def onEnter(self, other):
		self.controller = other
		other.emit('control', self._actor)
		if self.attach:
			other.disable()
			other.set('anim', 'sit')
			other.set('model.rotate', True)
		
	def onExit(self):
		if self.attach:
			self.controller.enable()
			self.controller.set('model.rotate', False)
		self._actor.emit('exit')
		self.controller = None
