
class ComponentDef:
	def __init__(self):
		self.max = 100.0
		self.destroy = True
		self.lifespan = 0
		
	def load(self, x):
		x.query(self, 'max')
		x.query(self, 'destroy')
		x.query(self, 'lifespan')
		
	def create(self):
		c = Component()
		c.hp = c.max = self.max
		c.destroy = self.destroy
		c.lifespan = self.lifespan
		return c
		
		
class Component:
	def __init__(self):
		self.hp = self.max = 0
		self.alive = True
		self.lifespan = 0
		self._actions = {
			'get_max_hp':0,
			'get_hp':0,
			'set_hp':0,
			'heal':0,
			'damage':0,
		}
		
	def init(self):
		if self.lifespan:
			self._actor.addTimer(self.lifespan, self.kill, times=1)
		
	def get_max_hp(self):
		return self.max
		
	def get_hp(self):
		return self.hp
		
	def set_hp(self, v):
		self.hp = v
		
	def heal(self, v):
		self.hp += v
		self._actor.emit('heal')
		
	def revive(self):
		self.alive = True
		self.actor.emit('revive')
		
	def kill(self):
		self.hp = 0
		self.alive = False
		self._actor.emit('death')
		if self.destroy:
			self._actor.destroy()
		
	def damage(self, v):
		if self.alive:
			self.hp -= v
			self._actor.emit('damage')
			if self.hp <= 0:
				self.kill()
