
class ComponentDef:
	def __init__(self):
		self.duration = 1000
		
	def load(self, x):
		x.query(self, 'duration')
		
	def create(self):
		c = Component()
		c.duration = self.duration
		return c
		
		
class Component:
	def __init__(self):
		self.duration = 0
		
	def init(self):
		self._actor.addTimer(self.duration, self._actor.destroy, times=1)
		
