from sgl.core.tuples import *

# Add ability to target actors seamlessly (get their position when asked)

class Component:
	def init(self):
		self.target = None
		self.target_point = Point(0,0)
		self._actions = {
			'get_target':0,
			'set_target':0,
			'get_target_point':0,
			'set_target_point':0,
		}
		
	def get_target(self):
		return self.target
		
	def set_target(self, obj):
		if obj != self.target:
			self.target = obj
			self.actor.emit('target', obj)
		
	def get_target_point(self):
		return (self.target and not self.target.isDestroyed) and self.target.get('position') or self.target_point
		
	def set_target_point(self, v):
		self.target_point = Point(v)
		self.actor.emit('target_point', self.target_point)
		
