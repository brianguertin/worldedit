from xml.etree.ElementTree import ElementTree

class ComponentDef(object):
	def __init__(self):
		self.default = ''
		self.attach = ''
		self.attachPoint = (0,0)
	
	def load(self, x):
		x.query(self, 'default')
		x.query(self, 'attach')
		x.query(self, 'attachPoint')
		
	def create(self):
		c = Component()
		c.weapon = self.default
		c.attach = self.attach
		c.attachPoint = self.attachPoint
		return c
	
class Component(object):
	def __init__(self):
		self.weapon = None
		self.attach = None
		self.attachPoint = (0,0)
		self._actions = {
			'fire':0,
			'altfire':0,
			'set_weapon':0,
			'get_weapon':0,
		}
		
	def init(self):
		self.actor.connect('+action', self.fire)
		self.actor.connect('+alt', self.altfire)
		self.actor.connect('update', self.update)
		self.actor.connect('target_point', self.set_target)
		self.actor.connect('face', self.face)
		
		if self.weapon:
			self.actor.world.spawn(self.weapon, self.set_weapon)
			self.weapon = None
			
	def face(self, face):
		if self.weapon:
			self.weapon.set('face', face)
		
	def set_target(self, t):
		if self.weapon:
			self.weapon.set('target_point', t)
		
	def update(self, t):
		if self.weapon:
			p = self.actor.get('point', self.attach)
			self.weapon.set('position', self.actor.get('position') + self.attachPoint + p)
			self.weapon.set('angle', p.orientation)
		
	def fire(self):
		if self.weapon:
			self.weapon.emit('+action')
			
	def altfire(self):
		if self.weapon:
			self.weapon.emit('+alt')
		
	def set_weapon(self, obj):
		
		if obj:
			self.actor.set('anim', 'wield', group=1)
			self.weapon = obj
			obj.set('position', self.actor.get('position') + self.attachPoint)
			try:
				obj.set('carrier', self.actor)
			except:
				pass
		else:
			self.weapon = None
			self.actor.set('anim', None, group=1)
			
	def get_weapon(self):
		return self.weapon
		
