from __future__ import with_statement
import new
from sgl.core import compiler, path

def _object_init(self):
	self.actor = None

def _make_object(src):
	exec src
	d = locals()
	d['__init__'] = _object_init
	del d['src']
	return new.classobj('ScriptObject', (object,), locals())
	

class ComponentDef:
	def __init__(self):
		self.compiled = []
		
	def load(self, x):
		i = 0
		for script in x.findall('script'):
			src = script.get('src')
			
			# From file
			if src:
				src = path.find(src)
				with open(src, 'r') as file:
					compiled_script = compiler.compile_literal(file.read(), src=src)
			
			# From literal
			else:
				compiled_script = compiler.compile_literal(script.text, src='<literal%s>' % (i or ''))
				
			try:
				self.compiled.append(_make_object(compiled_script))
			except Exception, e:
				import traceback
				print traceback.format_exc()
		
	def create(self):
		s = Component()
		s.objects = [cls() for cls in self.compiled]
		return s

class Component:
	def __init__(self):
		self.objects = []
		
	def init(self):
		for o in self.objects:
			o.actor = self._actor
			if hasattr(o, 'init'):
				o.init()
				
