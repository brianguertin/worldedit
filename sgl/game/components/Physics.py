from Box2D import *
from sgl.video.texture import Texture
from sgl.core.tuples import *
from copy import copy

_log = lambda x: log(x, type='Box2D')

name = 'Physics'

class AABB:
	def __init__(self):
		self.left = self.right = self.top = self.bottom = 0.0
		
	@property
	def width(self):
		return self.right - self.left
		
	@property
	def height(self):
		return self.top - self.bottom
		
class Manager:
	def __init__(self):
		self._init = False
		self.gravity = b2Vec2(0, -9.98)
		self.doSleep = False
		self.extraSpace = 5.0
		self._actions = {
			'query_aabb':0,
			'get_actors_in_aabb':0,
		}
		
	def init(self):
		assert(not self._init)
		self._init = True
		self.hovering = set()
		self.ticks = 0
		self.timeStep = 1.0 / 60
		worldAABB = b2AABB()
		worldAABB.lowerBound.Set(-self.extraSpace, -self.extraSpace)
		worldAABB.upperBound.Set(self._world.width + self.extraSpace, self._world.height + self.extraSpace)
		self.world = b2World(worldAABB, self.gravity, self.doSleep)
		self.contactListener = ContactListener()
		self.contactFilter = ContactFilter()
		self.world.SetContactListener(self.contactListener)
		self.world.SetContactFilter(self.contactFilter)
		
	def load(self, x):
		x.query(self, 'doSleep')
		xGrav = x.find('gravity')
		if xGrav is not None:
			xGrav.query(self.gravity, 'x')
			xGrav.query(self.gravity, 'y')
		
	def update(self, ticks):
		self.ticks += ticks
		while self.ticks >= self.timeStep * 1000:
			self.ticks -= self.timeStep * 1000
			for b in self.hovering:
				g = b2Vec2()
				g.x = -b.GetMass() * self.gravity.x
				g.y = -b.GetMass() * self.gravity.y
				b.ApplyForce(g, b.position)
			self.world.Step(self.timeStep, 10, 8)
			
	def query_aabb(self, aabb, max=30):
		b = b2AABB()
		b.lowerBound = aabb.lowerBound
		b.upperBound = aabb.upperBound
		
		count, shapes = self.world.Query(b, max)
		return shapes
		
	def get_actors_in_aabb(self, aabb):
		shapes = self.query_aabb(aabb)
		r = set()
		for shape in shapes:
			r.add(shape.GetBody().userData.parent.actor)
		return list(r)
			
class ContactFilter(b2ContactFilter):
	def __init__(self):
		super(ContactFilter, self).__init__()
		
	def ShouldCollide(self, shape1, shape2):
		collide = super(ContactFilter, self).ShouldCollide(shape1, shape2)
		if collide:
			c1 = shape1.GetBody().userData.parent
			c2 = shape2.GetBody().userData.parent
			if c1 and c2:
				a1 = c1.actor
				a2 = c2.actor
				if a2 in c1.no_collide or a1 in c2.no_collide:
					return False
				for fn in c1.collision_filters:
					if not fn(a1, a2):
						return False
				for fn in c2.collision_filters:
					if not fn(a2, a1):
						return False
		return collide
			
class ContactListener(b2ContactListener):
	def __init__(self):
		super(ContactListener, self).__init__()
	def Add(self, point):
		"""Handle add point"""
		
		self.checkSensor(point, 1)
		
		data1 = point.shape1.GetBody().userData
		data2 = point.shape2.GetBody().userData
		
		if data1.isPlatform:
			if not self.handlePlatform(point):
				return
		elif data2.isPlatform:
			if not self.handlePlatformReverse(point):
				return
		
			
		c1 = data1.parent
		c2 = data2.parent
		if not (c1.actor.isDestroyed or c2.actor.isDestroyed):
			c1.collide(c2)
			c2.collide(c1)
		
	def Persist(self, point):
		"""Handle persist point"""
		pass
		
	def Remove(self, point):
		"""Handle remove point"""
		#data1 = point.shape1.GetBody().userData
		#data2 = point.shape2.GetBody().userData
		#if data1.isPlatform or data2.isPlatform:
		#	return
		if hasattr(point, 'contact'):
			contact = point.contact
			if contact.GetManifoldCount() == 0:
				contact.flags &= ~b2Contact.e_nonSolidFlag
			
		self.checkSensor(point, -1)
		
	def Result(self, point):
		"""Handle results"""
		pass
		
	def checkSensor(self, point, mod):
		if point.shape1.isSensor and not point.shape2.isSensor:
			self.modifySensorCount(point.shape1, mod)
		elif point.shape2.isSensor and not point.shape1.isSensor:
			self.modifySensorCount(point.shape2, mod)
		
	def modifySensorCount(self, shape, mod):
		if shape.userData:
			shape.userData.count += mod
			
	def handlePlatform(self, point):
		if not hasattr(point, 'contact'):
			return True
		contact = point.contact
		manifold = contact.GetManifolds()
		
		#log('contact! %s' % manifold.normal)
		
		if manifold.normal.y <= 0:
			contact.flags |= b2Contact.e_nonSolidFlag
			return False
		else:
			return True
			
	def handlePlatformReverse(self, point):
		if not hasattr(point, 'contact'):
			return True
		contact = point.contact
		manifold = contact.GetManifolds()
		
		#log('-contact! %s' % manifold.normal)
		
		if manifold.normal.y >= 0:
			contact.flags |= b2Contact.e_nonSolidFlag
			return False
		else:
			return True
		
class BodyData:
	def __init__(self):
		self.draw = False
		self.texture = None
		self.texSize = Size(0,0)
		self.texAngle = 0
		self.texScale = 1.0
		self.parent = None
		self.isPlatform = False
		
	def __repr__(self):
		if self.parent:
			return '<BodyData: With Parent>'
		else:
			return '<BodyData: No Parent>'
		
class SensorData:
	def __init__(self):
		self.count = 0

class BodyShapesDef:
	def __init__(self):
		self.id = ''
		self.dynamic = True
		self.bodyDef = b2BodyDef()
		#self.bodyDef.fixedRotation = True
		self.shapeDefs = []
		self.shapeIDs = []
		
	def load(self, x):
		x.query(self, 'id')
		x.query(self, 'dynamic')
		pos = x.find('position')
		if pos is None:
			pos = x.get('position')
			if pos:
				self.bodyDef.position = Point(pos)
		else:
			pos.query(self.bodyDef.position, 'x')
			pos.query(self.bodyDef.position, 'y')
		x.query(self.bodyDef, 'linearDamping')
		x.query(self.bodyDef, 'angularDamping')
		x.query(self.bodyDef, 'angle')
		x.query(self.bodyDef, 'bullet', 'isBullet')
		x.query(self.bodyDef, 'sleep', 'allowSleep')
		x.query(self.bodyDef, 'rotate', 'fixedRotation', inverse=True)
		
		data = self.bodyDef.userData = BodyData()
		x.query(data, 'draw')
		x.query(data, 'isPlatform')
		
		xTex = x.find('texture')
		if xTex is not None:
			file = xTex.get('file')
			if file:
				data.texture = Texture(file)
				data.texSize.x = xTex.get('width') or data.texture.width
				data.texSize.y = xTex.get('height') or data.texture.height
				data.texAngle = float(xTex.get('angle') or 0)
				data.texScale = float(xTex.get('scale') or 1)
		
		for xPoly in x.findall('polygon'):
			d = b2PolygonDef()
			xPoly.query(d, 'density')
			xPoly.query(d, 'restitution')
			xPoly.query(d, 'friction')
			xPoly.query(d, 'sensor', 'isSensor')
			
			xVertices = xPoly.findall('vertex')
			assert(len(xVertices) <= 8)
			vertices = []
			for xVertex in xVertices:
				v = b2Vec2()
				xVertex.query(v, 'x')
				xVertex.query(v, 'y')
				vertices.append(v)
			d.setVertices(vertices)
				
			self.shapeDefs.append(d)
			self.shapeIDs.append(xPoly.get('id') or '')
			
		for xCircle in x.findall('circle'):
			d = b2CircleDef()
			xCircle.query(d, 'radius')
			xCircle.query(d, 'density')
			xCircle.query(d, 'restitution')
			xCircle.query(d, 'friction')
			xCircle.query(d, 'sensor', 'isSensor')
			self.shapeDefs.append(d)
			self.shapeIDs.append(xCircle.get('id') or '')
		
		for xPoly in x.findall('pipe'):
			d = b2PolygonDef()
			xPoly.query(d, 'density')
			xPoly.query(d, 'restitution')
			xPoly.query(d, 'friction')
			xPoly.query(d, 'sensor', 'isSensor')
			id = xPoly.get('id') or ''
			
			
			radius = float(xPoly.get('radius'))
			segments = int(xPoly.get('segments') or 5)
			padding = float(xPoly.get('padding') or 0)
			start = float(xPoly.get('start') or 0.5) * math.pi * 2
			end = float(xPoly.get('end') or 0.75) * math.pi * 2
			diff = end - start
			inc = diff / segments
			i = start
			p1 = p2 = Vector.fromAngle(i) * radius
			while i < end:
				p1 = p2
				i += inc
				p2 = Vector.fromAngle(i) * radius
				
				if i < math.pi * 0.5 or i > math.pi * 1.5:
					x = radius + padding
				else:
					x = -radius - padding
				
				vertices = []
				vertices.append((x, p1.y))
				vertices.append((x, p2.y))
				vertices.append(p2)
				vertices.append(p1)
				
				df = b2PolygonDef()
				df.density = d.density
				df.restitution = d.restitution
				df.friction = d.friction
				df.isSensor = d.isSensor
				df.setVertices(vertices)
					
				self.shapeDefs.append(df)
				self.shapeIDs.append(id)
			
	def save(self, x):
		if self.id:
			x.record(self, 'id')
		x.record(self, 'dynamic')
		x.record(self.bodyDef, 'linearDamping')
		x.record(self.bodyDef, 'angularDamping')
		x.record(self.bodyDef, 'angle')
		x.record(self.bodyDef, 'bullet', 'isBullet')
		x.record(self.bodyDef, 'sleep', 'allowSleep')
		x.record(self.bodyDef, 'rotate', 'fixedRotation', inverse=True)
		
		xPos = x.makeelement('position')
		xPos.record(self.bodyDef.position, 'x')
		xPos.record(self.bodyDef.position, 'y')
		x.append(xPos)
		
		for sdef in self.shapeDefs:
			if isinstance(sdef, b2PolygonDef):
				xShape = x.makeelement('polygon')
				for v in sdef.vertices:
					xV = x.makeelement('vertex')
					xV.set('x', v[0])
					xV.set('y', v[1])
					xShape.append(xV)
			elif isinstance(sdef, b2CircleDef):
				xShape = x.makeelement('circle')
				xShape.record(sdef, 'radius')
			
			xShape.record(sdef, 'density')
			xShape.record(sdef, 'restitution')
			xShape.record(sdef, 'friction')
			xShape.record(sdef, 'sensor', 'isSensor')
			x.append(xShape)
		
		data = self.bodyDef.userData
		x.record(data, 'draw')
		if data.texture:
			xTex = x.makeelement('texture')
			xTex.set('file', data.texture.filename)
			xTex.set('width', data.texSize.x)
			xTex.set('height', data.texSize.y)
			x.append(xTex)

class RevoluteJointDefiner:
	def __init__(self):
		self.id = ''
		self.body1_id = ''
		self.body2_id = ''
		self.anchorBody = 2
		self.anchor = None
		
		self.lowerAngle = 0
		self.upperAngle = 0
		self.enableLimit = False
		
		self.maxMotorTorque = 0.0
		self.motorSpeed = 0.0
		self.enableMotor = True
		
	def create(self, bodies):
		body1 = bodies[self.body1_id]
		if self.body2_id:
			body2 = bodies[self.body2_id];
		if self.anchor:
			anchor = self.anchor
		else:
			if self.anchorBody == 1:
				anchor = body1.GetWorldCenter()
			else:
				anchor = body2.GetWorldCenter()
				
		jdef = b2RevoluteJointDef()
		jdef.Initialize(body1, body2, anchor)
		jdef.maxMotorTorque = self.maxMotorTorque
		jdef.motorSpeed = self.motorSpeed
		jdef.enableMotor = self.enableMotor
		jdef.enableLimit = self.enableLimit
		jdef.lowerAngle = self.lowerAngle
		jdef.upperAngle = self.upperAngle
		return jdef
		
class FilterData:
	def __init__(self):
		self.groupIndex = 0
		self.categoryBits = 0
		self.maskBits = 0
		
class ComponentDef:
	def __init__(self):
		self.defs = []
		self.jointDefiners = []
		self.filter = FilterData()
		self.gravity = True
		
	def load(self, x):
		x.query(self, 'gravity')
		x.query(self.filter, 'group', 'groupIndex')
		x.query(self.filter, 'categoryBits')
		x.query(self.filter, 'maskBits')
		for xBody in x.findall('body'):
			d = BodyShapesDef()
			d.load(xBody)
			self.defs.append(d)
		for xJoint in x.findall('joint'):
			type = xJoint.get('type')
			if type == 'revolute':
				d = RevoluteJointDefiner()
				
				# Motor
				xJoint.query(d, 'maxMotorTorque')
				xJoint.query(d, 'motorSpeed')
				xJoint.query(d, 'enableMotor')
				
				# Limit
				xJoint.query(d, 'enableLimit')
				xJoint.query(d, 'lowerAngle')
				xJoint.query(d, 'upperAngle')
				
				# Anchor
				xAnchor = xJoint.find('anchor')
				if xAnchor is not None:
					d.anchor = b2Vec2()
					xAnchor.query(d.anchor, 'x')
					xAnchor.query(d.anchor, 'y')
				else:
					c = xJoint.find('center')
					if c:
						if c.text == 'body1':
							d.anchorBody = 1
						elif c.text == 'body2':
							d.anchorBody = 2
				info = d
			else:
				_log('Unknown joint type: %s' % type)
				continue
			xJoint.query(info, 'id')
			xJoint.query(info, 'body1', 'body1_id')
			xJoint.query(info, 'body2', 'body2_id')
			#info.body1_id = xJoint.find('body1').text
			#info.body2_id = xJoint.find('body2').text
			self.jointDefiners.append(info)
			
	
	def save(self, x):
		x.set('gravity', self.gravity)
		x.set('group', self.filter.groupIndex)
		x.set('categoryBits', self.filter.categoryBits)
		x.set('maskBits', self.filter.maskBits)
		for d in self.defs:
			xBody = x.makeelement('body')
			d.save(xBody)
			x.append(xBody)
		for j in self.jointDefiners:
			xJoint = x.makeelement('joint')
			if isinstance(j, RevoluteJointDefiner):
				xJoint.set('type', 'revolute')
				if j.anchor:
					xAnchor = x.makeelement('anchor')
					xAnchor.set('x', j.anchor.x)
					xAnchor.set('y', j.anchor.y)
					xJoint.append(xAnchor)
			xJoint.set('body1', j.body1_id)
			xJoint.set('body2', j.body2_id)
			x.append(xJoint)
			
	def create(self):
		c = Component()
		c.proto = self
		c.gravity = self.gravity
		return c
		
class Component:
	def __init__(self):
		self.proto = None
		self.bodies = []
		self.joints = []
		self.jointDict = {}
		self.bodyDict = {}
		self.shapeDict = {}
		self.group = 0
		self.no_collide = []
		self.collision_filters = []
		self._actions = {
			'get_position':0,
			'set_position':0,
			'get_angle':0,
			'set_angle':0,
			'set_rotation':0,
			'get_velocity':0,
			'set_velocity':0,
			'set_texture':0,
			'get_aabb':0,
			'impulse':0,
			'get_shape_by_name':'getShapeByName',
			'applyForce':0,
			'applyTorque':0,
			'ignore_collide':0,
			'notice_collide':0,
			'add_collision_filter':0,
			'get_joint':0,
			'add_body_from_xml':0,
			'add_bodies_from_xml':0,
			'remove_body_by_id':0,
		}
		
	@property
	def world(self):
		return self.componentManager.world
		
	def _addBody(self, d):
		b = self.world.CreateBody(d.bodyDef)
		for i, sdef in zip(range(len(d.shapeDefs)), d.shapeDefs):
			if self.proto.filter.groupIndex:
				sdef.filter.groupIndex = self.proto.filter.groupIndex
			if self.proto.filter.categoryBits:
				sdef.filter.categoryBits = self.proto.filter.categoryBits
			if self.proto.filter.maskBits:
				sdef.filter.maskBits = self.proto.filter.maskBits
			s = b.CreateShape(sdef)
			if s.isSensor:
				s.userData = SensorData()
			id = d.shapeIDs[i]
			if id:
				self.shapeDict[id] = s
		if d.dynamic:
			b.SetMassFromShapes()
		b.userData = copy(b.userData)
		b.userData.parent = self
		self.bodies.append(b)
		
		if d.id:
			self.bodyDict[d.id] = b
		return b
			
	def _addJoint(self, d):		
		jdef = d.create(self.bodyDict)
		j = self.world.CreateJoint(jdef).getAsType()
		self.joints.append(j)
		if d.id:
			self.jointDict[d.id] = j
		
	def init(self):
		self._actor.connect('draw', self.draw)
		self._actor.connect('disable', self.disable)
		self._actor.connect('enable', self.enable)
		self._actor.connect('serialize', self.serialize)
		self._actor.connect('deserialize', self.deserialize)
		
		for d in self.proto.defs:
			self._addBody(d)
			
		if len(self.bodies) < 1:
			log('Number of bodies should be at least 1', type='Physics', level=1)
		
		for d in self.proto.jointDefiners:
			self._addJoint(d)
			
		if not self.gravity:
			for b in self.bodies:
				self.componentManager.hovering.add(b)
		
	def _destroy(self):
		for j in self.joints:
			self.world.DestroyJoint(j)
		self.joints = []
		for b in self.bodies:
			if b in self.componentManager.hovering:
				self.componentManager.hovering.remove(b)
			self.world.DestroyBody(b)
		self.bodies = []
		
	def save(self, x):
		for b in self.bodies:
			xBody = x.makeelement('body',{})
			
			# Save Position
			pos = b.position
			xPos = xBody.makeelement('position',{})
			xPos.set('x', pos.x)
			xPos.set('y', pos.y)
			xBody.append(xPos)
			
			# Save Shapes
			for s in b.shapeList:
				if isinstance(s, b2CircleShape):
					xShape = xBody.makeelement('circle', {})
					pos = s.localPosition
					pos.x += b.position.x
					pos.y += b.position.y
					xShape.set('radius', s.radius)
				elif isinstance(s, b2PolygonShape):
					xShape = xBody.makeelement('polygon', {})
					for v in s.vertices:
						xVertex = xShape.makeelement('vertex', {})
						xVertex.set('x', v[0])
						xVertex.set('y', v[1])
						xShape.append(xVertex)
				xShape.set('density', s.density)
				xShape.set('restitution', s.restitution)
				xShape.set('friction', s.friction)
				if s.isSensor:
					xShape.set('sensor', 'true')
				xBody.append(xShape)
				
			# Save Texture
			data = b.userData
			if data.draw:
				xBody.set('draw', 'true')
			if data.texture:
				xTex = xBody.makeelement('texture', {})
				xTex.set('file', data.texture.filename)
				xTex.set('width', data.texSize[0])
				xTex.set('height', data.texSize[1])
				xBody.append(xTex)
			
			x.append(xBody)
			
	def serialize(self, d):
		d['position'] = self.get_position()
		d['velocity'] = self.get_velocity()
		
	def deserialize(self, d):
		self.set_position(d['position'])
		self.set_velocity(d['velocity'])
			
	def add_body_from_xml(self, x):
		b = BodyShapesDef()
		b.load(x)
		self._addBody(b)
		
	def add_bodies_from_xml(self, x):
		for xBody in x.find('body'):
			b = BodyShapesDef()
			b.load(xBody)
			b = self._addBody(b)
			b.position += self.get_position()
		
	def remove_body_by_id(self, id):
		try:
			b = self.bodyDict[id]
			self.bodies.remove(b)
			del self.bodyDict[id]
			
			if b in self.componentManager.hovering:
				self.componentManager.hovering.remove(b)
			self.world.DestroyBody(b)
			
		except KeyError:
			pass
		
	def enable(self):
		self.collisionEnabled = True
		for b in self.bodies:
			for s in b.shapeList:
				s.filter.maskBits = 65535
				s.filter.categoryBits = 1
				self.world.Refilter(s)
			b.WakeUp()
	
	def disable(self):
		self.collisionEnabled = False
		for b in self.bodies:
			for s in b.shapeList:
				s.filter.maskBits = 0
				s.filter.categoryBits = 0
				self.world.Refilter(s)
			b.PutToSleep()
			
	def add_collision_filter(self, fn):
		pass #self.collision_filters.append(fn)
		
	def notice_collide(self, other):
		if other in self.no_collide:
			self.no_collide.remove(other)
			
	def ignore_collide(self, other):
		if other not in self.no_collide:
			self.no_collide.append(other)
			
	def draw(self, display):
		for b in self.bodies:
			data = b.userData
			if data.draw:
				renderBody(b, display)
			if data.texture:
				display.color((1.0, 1.0, 1.0))
				display.pushMatrix()
				display.translate((b.position.x, b.position.y))
				display.rotate(b.angle)
				display.rotate(data.texAngle)
				display.scale(data.texScale)
				display.drawTexture(data.texture, data.texSize)
				t = data.texture
				display.popMatrix()
		#display.color(0, 1, 0);
		#display.drawRectangle(this.aabb);
		
	def set_texture(self, tex, body = 0):
		self.bodies[body].userData.texture = tex
		
	def get_joint(self, name):
		if name in self.jointDict:
			return self.jointDict[name]
		
	def get_rotation(self):
		return self.bodies[0].angularVelocity
		
	def set_rotation(self, v):
		self.bodies[0].angularVelocity = v
		
	rotation = property(get_rotation, set_rotation)
	
	def get_position(self, body = 0):
		return Point(self.bodies[body].position)
		
	def set_position(self, v, body = 0):
		v = Point(v)
		mod_x = v.x - self.bodies[body].position.x
		mod_y = v.y - self.bodies[body].position.y
		for b in self.bodies:
			b.position = (b.position.x + mod_x, b.position.y + mod_y)
		
	def get_angle(self, body = 0):
		return self.bodies[body].angle
		
	def set_angle(self, v, body = 0):
		self.bodies[body].angle = v
		
	def impulse(self, v, body = 0):
		self.bodies[body].ApplyImpulse(v, self.bodies[body].position)
		
	def get_velocity(self):
		return Point(self.bodies[0].GetLinearVelocity())
		
	def set_velocity(self, v):
		self.bodies[0].SetLinearVelocity(v)
	
	def applyForce(self, v):
		self.bodies[0].ApplyForce(v, self.bodies[0].position)
		
	def applyTorque(self, v):
		self.bodies[0].ApplyTorque(v)
		
	def collide(self, other):
		self._actor.world.trigger('collide', object=self._actor, other=other._actor)
		self._actor.emit('collide', other._actor)
			
	def _collide(self, other):
		if other._actor not in self.no_collide:
			for filter in self.collision_filters:
				if not filter(self._actor, other._actor):
					return
			self._actor.emit('collide', other._actor)
		
	def getShapeByName(self, name):
		return self.shapeDict[name]
	
	def get_aabb(self):
		r = AABB()
		r.left = r.bottom = 99999999999.0
		r.top = r.right = -9999999999.0
		for body in self.bodies:
			bpos = body.position;
			for s in body.shapeList:
				if isinstance(s, b2CircleShape):
					pos = s.localPosition
					pos.x += bpos.x
					pos.y += bpos.y
					rad = s.radius
					r.left = min(r.left, pos.x - rad)
					r.bottom = min(r.bottom, pos.y - rad)
					r.right = max(r.right, pos.x + rad)
					r.top = max(r.top, pos.y + rad)
				elif isinstance(s, b2PolygonShape):
					for v in s.vertices:
						r.left = min(r.left, v[0] + bpos.x)
						r.right = max(r.right, v[0] + bpos.x)
						r.bottom = min(r.bottom, v[1] + bpos.y)
						r.top = max(r.top, v[1] + bpos.y)
		return r

def renderBody(body, display):
	display.pushMatrix()
	pos = body.position
	display.translate((pos.x, pos.y))
	display.rotate(body.angle)
	renderBodyOnly(body, display)
	display.popMatrix()
	

def renderBodyOnly(body_, display):
	for s in body_.shapeList:
		if isinstance(s, b2PolygonShape):
			vertices = s.vertices
			if len(vertices) > 3:
				display.color((0.4, 0.4, 1.0))
			else:
				display.color((0.0, 1.0, 0.0))
				
			display.drawFilledPolygon(vertices)
			
			if len(vertices) > 3:
				display.color((0.0, 0.0, 0.5))
			else:
				display.color((0.0, 0.5, 0.0))
			display.drawPolygon(vertices)
		elif isinstance(s, b2CircleShape):
			radius = s.radius
			display.color((1.0, 0.5, 1.0))
			display.drawFilledCircle(radius)
			display.color((0.5, 0.0, 0.5))
			display.drawCircle(radius)
			display.drawLine(Point(radius,0))
			#display.drawLine(Point(-radius, 0), Point(radius, 0))
		else:
			raise Exception('Don\'t know how to draw' % s)
