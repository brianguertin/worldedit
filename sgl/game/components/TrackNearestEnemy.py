
class Component:
	def init(self):
		self._actor.addTimer(150, self.updateTarget)
		#self.updateTarget()
		
	def updateTarget(self):
		enemies = self._actor.getEnemies()
		if len(enemies):
			pos = self._actor.get('position')
			nearest = enemies[0]
			nearest_distance = pos.distance(enemies[0].get('position'))
			for enemy in enemies[1:]:
				dist = pos.distance(enemy.get('position'))
				if dist < nearest_distance:
					nearest_distance = dist
					nearest = enemy
					
			self._actor.set('target', nearest)
		else:
			self._actor.set('target', None)
