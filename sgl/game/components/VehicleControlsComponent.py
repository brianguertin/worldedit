
class Component:
	def __init__(self):
		self.moveLeft = 0
		self.moveRight = 0
		
	def init(self):
		self._actor.connect('+moveleft', self.onMoveLeft)
		self._actor.connect('-moveleft', self.onMoveLeft_r)
		self._actor.connect('+moveright', self.onMoveRight)
		self._actor.connect('-moveright', self.onMoveRight_r)
		self._actor.connect('update', self.update)

	def onMoveLeft(self):
		self.moveLeft = 1.0
		
	def onMoveLeft_r(self):
		self.moveLeft = 0.0
		
	def onMoveRight(self):
		self.moveRight = 1.0
		
	def onMoveRight_r(self):
		self.moveRight = 0.0
		
	def update(self, ticks):
		if self.moveRight > 0 and self.moveRight > self.moveLeft:
			self._actor.do('applyForce', (0.1, 0))
		elif self.moveLeft > 0:
			self._actor.do('applyForce', (-0.1, 0))
