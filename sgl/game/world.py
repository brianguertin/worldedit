from sgl.game.actor import *
from xml.etree.ElementTree import ElementTree, Element
from sgl.core import path
from sgl.video.texture import Texture
from sgl.audio import music
from sgl.audio.sound import Sound
from sgl.core.tuples import Point
from sgl.game.faction import Faction
from sgl.core.signals import Signal
from sgl.game.tilemap import Tilemap
from sgl.game.events import EventSystem
from sgl.video.particles import ParticleSystem

_log = lambda x: log(x, type='World')

MAX_ACTORDEF_INHERIT = 5

class ActorNotFound(Exception):
	def __init__(self, *args, **kargs):
		super(ActorNotFound, self).__init__(*args, **kargs)
		
class _Factory:
	def __init__(self, cls):
		self.cls = cls
		
	def create(self):
		return self.cls()
		
def _F(cls):
	def fn():
		return _Factory(cls)
	return fn

class ActorSpawnInfo:
	def __init__(self, adef=None):
		self.adef = adef
		self.actions = []
		self.onSpawn = Signal()
		
	def do(self, name, *args, **kargs):
		def fn(actor):
			actor.do(name, *args, **kargs)
		self.actions.append(fn)
		
	def set(self, name, *args, **kargs):
		def fn(actor):
			actor.set(name, *args, **kargs)
		self.actions.append(fn)
		
	def connect(self, signal, callback):
		def fn(actor):
			actor.connect(signal, callback)
		self.actions.append(fn)

class Background:
	def __init__(self):
		self.texture = None
		self.width = 0
		self.height = 0
		self.opacity = 1.0

class ComponentFactory:
	def __init__(self, fn, type, cdef=None):
		self.fn = fn
		self.type = type
		self.cdef = cdef
		
	def __call__(self, *args, **kargs):
		return self.fn(*args, **kargs)

class Player(object):
	def __init__(self):
		pass

class World(object):
	def __init__(self, filename = None, builtins=True):
		self.particles = ParticleSystem()
		self.x = self.y = self.width = self.height = 0
		self.source = ''
		self._isUpdating = False
		self._entityCount = 0
		self.actors = []
		self.disabledActors = []
		self.destroyedActors = []
		self.queuedSpawns = []
		self.componentFactories = {}
		self.managerFactories = {}
		self.managers = {}
		#self.components = []
		self.backgrounds = []
		self.tilemap = None
		self.factions = []
		self.actordefs = {}
		self.view = None
		self.events = EventSystem()
		self.players = []
		if builtins:
			from sgl.game import components
			reload(components)
			components.register_builtin_components(self)
		if filename:
			self.source = filename
			doc = ElementTree(file=filename)
			self.load(doc.getroot())
		
	def load(self, xElem):
		xElem.query(self, 'x')
		xElem.query(self, 'y')
		xElem.query(self, 'width')
		xElem.query(self, 'height')
		
		xMusic = xElem.find('music')
		if xMusic is not None:
			music.play(xMusic.get('filename'))
		
		for xBG in xElem.findall('background'):
			b = Background()
			b.texture = Texture(xBG.get('filename'))
			xBG.query(b, 'layer')
			xBG.query(b, 'opacity')
			w, h = xBG.get('width'), xBG.get('height')
			if w:
				if w.endswith('%'):
					b.width = self.width * 0.01 * float(w[:-1])
				else:
					b.width = float(w)
			else:
				b.width = self.width
			if h:
				if h.endswith('%'):
					b.height = self.height * 0.01 * float(h[:-1])
				else:
					b.height = float(h)
			else:
				b.height = self.height
			self.backgrounds.append(b)
				
		xTilemap = xElem.find('tilemap')
		if xTilemap is not None:
			tmap = Tilemap()
			tmap.load(xTilemap)
			self.tilemap = tmap
			
		# Load Factions
		for xFaction in xElem.findall('faction'):
			name = xFaction.get('name')
			if not name:
				_log('ERROR: Faction must have a name')
			f = self.getFaction(name)
			for xEnemy in xFaction.findall('enemy'):
				n = xEnemy.get('name')
				if n:
					f.enemies.append(self.getFaction(n))
			for xFriend in xFaction.findall('friend'):
				n = xFriend.get('name')
				if n:
					f.friends.append(self.getFaction(n))
			
		# Load Managers
		for xManager in xElem.findall('manager'):
			type = xManager.get('type')
			m = self.getManager(type, init=False)
			if hasattr(m, 'load'):
				m.load(xManager)
			if hasattr(m, 'init'):
				m.init()
			
		# Load Actor Defs
		for xEntity in xElem.findall('actordef'):
			name = xEntity.get('name')
			if name:
				e = ActorDef()
				e.world = self
				e.load(xEntity)
				e.filename = '@%s' % name
				self.actordefs[e.filename] = e
				_log('Loaded local ActorDef "%s"' % name)
			else:
				_log('Name is required for actordef element')
				
		# Load Actors
		for xEntity in xElem.findall('actor'):
			try:
				templ = xEntity.get('template')
				if not templ:
					raise Exception('No template specified')
				e = self.loadActorDef(templ)
				info = self.spawn(e)
				
				id = xEntity.get('id')
				if id:
					info.set('id', id)
				for xSet in xEntity.findall('set'):
					var = xSet.get('var')
					val = xSet.get('value')
					if var and val:
						try:
							val = eval(val)
						except:
							pass
					info.set(var, val)
					
			except Exception, e:
				log('Failed creating entity: [%s] %s' % (e.__class__.__name__, e), level=1, type='World')
				
		# Load Events
		self.events.load(xElem)

		self.doSpawns()
		
	def save(self, filename):
		from os import system
		tree = ElementTree()
		xWorld = tree._root = Element('world')
		xWorld.set('width', self.width)
		xWorld.set('height', self.height)
		
		# Save Backgrounds
		for bg in self.backgrounds:
			xBG = Element('background')
			xBG.set('filename', bg.texture.filename)
			xBG.set('width', bg.width)
			xBG.set('height', bg.height)
			xBG.set('opacity', bg.opacity)
			xWorld.append(xBG)
			
		if self.tilemap:
			xTilemap = Element('tilemap')
			self.tilemap.save(xTilemap)
			xWorld.append(xTilemap)
			
		# Save Factions
		for faction in self.factions:
			xFaction = Element('faction')
			xFaction.set('name', faction.name)
			for f in faction.friends:
				x = Element('friend')
				x.set('name', f.name)
				xFaction.append(x)
			for f in faction.enemies:
				x = Element('enemy')
				x.set('name', f.name)
				xFaction.append(x)
			xWorld.append(xFaction)
			
		# TODO: Save Managers
		
		# TODO: Save Actor Defs
		for name, adef in self.actordefs.items():
			if name.startswith('@'):
				xActorDef = Element('actordef')
				xActorDef.set('name', name[1:])
				for name, c in adef.components.items():
					xComp = Element('component')
					xComp.set('type', name)
					if hasattr(c, 'save'):
						c.save(xComp)
					xActorDef.append(xComp)
				xWorld.append(xActorDef)
		
		# Save Entities
		for actor in self.actors:
			xActor = Element('actor')
			if actor.id:
				xActor.set('id', actor.id)
			if actor.faction:
				xActor.set('faction', actor.faction.name)
				
			xActor.set('template', actor.template)
			for prop in actor.properties:
				try:
					val = actor.get(prop)
					if val is not None:
						xSet = Element('set')
						xSet.set('var', prop)
						xSet.set('value', str(val))
						xActor.append(xSet)
				except:
					pass
			xWorld.append(xActor)
		
		tree.getroot().cleanup()
		tree.write(filename)
		system('xmlindent -w %s' % filename)
		
	def action(self, name):
		for c in self.managers.values():
			if hasattr(c, '_actions') and name in c._actions:
				return getattr(c, c._actions[name] or name)
		else:
			raise Exception('No action "%s" in world' % name)
				
	def do(self, name, *args, **kargs):
		return self.action(name)(*args, **kargs)
		
	# FIXME: dont use ambiguous names here?
	def get(self, name, default=None):
		return self.events._vars.get(name, default)
		
	def set(self, name, value):
		self.events._vars[name] = value
		
	def createActorDef(self, x):
		# TODO: load base actordef from templates
		a = ActorDef()
		a.world = self
		a.load(x)
		return a

	def loadNewActorDef(self, fname, level=0):
		if level > MAX_ACTORDEF_INHERIT:
			raise Exception('ActorDef inheritence limit reached')
		filename = path.find(fname)
		
		x = ElementTree(file=filename).getroot()
		templ = x.get('template')
		
		path.add(path.dirname(filename))
		try:
			if templ:
				d = self.loadNewActorDef(templ, level=level+1)
			else:
				d = ActorDef()
			d.world = self
		
			d.load(x)
		finally:
			path.pop()
		d.filename = filename
		
		return d
		
	def loadActorDef(self, fname):
		if fname.startswith('@'):
			if fname in self.actordefs:
				return self.actordefs[fname]
			else:
				raise Exception('ActorDef not defined: "%s"' % fname)
			
		filename = path.find(fname)
		if path.isdir(filename):
			d = filename
			filename = path.join(d, 'entity.xml')
			if not path.exists(filename):
				raise Exception('ActorDef missing entity.xml in "%s"' % d)
		
		if filename in self.actordefs:
			return self.actordefs[filename]
		else:			
			d = self.loadNewActorDef(filename)
				
			self.actordefs[filename] = d
			_log('Loaded ActorDef "%s"' % filename)
			return d
		
	def loadSound(self, filename):
		return Sound(filename)
		
	def playSound(self, filename, *args, **kargs):
		return Sound(filename).play(*args, **kargs)
		
	def trigger(self, name, **vars):
		self.events.trigger(self, name, vars)
		
	def draw(self, d, camera=(0,0), scale=1.0):
		viewsize = Point(d.size)
		camera = Point(camera)
		
		# Cap camera so you cant see out of game world
		xcam_min = viewsize.x / scale / 2
		ycam_min = viewsize.y / scale / 2
		xcam_max = self.width - xcam_min
		ycam_max = self.height - ycam_min
		camera.x = max(camera.x, xcam_min)
		camera.y = max(camera.y, ycam_min)
		camera.x = min(camera.x, xcam_max)
		camera.y = min(camera.y, ycam_max)
		
		d.pushMatrix()
		d.translate((d.width / 2, d.height / 2))
		d.scale(scale)
		
		# Center camera if display is bigger then game world
		if self.width * scale < viewsize.x:
			camera.x = self.width / 2
		if self.height * scale < viewsize.y:
			camera.y = self.height / 2
			
		d.translate((-camera.x, -camera.y))
		
		self.view = d.viewport
		
		# Draw Background Layers
		for bg in self.backgrounds:
			d.pushMatrix()
			if bg.width < self.width or bg.height < self.height:
				ratio = (camera[0] - xcam_min) / (xcam_max - xcam_min)
				x = ratio * (self.width - bg.width)
				ratio = (camera[1] - ycam_min) / (ycam_max - ycam_min)
				y = ratio * (self.height - bg.height)
				d.translate((bg.width / 2, bg.height / 2))
				d.translate((x, y))
				d.color((1.0,1.0,1.0))
				d.pushAlpha(bg.opacity)
				d.drawTexture(bg.texture, (bg.width, bg.height))
				d.popAlpha()
			else:
				d.drawTexture(bg.texture, (bg.width, bg.height))
			d.popMatrix()
			
		# Draw Tilemap
		if self.tilemap:
			self.tilemap.draw(d)
			
		# Draw Actors
		draw = lambda a:a.draw(d)
		map(draw, self.actors)
		
		# Draw Particles
		self.particles.draw(d)
			
		d.popMatrix()
			
	def update(self, ticks):
		self.doSpawns()
		
		self._isUpdating = True
		
		for i, m in self.managers.items():
			if hasattr(m, 'update'):
				m.update(ticks)
				
		update = lambda a:a.update(ticks)
		map(update, self.actors)
		
		if len(self.destroyedActors):
			lst = self.destroyedActors
			self.destroyedActors = []
			self._removeActors(lst)
			
		self.particles.update(ticks)
			
		self._isUpdating = False
		
	def spawn(self, df, onSpawn=None):
		if not isinstance(df, ActorDef):
			df = self.loadActorDef(df)
		info = ActorSpawnInfo(df)
		if onSpawn:
			info.onSpawn(onSpawn)
		self.queuedSpawns.append(info)
		return info
		
	def doSpawn(self):
		info = self.queuedSpawns.pop(0)
		try:
			a = self._spawn(info.adef)
			for fn in info.actions:
				fn(a)
			return a
		except Exception, e:
			_log('WARNING: Error spawning actor "%s", %s' % (info.adef.filename, e))
		
	def doSpawns(self):
		spawn = self._spawn
		for info in self.queuedSpawns:
			try:
				a = spawn(info.adef)
				for fn in info.actions:
					fn(a)
				info.onSpawn.emit(a)
			except Exception, e:
				_log('WARNING: Error spawning actor "%s", %s' % (info.adef.filename, e))
		self.queuedSpawns = []
		
	def _removeActors(self, actors):
		for a in set(actors):
			a.emit('destroy')
			for c in a.components:
				if hasattr(c, '_destroy'):
					c._destroy()
				#self.components.remove(c)
			self.actors.remove(a)
			
		
	def destroy(self, actor):
		if actor in self.actors and actor not in self.destroyedActors:
			if actor.isDestroyable:
				actor.isDestroyed = True
				if self._isUpdating:
					self.destroyedActors.append(actor)
				else:
					self._removeActors([actor])
			
	def getFaction(self, name):
		for f in self.factions:
			if f.name == name:
				return f
		else:
			f = Faction()
			f.name = name
			self.factions.append(f)
			return f
			
	def getActorByUid(self, uid):
		for a in self.actors:
			if a.uid == uid:
				return a
		else:
			raise ActorNotFound('UID %s' % uid)
		
	def getActorById(self, id):
		for a in self.actors:
			if a.id == id:
				return a
		else:
			raise ActorNotFound('ID %s' % id)
				
	def getActorByName(self, name):
		for a in self.actors:
			if a.name == name:
				return a
		else:
			raise ActorNotFound('Name %s' % name)
				
				
	def getActorsByClass(self, cls):
		return [a for a in self.actors if a.classname == cls]
		
	def getNextActorByClass(self, actor):
		actors = self.getActorsByClass(actor.classname)
		index = actors.index(actor) + 1
		if index < len(actors):
			r = actors[index]
		else:
			r = actors[0]
		if r != actor:
			return r
		
	def getManager(self, type, init=True):
		if type in self.managers:
			return self.managers[type]
		elif type in self.managerFactories:
			m = self.managerFactories[type]()
			m._world = self
			self.managers[type] = m
			if init and hasattr(m, 'init'):
				m.init()
			return m
		
	def addManagerFactory(self, type, mgr):
		self.managerFactories[type] = mgr
		
	def addFactory(self, type, cls, cdef=None):
		if cdef:
			fn = cdef
		else:
			fn = _F(cls)
		self.componentFactories[type] = ComponentFactory(fn, cls, cdef)
	
	def getRegisteredComponentFactoryName(self, c):
		for k, v in self.componentFactories.items():
			if c == v.fn:
				return k
		else:
			raise Exception('Component Factory not found for %s' % c)
				
	def getRegisteredComponentDefName(self, c):
		cls = c.__class__
		for k, v in self.componentFactories.items():
			if cls == v.cdef:
				return k
		else:
			raise Exception('Component Factory not found for %s' % c)
			
				
	def getRegisteredComponentName(self, c):
		for k, v in self.componentFactories.items():
			if isinstance(c, v.type):
				return k
		
	def createComponentDef(self, type):
		l = list(self.componentFactories.keys())
		l.sort()
		print l
		return self.componentFactories[type]()
		
	def _addComponent(self, c, type):
		m = self.getManager(type)
		c.componentManager = m
		#self.components.append(c)
		
	def _spawn(self, adef):
		assert(not self._isUpdating)
		# Create actor
		a = Actor()
		a.uid = self._entityCount
		a.template = adef.filename
		a.world = self
		a.number = self._entityCount
		a.name = adef.name
		a.layer = adef.layer
		a.classname = adef.classname
		a.faction = adef.faction
		
		# generate components
		for k, cdef in adef.components.items():
			c = cdef.create()
			self._addComponent(c, k) # FIXME: if an exception occurs after this, component is orphaned
			a.components.append(c)
		
		# initialize actor and components
		a.init()
		
		# add actor and components to world
		self.actors.append(a)
		#this.components ~= actor.components; // components should be added here, maybe
		
		# increment unique identifer count
		self._entityCount += 1
		#print 'Spawned Actor [%s]' % a.id
		#log!(World)("Spawning Actor [%s]".format(actor.id));
		return a
