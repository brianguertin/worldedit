from xml.etree.ElementTree import ElementTree, Element
from sgl.video.texture import Texture
from sgl.core import path
from sgl.core.tuples import *
from sgl.core.signals import Signal
from os.path import isdir
import math

class Polygon:
	def __init__(self):
		self.vertices = []

class Bone(object):
	def __init__(self, xBone):
		self.id = ""
		self.index = 0.0
		self.length = 0.0
		self.baseAngle = 0.0
		self.bones = []
		self.rotate = True # For ragdoll purposes TODO: specific angle limits here?
	
		self.id = xBone.get("id")
		xBone.query(self, 'index')
		xBone.query(self, 'angle', 'baseAngle')
		xBone.query(self, 'length')
		for xB in xBone.findall("bone"):
			self.bones.append(Bone(xB))
			
	def __hash__(self):
		return id(self)
	
	"""
	def save(e):
		xBone = Element("bone")
		xBone.setAttribute("id", self.id)
		xBone.setAttribute!(float)("angle", self.baseAngle)
		xBone.setAttribute!(float)("length", self.length)
		xBone.setAttribute!(float)("index", self.index)
		for bone in self.bones:
			bone.save(xBone)
		e.addNode(xBone)
	"""

	def __cmp__(self, o):
		return cmp(self.index, o.index)

class KeyFrame:
	def __init__(self, x=None):
		self.duration = 0
		self.angles = {}
		if x:
			self.load(x)
	
	def load(self, xFrame):
		self.duration = int(xFrame.get("duration"))
		for xBone in xFrame.findall("bone"):
			id = xBone.get("id")
			self.angles[id] = float(xBone.get("angle"))

class Animation:
	def __init__(self, xAnim=None):
		self.id = ''
		self.frames = []
		if xAnim:
			self.id = xAnim.get("id")
			for xFrame in xAnim.findall("frame"):
				self.frames.append(KeyFrame(xFrame))

class Skeleton:
	def __init__(self, filename):
		self.animations = []
		self.tree = [] # Bones in a tree structure
		self.bones = [] # Bones in a flat list, ordered by z-index
		self.width = self.height = 0.0
		
		filename = path.find(filename)
		if isdir(filename):
			path.add(filename)
			try:
				self.load(ElementTree(file=path.find('skeleton.xml')).getroot())
			finally:
				path.pop()
		
		else:
			path.add(path.dirname(filename))
			try:
				self.load(ElementTree(file=filename).getroot())
			finally:
				path.pop()
		
		log('Loaded "%s"' % filename, type='Skeleton')
		
	def load(self, xSprite):
		xSkel = xSprite.find("skeleton")
		
		# Read approximate width/height
		w = xSkel.get('width')
		if w:
			self.width = int(w)
		h = xSkel.get('height')
		if h:
			self.height = int(h)
			
		# Read bone tree
		for xBone in xSkel.findall("bone"):
			self.tree.append(Bone(xBone))
			
		def add_bones(bs):
			for bone in bs:
				self.bones.append(bone)
				add_bones(bone.bones)

		add_bones(self.tree)
		self.bones.sort() # Sorted by z-index / rendering order
		
		for xAnim in xSprite.findall('animation'):
			self.animations.append(Animation(xAnim))
		
		# FIXME: dont store the bone id string so many times!
		# TODO: validate frames,
		# make sure the same IDs appear in each frame
	"""
	def save(string filename) {
		// Create XML Document
		scope xDoc = new Document
		xSprite = xDoc.addNode(new Element("sprite"))
		
		// Save skeleton
		xSkel = xSprite.addNode(new Element("skeleton"))
		foreach (bone in self.tree) {
			bone.save(xSkel)
		}
		
		// Save animations
		foreach (anim in self.animations) {
			xAnim = xSprite.addNode(new Element("animation"))
			xAnim.setAttribute("id", anim.id)
			foreach (frame in anim.frames) {
				xFrame = new Element("frame")
				xFrame.setAttribute!(int)("duration", frame.duration)
				foreach (bone_id, bone_angle in frame.angles) {
					xBone = new Element("bone")
					xBone.setAttribute("id", bone_id)
					xBone.setAttribute("angle", bone_angle)
					xFrame.addNode(xBone)
				}
				xAnim.addNode(xFrame)
			}
		}
		
		# Save XML Document
		xDoc.save(filename)
		log("Saved '%s'" % filename, type='Skeleton')
	"""
	
	def getAnimByID(id):
		for anim in self.animations:
			if anim.id == id:
				return anim
		
		raise Exception("Animation doesn't exist with id '%s'" % id)
	
	def getBoneByID(id):
		for bone in self.bones:
			if bone.id == id:
				return bone
		raise Exception("Bone doesn't exist with id '%s'" % id)
		

class BoneSkin:
	def __init__(self):
		self.offset = Vector(0,0)
		self.angle = 0.0
		self.scale = 1.0
		self.image = None

class Skin:
	def __init__(self, filename=None):
		self.images = {}
		#self.sounds = []
		if filename:
			filename = path.find(filename)
			if isdir(filename):
				path.add(filename)
				try:
					self.load(ElementTree(file=path.find('skin.xml')).getroot())
				finally:
					path.pop()
			else:
				path.add(path.dirname(filename))
				try:
					self.load(ElementTree(file=filename).getroot())
				finally:
					path.pop()
			
			log('Loaded "%s"' % filename, type='Skin')
		
	def load(self, xSkin):
		for x in xSkin.findall('image'):
			id = x.get("id")
			boneSkin = BoneSkin()
			boneSkin.image = Texture(x.get('file'))
			x.query(boneSkin.offset, 'x')
			x.query(boneSkin.offset, 'y')
			x.query(boneSkin, 'angle')
			x.query(boneSkin, 'scale')
			self.images[id] = boneSkin

class AnimationState:
	def __init__(self, a):
		self.group = 0
		self.ticks = 0
		self.current = 0
		self.next = 1
		self.mod = 0
		self.num_loops = 0 # 0 = infinite
		self.loop_counter = 0
		self.animation = a
		if self.next >= len(self.animation.frames):
			self.next = 0
		#self.onFrameChanged = Signal()
	
	def update(self, t):
		prev = self.ticks
		self.ticks += t
		if self.ticks >= self.animation.frames[self.current].duration:
			self.current += 1
			self.next += 1
			self.ticks = 0
			if self.next >= len(self.animation.frames):
				self.next = 0
			
			if self.current >= len(self.animation.frames):
				self.current = 0
				++self.loop_counter
			
		
		self.mod = float(self.ticks) / self.animation.frames[self.current].duration
		#log!(Animation)("%s: frame = %s, next = %s, ticks = %s of %s, mod = %s".format(self.animation.id, self.current, self.next, self.ticks, self.animation.frames[self.current].duration, self.mod))
	
	def setFrame(self, f):
		self.current = f
		self.next = f + 1
		if self.current >= len(self.animation.frames):
			self.current = 0
			self.next = 1
		
		if self.next >= len(self.animation.frames):
			self.next = 0
		#self.sigFrameChanged.emit()
	
	def restart(self):
		self.setFrame(0)
		self.ticks = 0
		self.mod = 0
	
	@property
	def isExpired(self):
		if self.num_loops < 1:
			return False
		else:
			return self.loop_counter >= self.num_loops

class BoneState:
	def __init__(self):
		self.position = Vector(0,0)
		self.angle = 0.0
		self.absAngle = 0.0
		self.mask = Polygon()
		self.rotation = 0.0

class Model:
	def __init__(self, fname=None):
		self.onAnimAdded = Signal()
		self.activeAnims = []
		self.boneStates = {}
		self.flipHorizontal = False
		self.skeleton = None
		self.skin = None
		
		if fname:
			filename = path.find(fname)
			if isdir(filename):
				self.init(Skeleton(filename), Skin(filename))
			else:
				doc = ElementTree(file=filename)
				self.load(doc.getroot())
				
	def load(self, x):
		skel = Skeleton(x.get('skeleton'))
		sk = Skin(x.get('skin'))
		self.init(skel, sk)
			
	def init(self, skeleton, skin):
		self.skeleton = skeleton
		self.skin = skin
		for bone in self.skeleton.bones:
			b = BoneState()
			b.angle = bone.baseAngle
			b.mask = Polygon()
			self.boneStates[bone] = b
		
	def updateBones(self):
		self._updateBones(self.skeleton.tree, Vertex(0,0), math.pi)
		
	def _updateBones(self, bones, offset, angle):
		for bone in bones:
			state = self.boneStates[bone]
			boneVec = Vector.fromAngle(state.angle + angle) * bone.length
			state.position = offset # + (boneVec / 2)
			state.absAngle = angle
			
			"""
			vs = []
			vs.append(Vector(-4, -4))
			vs.append(Vector(-4, 4))
			vs.append(Vector(4, 4))
			vs.append(Vector(4, -4))
			
			state.mask = Polygon()
			state.mask.vertices = vs
			"""
			
			self._updateBones(bone.bones, offset + boneVec, state.angle + angle)

	def update(self, ticks):
		deadAnims = []
		for anim in self.activeAnims:
			anim.update(ticks)
			if anim.isExpired:
				deadAnims.append(anim)
		for anim in deadAnims:
			self.activeAnims.remove(anim)
		
		for i, bone in zip(range(len(self.skeleton.bones)), self.skeleton.bones):
			st = self.boneStates[bone]
			st.angle = bone.baseAngle + st.rotation
			for anim in self.activeAnims:
				try:
					first = anim.animation.frames[anim.current].angles[bone.id]
					second = anim.animation.frames[anim.next].angles[bone.id]
					st.angle = bone.baseAngle + st.rotation + first + (second - first) * anim.mod
					break
				except:
					pass
		self.updateBones()
		
	def draw(self, d):
		if self.flipHorizontal:
			d.scale(-1, 1)
		for bone in self.skeleton.bones:
			state = self.boneStates[bone]
			d.pushMatrix()
			d.translate(state.position)
			d.rotate(state.angle + state.absAngle)
			d.translate((bone.length / 2, 0))
			if self.skin:
				try:
					image = self.skin.images[bone.id]
					d.translate(image.offset)
					d.rotate(image.angle)
					d.scale(image.scale)
					d.color((1.0, 1.0, 1.0))
					d.drawTexture(image.image)
					#image.image.render(d)
					#image.image.render(display, Rect(0, 0, image.image.size.x, image.image.size.y))
				except KeyError:
					pass
			else:
				d.drawLine(Vector(bone.length, 0))
			d.popMatrix()
	
	"""
	defPolygon[] masks() {
		static const int BONE_WIDTH = 2
		Polygon[] result
		foreach (ref bone in self.skeleton.bones) {
			state = self.boneStates[bone]
			poly = new Polygon
			poly.vertices ~= Vertex(0, -BONE_WIDTH) // bottom left
			poly.vertices ~= Vertex(bone.length, -BONE_WIDTH) // bottom right
			poly.vertices ~= Vertex(bone.length, BONE_WIDTH) // top right
			poly.vertices ~= Vertex(0, BONE_WIDTH) // top left
			poly.rotate(state.angle + state.absAngle)
			poly.transform(state.position)
			if (self.flipHorizontal)
				poly.flipHorizontal()
			result ~= poly
		
		return result
	"""
		
	def singleAnim(self, name):
		self.addAnim(name, 1)
		
	def addAnim(self, name, loops = 0, group = 0):
		for anim in self.skeleton.animations:
			if anim.id == name:
				st = AnimationState(anim)
				st.num_loops = loops
				st.group = group
				self.activeAnims.append(st)
				self.onAnimAdded.emit()
				return st

		raise Exception("Animation not found: '%s'" % name)
		
	def clearAnims(self, group = None):
		if group is None:
			self.activeAnims = []
		else:
			self.activeAnims = [a for a in self.activeAnims if a.group != group]
		
	def setAnim(self, anim, group = None):
		if not anim:
			self.clearAnims(group=group)
		elif not self.animIsActive(anim):
			self.clearAnims(group=group)
			self.addAnim(anim, group=group)
		
	def restartAnims(self):
		for animState in self.activeAnims:
			animState.restart()
		
		self.update(0)
	
	def animIsActive(self, name):
		for animState in self.activeAnims:
			if name == animState.animation.id:
				return True
		return False
		
	def getBoneAndState(self, name):
		if not name:
			return (None, None)
		for k, v in self.boneStates.items():
			if k.id == name:
				return (k, v)
				
	def getBonePosition(self, name, span = 1.0):
		k, v = self.getBoneAndState(name)
		if k:
			r = v.position
			if span > 0:
				sp = Vector.fromAngle(v.absAngle + v.angle).scale(k.length * span)
				old_r = r
				r = r + sp
			r.orientation = v.absAngle + v.angle
			if self.flipHorizontal:
				r.x = -r.x
			return r
		else:
			return (0,0)
			
	@property
	def width(self):
		return self.skeleton.width
		
	@property
	def height(self):
		return self.skeleton.height
