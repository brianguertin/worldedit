from sgl.core.signals import Signal
from sgl.core import compiler

class Event(object):
	"""
	# Example Usage
	ev = Event()
	ev.add_trigger('init')

	def do_stuff():
		print 'hello!'

	ev.add_action(do_stuff)

	es = EventSystem()
	es.add_event(ev)

	es.trigger('init')
	"""
	
	def __init__(self):
		self.onAddTrigger = Signal()
		self._triggers = []
		self._conditions = []
		self._actions = []
		
	def add_trigger(self, type):
		trg = Trigger()
		trg._event = self
		trg._type = type
		self._triggers.append(trg)
		self.onAddTrigger.emit(trg)
		
	def add_condition(self, fn):
		self._conditions.append(fn)
		
	def add_action(self, fn):
		self._actions.append(fn)
		
	def add_script(self, text):
		script = compiler.compile_literal(text)
		
		def fn(**args):
			exec script in args
			
		self.add_action(fn)
		
class Trigger(object):
	def __init__(self):
		self._event = None
		self._type = ''
		
class EventSystem(object):
	def __init__(self):
		self._triggers_by_type = {}
		self._vars = {}
		
	def load(self, x):
		for xEvent in x.findall('event'):
			ev = Event()
			for xTrig in xEvent.findall('trigger'):
				ev.add_trigger(xTrig.get('type'))
			for xScript in xEvent.findall('script'):
				ev.add_script(xScript.text)
			self.add(ev)
		
	def get_trigger_by_type(self, name):
		try:
			return self._triggers_by_type[name]
		except KeyError:
			lst = []
			self._triggers_by_type[name] = lst
			return lst
			
	def create(self):
		e = Event()
		self.add(e)
		return e
		
	def _add_trigger(self, trg):
		self.get_trigger_by_type(trg._type).append(trg)
		
	def add(self, event):
		for trg in event._triggers:
			self._add_trigger(trg)
		event.onAddTrigger.connect(self._add_trigger)
		
	def trigger(self, world, name, vars):
		# If we have any triggers listening for that event
		if name in self._triggers_by_type:
			
			for trg in self._triggers_by_type[name]:		
				event = trg._event
				
				# Make sure all conditions are true for this event
				for cond in event._conditions:
					if not cond(world=world, **vars):
						break # Break if any condition is false
					
				# If all conditions are true, do all the actions	
				else:
					for act in event._actions:
						act(world=world, **vars)
		
# world.triggers.trigger('collide', obj1=7, obj2=10)
