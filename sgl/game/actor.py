from sgl.core.signals import Signal
from sgl.core import path

PUBLIC_PROPERTIES = ['id', 'name', 'classname', 'layer', 'isDestroyable', 'faction', 'face']

class ActorDef(object):
	def __init__(self):
		self.name = ''
		self.classname = ''
		self.thumb = ''
		self.world = None
		self.layer = None
		self.components = {}
		self.isDestroyable = True
		self.faction = None
		self.filename = None
		
	def load(self, x):
		assert(self.world)
		x.query(self, 'layer')
		x.query(self, 'name')
		x.query(self, 'thumb')
		x.query(self, 'class', 'classname')
		faction = x.get('faction')
		if faction:
			self.faction = self.world.getFaction(faction)
		#x.query(self, 'faction')
		for xComp in x.findall('component'):
			type = xComp.get('type') or xComp.get('id')
			# Use existing component def if found
			if type in self.components: 
				comp = self.components[type]
			else: # Otherwise create a new one from its factory
				comp = self.world.createComponentDef(type)
			
			# Update def with xml data
			if hasattr(comp, 'load'):
				comp.load(xComp)
			self.components[type] = comp

class _PointGetter:
	def __init__(self, fn):
		self.fn = fn
		
	def get(self):
		return self.fn()

class _Timer:
	def __init__(self):
		self.name = ''
		self.interval = 1000
		self.func = None
		self.ticks = 0
		self.times = 0
		self.expire = None
		self.running = True
		
	def stop(self):
		self.running = False

class ActionNotFound(AttributeError):
	def __init__(self, *args, **kargs):
		super(ActionNotFound, self).__init__(*args, **kargs)
		
class PointNotFound(AttributeError):
	def __init__(self, *args, **kargs):
		super(ActionNotFound, self).__init__(*args, **kargs)
		
class Actor(object):
	def __init__(self):
		self.prototype = None
		self.components = []
		self.uid = -1 # First uid is 0, this way we know if it was set
		self.id = ''
		self.name = ''
		self.classname = ''
		self.signals = {}
		self.player = 0
		self.faction = None
		self.timers = []
		self.number = 0
		self.isDestroyable = True
		self.draw = self.signal('draw').emit
		self.updateSignal = self.signal('update')
		self.queue = []
		self.disabled = False
		self.isDestroyed = False
		self._face = 1 # 0 - Left, 1 - Right
		
	def __repr__(self):
		if self.name and self.id:
			return '<Actor: %s (%s)>' % (self.name, self.id)
		else:
			return '<Actor: %s>' % (self.name or self.id)
			
	# Named identifier
	def __str__(self):
		if self.name or self.id:
			return self.name or self.id
		elif self.classname:
			return '%s %s' % self.classname, self.uid
		else:
			return 'Unknown Object %s' % self.uid
			
	def get_face(self):
		return ('left','right')[self._face]
		
	def set_facei(self, face):
		if face == 0 and self._face != 0:
			self._face = 0
			self.emit('face', 'left')
		elif face == 1 and self._face != 1:
			self._face = 1
			self.emit('face', 'right')
			
	def set_face(self, v):
		if v == 'left':
			self.set_facei(0)
		elif v == 'right':
			self.set_facei(1)
		else:
			raise Exception('Invalid face value "%s" must be "left" or "right"' % v)
			
	face = property(get_face, set_face)
		
	def addTimer(self, interval, fn, name='', times=0, expire=None):
		t = _Timer()
		t.name = name
		t.interval = interval
		t.func = fn
		t.times = times
		t.expire = expire
		self.timers.append(t)
		return t
		
	def removeTimer(self, t):
		self.timers.remove(t)
		
	def add_component(self, c):
		self.components.append(c)
		c._actor = c.actor = self
		if hasattr(c, 'init'):
			c.init()
		
	def point(self, name):
		for c in self.components:
			if hasattr(c, '_points') and name in c._points:
				return _PointGetter(c._points[name])
		else:
			raise PointNotFound('No point "%s" in actor' % name)
				
	def action(self, name):
		for c in self.components:
			if hasattr(c, '_actions') and name in c._actions:
				return getattr(c, c._actions[name] or name)
		else:
			if name.startswith('set_') and name[4:] in PUBLIC_PROPERTIES:
				def set_prop(v):
					setattr(self, name[4:], v)
				return set_prop
			raise ActionNotFound('No action "%s" in actor "%s"' % (name, repr(self)))
		
	def do(self, name, *args, **kargs):
		return self.action(name)(*args, **kargs)
		
	def attempt(self, name, *args, **kargs):
		try:
			self.do(name, *args, **kargs)
			return True
		except:
			pass
			
	def query(self, name, *args, **kargs):
		try:
			return self.get(name, *args, **kargs)
		except AttributeError:
			return kargs.get('default', None)
			
	def get(self, name, *args, **kargs):
		if name in PUBLIC_PROPERTIES:
			return getattr(self, name)
		return self.do('get_%s' % name, *args, **kargs)
		
	def set(self, name, *args, **kargs):		
		if name in PUBLIC_PROPERTIES:
			setattr(self, name, args[0])
		return self.do('set_%s' % name, *args, **kargs)
			
	def signal(self, name):
		try:
			return self.signals[name]
		except KeyError:
			s = self.signals[name] = Signal()
			return s

	def emit(self, name, *args, **kargs):
		self.signal(name).emit(*args, **kargs)
			
	def connect(self, name, fn):
		self.signal(name).connect(fn)
		
	def init(self):
		for c in self.components:
			c._actor = c.actor = self
			if hasattr(c, 'init'):
				c.init()
				
	def serialize(self):
		r = {}
		r['face'] = self._face
		self.emit('serialize', r)
		return r
		
	def deserialize(self, data):
		self.emit('deserialize', data)
		self.set_facei(data['face'])
				
	def destroy(self):
		self.world.destroy(self)
		
	def disable(self):
		if not self.disabled:
			self.disabled = True
			self.emit('disable')
		
	def enable(self):
		if self.disabled:
			self.disabled = False
			self.emit('enable')
		
	def update(self, ticks):
		self.updateSignal.emit(ticks)
				
		# Update/Fire/Destroy timers
		for t in self.timers[:]: # Use a copy so we can .remove()
			if not t.running:
				continue
			t.ticks += ticks
			if t.ticks > t.interval:
				t.ticks -= t.interval
				t.func()
				
				# Handle/Destroy limited fire timers
				if t.times > 0:
					t.times -= 1
					if t.times == 0:
						if t.expire:
							t.expire()
						self.timers.remove(t)
				
	def getEnemies(self):
		def fn(a):
			return a.faction in self.faction.enemies
		return filter(fn, self.world.actors)
		
	@property
	def properties(self):
		readable = []
		writable = []
		for c in self.components:
			if hasattr(c, '_actions'):
				for key in c._actions.keys():
					if key[:4] == 'get_':
						readable.append(key[4:])
					elif key[:4] == 'set_':
						writable.append(key[4:])
		return [r for r in readable if r in writable]
		
