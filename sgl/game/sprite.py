from xml.etree.ElementTree import ElementTree
from sgl.video.texture import Texture
from sgl.core import path
from sgl.core.signals import Signal

class _Frame(object):
	def __init__(self, index = 0):
		self.index = int(index)
		self.opacity = 1.0
		self.scale = 1.0

class _Animation(list):
	def __init__(self, *args, **kargs):
		super(_Animation, self).__init__(*args, **kargs)
		self.loop = True
		self.speed = 0

class SpriteDef(object):
	_cache = {}
	def __new__(cls, filename):
		f = path.find(filename)
		if f in SpriteDef._cache:
			return SpriteDef._cache[f]
		else:
			self = object.__new__(SpriteDef)
			log('Loading: "%s"' % f, type='SpriteDef')
			path.add(path.dirname(f))
			try:
				self._init(f)
			finally:
				path.pop()
			SpriteDef._cache[f] = self
			return self
	
	def _init(self, filename):
		x = ElementTree(file=filename).getroot()
		self.texture = Texture(x.get('filename'))
		self.columns = int(x.get('columns') or 1)
		assert(self.columns > 0)
		self.rows = int(x.get('rows') or 1)
		assert(self.rows > 0)
		self.width = int(self.texture.width / self.columns)
		self.height = int(self.texture.height / self.rows)
		self.num_frames = self.columns * self.rows
		assert(self.num_frames > 0)
		
		self.defaultSpeed = 100
		self.defaultLoop = True
		x.query(self, 'speed', 'defaultSpeed')
		x.query(self, 'loop', 'defaultLoop')
		self.anims = {}
		
		for xAnim in x.findall('animation'):
			name = xAnim.get('name')
			frame_list = xAnim.get('frames')
			if name:
				if frame_list:
					frames = map(_Frame, frame_list.split(','))
				else:
					frames = []
					for xF in xAnim.findall('frame'):
						f = _Frame()
						xF.query(f, 'index')
						xF.query(f, 'opacity')
						xF.query(f, 'scale')
						frames.append(f)
				if len(frames):
					anim = _Animation(frames)
					anim.speed = self.defaultSpeed
					xAnim.query(anim, 'speed')
					anim.loop = self.defaultLoop
					xAnim.query(anim, 'loop')
					self.anims[name] = anim
					
		if 'default' not in self.anims:			
			anim = _Animation([_Frame(i) for i in range(self.num_frames)])
			anim.speed = self.defaultSpeed
			anim.loop = self.defaultLoop
			self.anims['default'] = anim
		
	def drawFrame(self, d, frame):
		d.pushMatrix()
		d.pushAlpha(frame.opacity)
		d.scale(frame.scale)
		d.color((1,1,1))
		index = frame.index
		x = int(index % self.columns) * self.width
		y = int(index / self.columns) * self.height
		self.texture.drawFragment(x, y, x + self.width, y + self.height)
		d.popAlpha()
		d.popMatrix()
		
	def drawBetweenFrame(self, d, frame, next, next_mod):
		# TODO: custom color transformation between frames
		mod = 1 - next_mod
		d.pushMatrix()
		d.pushAlpha(frame.opacity * mod + next.opacity * next_mod)
		d.scale(frame.scale * mod + next.scale * next_mod)
		d.color((1,1,1))
		index = frame.index
		x = int(index % self.columns) * self.width
		y = int(index / self.columns) * self.height
		self.texture.drawFragment(x, y, x + self.width, y + self.height)
		d.popAlpha()
		d.popMatrix()
		
	reload = _init
	
class Sprite:
	def __init__(self, filename):
		self.sdef = SpriteDef(filename)
		self.onAnimEnd = Signal()
		self.ticks = 0
		self.frame = 0
		self.next_frame = 0
		self.active = True
		self.setAnim('default')
		
	def setAnim(self, name):
		self.anim = self.sdef.anims[name]
		self.frame = 0
		self.ticks = 0
		self.active = True
		if len(self.anim) > 1:
			self.next_frame = self.frame + 1
		else:
			self.next_frame = 0
		
	def update(self, t):
		if self.active:
			self.ticks += t
			if self.ticks > self.anim.speed:
				self.ticks -= self.anim.speed
				self.frame += 1
				if self.frame >= len(self.anim):
					if self.anim.loop:
						self.frame = 0
					else:
						self.frame = len(self.anim) - 1
						self.active = False
					self.onAnimEnd.emit()
					
				self.next_frame = self.frame + 1
				if self.next_frame >= len(self.anim):
					self.next_frame = 0
		
	def draw(self, d):
		if len(self.anim) and self.ticks > 0 and (self.next_frame > 0 or self.anim.loop):
			mod = float(self.ticks) / self.anim.speed
			self.sdef.drawBetweenFrame(d, self.anim[self.frame], self.anim[self.next_frame], mod)
		else:
			self.sdef.drawFrame(d, self.anim[self.frame])
		
	@property
	def width(self):
		return self.sdef.width
		
	@property
	def height(self):
		return self.sdef.height
		
	@property
	def size(self):
		return self.width, self.height
