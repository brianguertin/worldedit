from sgl.game.tileset import Tileset

class Tilemap(object):
	def __init__(self):
		self.layers = []
		self.tilesets = []
		self.cellwidth = 1.0
		self.cellheight = 1.0
		
	def load(self, x):
		x.query(self, 'cellwidth')
		x.query(self, 'cellheight')
		
		for xTileset in x.findall('tileset'):
			filename = xTileset.get('filename')
			if filename:
				self.tilesets.append(Tileset(filename))
				
		for xLayer in x.findall('layer'):
			layer = []
			for xRow in xLayer.findall('row'):
				tiles = xRow.get('tiles')
				if tiles:
					row = []
					for t in tiles.split(','):
						if t:
							tset, index = map(int, t.split(':'))
							row.append((tset, index))
						else:
							row.append(None)
					layer.append(row)
				else:
					layer.append([])
			self.layers.append(layer)
			
	def save(self, x):
		x.set('cellwidth', self.cellwidth)
		x.set('cellheight', self.cellheight)
		for tset in self.tilesets:
			xTileset = x.makeelement('tileset')
			xTileset.set('filename', tset.filename)
			x.append(xTileset)
			
		for layer in self.layers:
			xLayer = x.makeelement('layer')
			for row in layer:
				tiles = []
				for t in row:
					if t:
						tiles.append('%s:%s' % t)
					else:
						tiles.append('')
				tiles = ','.join(tiles)
				
				xRow = x.makeelement('row')
				xRow.set('tiles', tiles)
				xLayer.append(xRow)
			x.append(xLayer)
			
	def draw(self, d):
		d.pushMatrix()
		d.color((1,1,1))
		for layer in self.layers:
			d.pushMatrix()
			d.translate((0, self.cellheight * (len(layer)-1)))
			for row in reversed(layer):
				d.pushMatrix()
				for tile in row:
					if tile:
						tset, index = tile
						# Constrain width and bottom-align tiles
						tex = self.tilesets[tset].tiles[index].texture
						h_ratio = self.cellwidth / tex.width
						size = tex.size * h_ratio
						d.pushMatrix()
						# TODO: simplify?
						d.translate((self.cellwidth/2, size.y/2))
						d.drawTextureStretched(tex, size)
						d.popMatrix()
					d.translate((self.cellwidth, 0))
				d.popMatrix()
				d.translate((0, -self.cellheight))
			d.popMatrix()
		d.popMatrix()
		
	def locate(self, point):
		x = int(point.x / self.cellwidth)
		y = int(point.y / self.cellheight)
		return x, y
