from sgl.app.app import App, Event, Key, Mouse

THRESHOLD = 0.01

SELECT_RANGE = 0.2

class Controller:
	def __init__(self, actor):
		self.actor = actor
		self.a = actor
		actor.connect('control', self.handleControl)
		self.lastMousePos = (0,0)
		
	def handleControl(self, obj):
		self.a = obj
		obj.connect('exit', self.resetControl)
		
	def resetControl(self):
		self.a = self.actor
		
	def update(self):
		if self.a.world.view:
			self.a.set('target_point',  self.a.world.view.unProject(self.lastMousePos))
		
	def input(self, e):
		if e.type == Event.KEYDOWN:
			k = e.key
			if k in [Key.W, Key.UP]:
				self.a.emit('+moveup')
			elif k in [Key.S, Key.DOWN]:
				self.a.emit('+movedown')
			elif k in [Key.A, Key.LEFT]:
				self.a.emit('+moveleft')
			elif k in [Key.D, Key.RIGHT]:
				self.a.emit('+moveright')
			elif k == Key.E:
				self.a.emit('+use')
			elif k == Key.SPACE:
				self.a.emit('+jump')
			elif k == Key.K:
				self.a.emit('disable')
			elif k == Key.L:
				self.a.emit('enable')
			elif k == Key.ESCAPE:
				self.a.set('target', None)
			else:
				return False
		elif e.type == Event.KEYUP:
			k = e.key
			if k in [Key.W, Key.UP]:
				self.a.emit('-moveup')
			elif k in [Key.S, Key.DOWN]:
				self.a.emit('-movedown')
			elif k in [Key.A, Key.LEFT]:
				self.a.emit('-moveleft')
			elif k in [Key.D, Key.RIGHT]:
				self.a.emit('-moveright')
			elif k == Key.E:
				self.a.emit('-use')
			elif k == Key.SPACE:
				self.a.emit('-jump')
			else:
				return False

		elif e.type == Event.MOUSEBUTTONDOWN:
			b = e.button
			if b == Mouse.LEFT:
				self.a.set('target_point',  self.a.world.view.unProject(e.pos))
				self.a.emit('+action')
			elif b == Mouse.RIGHT:
				#self.a.set('target_point',  self.a.world.view.unProject(e.pos))
				#self.a.emit('+alt')
				p = self.a.world.view.unProject(e.pos)
				for actor in self.a.world.actors:
					pos = actor.query('position')
					if not pos:
						continue
					if p.distance(pos) < SELECT_RANGE:
						self.a.set('target', actor)
						break
				else:
					self.a.set('target', None)
			else:
				return False
		
		elif e.type == Event.MOUSEBUTTONUP:
			b = e.button
			if b == Mouse.LEFT:
				self.a.set('target_point',  self.a.world.view.unProject(e.pos))
				self.a.emit('-action')
			elif b == Mouse.RIGHT:
				self.a.set('target_point',  self.a.world.view.unProject(e.pos))
				self.a.emit('-alt')
			else:
				return False
		elif e.type == Event.MOUSEMOTION:
			self.lastMousePos = e.pos
			try:
				self.a.set('target_point',  self.a.world.view.unProject(e.pos))
			except:
				pass
		elif e.type == Event.JOYAXISMOTION:
			axis = e.axis
			v = e.value
			if axis == 0:
				if v < -THRESHOLD:
					self.a.emit('+moveleft')
					self.a.emit('-moveright')
				elif v > THRESHOLD:
					self.a.emit('+moveright')
					self.a.emit('-moveleft')
				else:
					self.a.emit('-moveright')
					self.a.emit('-moveleft')
			elif axis == 1:
				if v < -THRESHOLD:
					self.a.emit('+moveup')
					self.a.emit('-movedown')
				elif v > THRESHOLD:
					self.a.emit('+movedown')
					self.a.emit('-moveup')
				else:
					self.a.emit('-moveup')
					self.a.emit('-movedown')
			elif axis == 3:
				if v < -THRESHOLD or v > THRESHOLD:
					pos = self.a.get('position')
					target = self.a.get('target_point')
					self.a.set('target_point', (pos.x + v, target.y))
			elif axis == 2:
				pos = self.a.get('position')
				target = self.a.get('target_point')
				self.a.set('target_point', (target.x, pos.y - v))
			log('axis: %s, value: %s' % (axis, v))
		elif e.type == Event.JOYBUTTONDOWN:
			b = e.button
			if b in (0,7):
				self.a.emit('+action')
			elif b == 1:
				self.a.emit('+use')
			elif b == 6:
				self.a.emit('+moveup')
		elif e.type == Event.JOYBUTTONUP:
			b = e.button
			if b in (0,7):
				self.a.emit('-action')
			elif b == 1:
				self.a.emit('-use')
			elif b == 6:
				self.a.emit('-moveup')
		else:
			return False
		return True
