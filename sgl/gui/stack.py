from sgl.gui.bin import Bin

class Stack(Bin):
	def __init__(self):
		super(Stack, self).__init__()
		self.children = []
		
	def add(self, w):
		if self.child:
			self.child.onBlur.emit()
		self.children.append(w)
		self.child = w
		self.pack()
		self.child.onFocus.emit()
		
	def pop(self):
		if len(self.children):
			self.children[-1].onBlur.emit()
			self.children.pop()
			if len(self.children):
				self.child = self.children[-1]
				self.child.onFocus.emit()
			else:
				self.child = None
			self.pack()
		else:
			raise Exception('Stack has no children to pop')
