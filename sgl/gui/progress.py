from sgl.gui.widget import Widget

class Progress(Widget):
	def __init__(self, progress = 0.0):
		super(Progress, self).__init__(self)
		self._progress = 0.0
		self._setProgress(progress)
		
	def draw(self, display):
		display.pushMatrix()
		display.color((0,0,1))
		display.drawFilledRectangle(self.position, (self.x + self._progress * self.width, self.y2))
		display.color((1,1,1))
		display.drawRectangle(*self.bounds)
		display.popMatrix()
		
	def _getProgress(self):
		return self._progress
		
	def _setProgress(self, val):
		self._progress = max(0.0,min(val,1.0))
		
	progress = property(_getProgress, _setProgress)
