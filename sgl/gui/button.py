from sgl.gui.bin import Bin
from sgl.core.signals import Signal
from sgl.gui.label import Label
from sgl.gui.image import Image

class BasicButton(Bin):
	def __init__(self, child=None):
		super(BasicButton, self).__init__()
		self.onClick = Signal()
		self.onPress = Signal()
		self.onRelease = Signal()
		self.pressed = False
		self.active = False
		self.onMouseEnter(self.handleMouseEnter)
		self.onMouseExit(self.handleMouseExit)
		
		self.child = child
		self.pack()
		
	def handleMouseEnter(self):
		self.active = True
		
	def handleMouseExit(self):
		self.pressed = False
		self.active = False
		
	def handleMouseDown(self, event):
		if not self.pressed:
			self.onPress.emit()
			self.pressed = True
		return True
			
	def handleMouseUp(self, event):
		if self.pressed:
			self.onClick.emit()
			self.pressed = False
		return True
		
class Button(BasicButton):
	def __init__(self, text='', onClick=None):
		self.label = Label(text)
		super(Button, self).__init__(self.label)
		if onClick:
			self.onClick(onClick)
		
	def get_text(self):
		return self.label.text
		
	def set_text(self, v):
		self.label.text = v
		self.pack()
			
	text = property(get_text, set_text)
	
class ImageButton(BasicButton):
	def __init__(self, texture = None, onClick=None):
		self.image = Image(texture)
		super(ImageButton, self).__init__(self.image)
		if onClick:
			self.onClick(onClick)
		
	def get_texture(self):
		return self.image.texture
		
	def set_texture(self, v):
		self.image.texture = v
		self.pack()
			
	texture = property(get_texture, set_texture)
		
