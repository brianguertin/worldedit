from sgl.gui.label import Label
from sgl.app.app import Key
from sgl.core.signals import Signal
from sgl.gui.enums import Align
import string

CHARACTERS = string.digits + string.letters + string.punctuation + ' '

class Entry(Label):
	def __init__(self, text = '', mask = CHARACTERS):
		super(Entry, self).__init__(text)
		self.mask = mask
		self.align = Align.MIDDLELEFT
		self.onFocus(Key.repeat_enable)
		self.onBlur(Key.repeat_disable)
		self.onSubmit = Signal()
		
	def handleKeyDown(self, event):
		k = event.key
		if k == Key.BACKSPACE:
			if len(self.text):
				self.text = self.text[:-1]
		elif k == Key.RETURN:
			self.onSubmit.emit()
		elif event.unicode in self.mask:
			self.text += event.unicode
			
class PasswordEntry(Entry):
	def __init__(self, text = '', mask = CHARACTERS):
		super(PasswordEntry, self).__init__('', CHARACTERS)
		self.set_text(text)
		
	def get_text(self):
		return self._real_text
		
	def set_text(self, v):
		self._real_text = str(v)
		super(PasswordEntry, self).set_text('*' * len(self._real_text))
		
	text = property(get_text, set_text)
