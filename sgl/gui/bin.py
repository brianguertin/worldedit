from __future__ import division
from widget import Widget

class Bin(Widget):
	def __init__(self):
		super(Bin, self).__init__()
		self._child = None
		
	def handleKeyDown(self, event):
		if self._child:
			return self._child.handleKeyDown(event)
			
	def handleKeyUp(self, event):
		if self._child:
			return self._child.handleKeyUp(event)
		
	def handleMouseMotion(self, event):
		if self._child:
			return self._child.handleMouseMotion(event)
		
	def handleMouseDown(self, event):
		if self._child:
			return self._child.handleMouseDown(event)

	def handleMouseUp(self, event):
		if self._child:
			return self._child.handleMouseUp(event)
					
	def set_child(self, child):
		if child != self._child:
			self._child = child
			if self._child:
				self._updateRequestedSize()
				self._child.onRequestSize(self._updateRequestedSize)
				self.pack()
				
	def get_child(self):
		return self._child
		
	child = property(get_child, set_child)
					
	def pack(self):
		if self._child:
			self._child.x = self.x
			self._child.y = self.y
			self._child.width = self.width
			self._child.height = self.height
			self._child.pack()
			
	def _updateRequestedSize(self):
		self.requestedSize = self._child.requestedSize
		
