from sgl.gui.widget import Widget
from sgl.gui.enums import Align, Stretch
from sgl.video.texture import Texture
from sgl.core.tuples import Point

class Image(Widget):
	def __init__(self, texture=None, flip = False):
		super(Image, self).__init__()
		self.align = Stretch.FILL
		self.set_texture(texture)
		self.flip = flip
			
	def get_texture(self):
		return self._texture
		
	def set_texture(self, texture):
		if isinstance(texture, basestring):
			self._texture = Texture(texture)
			self.requestedSize = self._texture.size
		elif texture:
			self._texture = texture
			self.requestedSize = self._texture.size
		else:
			self._texture = None
			self.requestedSize = 0,0
		
	texture = property(get_texture, set_texture)
			
	def draw(self, display):
		if not self._texture:
			return
		display.pushMatrix()
		display.translate(self.position + self.size / 2)
		display.color((1.0,1.0,1.0))
		if not self.flip:
			display.scale(1.0,-1.0)
		display.drawObjectScaled(self._texture, self.size)
		display.popMatrix()
		
	@property
	def offset(self):
		tex = self._texture
		ratio = min(self.width / tex.width, self.height / tex.height)
		offsetx = (self.width - tex.width * ratio) / 2
		offsety = (self.height - tex.height * ratio) / 2
		return Point(offsetx, offsety)
