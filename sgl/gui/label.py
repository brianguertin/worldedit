from sgl.gui.widget import Widget
from sgl.gui.enums import Align

class Label(Widget):
	def __init__(self, text='', align = Align.MIDDLECENTER):
		super(Label, self).__init__()
		self.text = text
		self.align = align
		
	def __repr__(self):
		return self.text

	def get_text(self):
		return self._text
		
	def set_text(self, v):
		self._text = str(v or '')
		if v:
			self.texture = self.theme.font.render(v)
			self.requestedSize = self.texture.size
		else:
			self.texture = None
			self.requestedSize = 0, 0
			
	text = property(get_text, set_text)
