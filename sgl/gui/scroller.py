from sgl.gui.widget import Widget
from sgl.app.input import Mouse

class Scroller(Widget):
	def __init__(self, child = None):
		super(Scroller, self).__init__()
		self.child = child
		self.verticalScroll = True
		self.horizontalScroll = False
		self.scrollx = 0.0
		self.scrolly = 0.0
		self.scrollspeed = 50.0
		
	def handleKeyDown(self, event):
		if self.child:
			return self.child.handleKeyDown(event)
			
	def handleKeyUp(self, event):
		if self.child:
			return self.child.handleKeyUp(event)
		
	def handleMouseMotion(self, event):
		if self.child:
			return self.child.handleMouseMotion(event)
		
	def handleMouseDown(self, event):
		if self.child:
			r = self.child.handleMouseDown(event)
			if event.button == Mouse.WHEELUP:
				self.scrolly = max(self.scrolly - self.scrollspeed, 0.0)
			elif event.button == Mouse.WHEELDOWN:
				self.scrolly += self.scrollspeed
			self.pack()
			return r

	def handleMouseUp(self, event):
		if self.child:
			return self.child.handleMouseUp(event)
			
	def pack(self):
		if self.child:
			self.child.x = self.x - self.scrollx
			self.child.y = self.y - self.scrolly
			
			if self.horizontalScroll:
				self.child.width = max(self.width, self.child.requestedWidth)
			else:
				self.child.width = self.width
				
			if self.verticalScroll:
				self.child.height = max(self.height, self.child.requestedHeight)
			else:
				self.child.height = self.height
				
			self.child.pack()
		
	
