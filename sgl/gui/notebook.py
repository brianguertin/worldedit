from sgl.gui.widget import Widget
from sgl.gui.listbox import ListBox
from sgl.gui.bin import Bin
from sgl.gui.box import VBox, HBox

class Notebook(Bin):
	def __init__(self, tab_position = 'top'):
		super(Notebook, self).__init__()
		self.contentBox = Bin()
		
		if tab_position == 'left':
			self.tabBox = ListBox()
			self.child = HBox()
			self.child.add(self.tabBox, self.contentBox)
			
		elif tab_position == 'right':
			self.tabBox = ListBox()
			self.child = HBox()
			self.child.add(self.contentBox, self.tabBox)
			
		elif tab_position == 'bottom':
			self.tabBox = ListBox(horizontal=True)
			self.child = VBox()
			self.child.add(self.contentBox, self.tabBox)
			
		else:
			self.tabBox = ListBox(horizontal=True)
			self.child = VBox()
			self.child.add(self.tabBox, self.contentBox)
		
		self.tabBox.onSelect(self._handle_select)
		self.tabBox.expand = False
		
		self.children = []
		self.onSelect = self.tabBox.onSelect
		
	def _handle_select(self):
		self.contentBox.child = self.tabBox.value
		
	def add(self, *widgets):
		for widget in widgets:
			if isinstance(widget, tuple):
				widget, title = widget
			else:
				title = 'Page %s' % len(self.children)
			self.children.append(widget)
			self.tabBox.add(title, value=widget)
			
			#if len(self.children) == 1:
			#	self.tabBox.select(0)
			
	def select(self, index):
		self.tabBox.select(index)
		
	@property
	def selectedIndex(self):
		return self.tabBox.selectedIndex
