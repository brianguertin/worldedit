from sgl.app.app import Event

class Controller:
	def __init__(self, widget):
		self.widget = widget
		self.active = False
		
	def input(self, event):
		w = self.widget
		if event.type == Event.MOUSEMOTION:
			if w.contains(event.pos):
				if not self.active:
					self.active = True
					w.onMouseEnter.emit()
				return w.handleMouseMotion(event)
			elif self.active:
				self.active = False
				w.onMouseExit.emit()
		elif event.type == Event.MOUSEBUTTONDOWN:
			if w.contains(event.pos):
				return w.handleMouseDown(event)
		elif event.type == Event.MOUSEBUTTONUP:
			if w.contains(event.pos):
				return w.handleMouseUp(event)
		elif event.type == Event.KEYDOWN:
			return w.handleKeyDown(event)
		elif event.type == Event.KEYUP:
			return w.handleKeyUp(event)
		
