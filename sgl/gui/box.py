from panel import Panel

class Box(Panel):
	def __init__(self):
		super(_Box, self).__init__()
		self.vertical = False
		self.onModifyChildren.connect(self._calculateRequestedSize)
		self.onChildRequestSize.connect(self._calculateRequestedSize)
		
	def _calculateRequestedSize(self):
		pass
		
	def pack(self):
		if len(self.children):
			if self.vertical:
				xsize = self.width
				ysize = self.height / len(self.children)
			else:
				xsize = self.width / len(self.children)
				ysize = self.height
			
			for i, child in zip(range(len(self.children)), self.children):
				child.x = self.x
				child.y = self.y
				if self.vertical:
					child.y += ysize * i
				else:
					child.x += xsize * i
				child.width = xsize
				child.height = ysize
				child.pack()

_Box = Box
				
class VBox(_Box):
	def __init__(self, *args, **kargs):
		super(VBox, self).__init__(*args, **kargs)
		self.vertical = True
		
	def _calculateRequestedSize(self):
		if len(self.children):
			width = max([w.requestedWidth for w in self.children])
			height = sum([w.requestedHeight for w in self.children])
			self.requestedSize = width, height
		else:
			self.requestedSize = 0,0
		
	def pack(self):
		if len(self.children):
			child_width = self.width
			expand_height = 0
			force_height = None
			
			expand_count = len([w for w in self.children if w.expand])
			if self.height > self.requestedHeight:
				if expand_count:
					expand_height = (self.height - self.requestedHeight) / expand_count
			else:
				force_height = self.height / len(self.children)
			
			offset = 0 # incremented as we position each widget
			
			for i, child in zip(range(len(self.children)), self.children):
				
				if force_height is not None:
					child_height = force_height
				else:
					child_height = child.requestedHeight
					if child.expand:
						child_height += expand_height
				
				child.x = self.x
				child.y = self.y + offset
				child.width = child_width
				child.height = child_height
				child.pack()
				
				offset += child_height
		
class HBox(_Box):
	def __init__(self, *args, **kargs):
		super(HBox, self).__init__(*args, **kargs)
		self.vertical = False
		
	def _calculateRequestedSize(self):
		if len(self.children):
			width = sum([w.requestedWidth for w in self.children])
			height = max([w.requestedHeight for w in self.children])
			self.requestedSize = width, height
		else:
			self.requestedSize = 0,0
		
	def pack(self):
		if len(self.children):
			child_height = self.height
			expand_width = 0
			force_width = None
			
			expand_count = len([w for w in self.children if w.expand])
			if self.width > self.requestedWidth:
				if expand_count:
					expand_width = (self.width - self.requestedWidth) / expand_count
			else:
				force_width = self.width  / len(self.children)
			
			offset = 0 # incremented as we position each widget
			
			for i, child in zip(range(len(self.children)), self.children):
				
				if force_width is not None:
					child_width = force_width
				else:
					child_width = child.requestedWidth
					if child.expand:
						child_width += expand_width
				
				child.x = self.x + offset
				child.y = self.y
				child.width = child_width
				child.height = child_height
				child.pack()
				
				offset += child_width
		
