from __future__ import division
from sgl.gui.widget import Widget
from sgl.core.signals import Signal

# FIXME: when removing widgets, make sure they are not active or focused

class Panel(Widget):
	def __init__(self):
		super(Panel, self).__init__()
		self.children = []
		self._activeChild = None
		self._focusedChild = None
		
		self.onModifyChildren = Signal()
		self.onChildRequestSize = Signal()
		
		self.onMouseExit(lambda:self.setActiveChild(None))
		
		# FIXME: should stay focused but process blur/focus events along with panel
		self.onBlur(lambda:self.setFocusedChild(None))
		
	def handleMouseMotion(self, event):
		for w in self.children:
			if w.contains(event.pos):
				self.setActiveChild(w)
				w.handleMouseMotion(event)
				return True
		else:
			self.setActiveChild(None)
		
	def handleMouseDown(self, event):
		for w in self.children:
			if w.contains(event.pos):
				self.setFocusedChild(w)
				if w.handleMouseDown(event):
					return True

	def handleMouseUp(self, event):
		for w in self.children:
			if w.contains(event.pos):
				if w.handleMouseUp(event):
					return True
		
	def handleKeyDown(self, event):
		if self._focusedChild:
			self._focusedChild.handleKeyDown(event)
			
	def handleKeyUp(self, event):
		if self._focusedChild:
			self._focusedChild.handleKeyUp(event)
					
	def setActiveChild(self, w):
		if self._activeChild != w:
			if self._activeChild:
				self._activeChild.onMouseExit.emit()
			self._activeChild = w
			if self._activeChild:
				self._activeChild.onMouseEnter.emit()
				
	def setFocusedChild(self, w):
		if self._focusedChild != w:
			if self._focusedChild:
				self._focusedChild.onBlur.emit()
			self._focusedChild = w
			if self._focusedChild:
				self._focusedChild.onFocus.emit()
					
	def add(self, *widgets):
		if widgets:
			for w in widgets:
				w.onRequestSize(self.onChildRequestSize.emit)
				self.children.append(w)
			self.onModifyChildren.emit()
			
	def remove(self, *widgets):
		if widgets:
			for w in widgets:
				w.onRequestSize.disconnect(self.onChildRequestSize.emit)
				self.children.remove(w)
			self.onModifyChildren.emit()
		
	def pop(self, index = -1):
		w = self.children.pop(index)
		w.onRequestSize.disconnect(self.onChildRequestSize.emit)
		self.onModifyChildren.emit()
		
	def clear(self):
		if len(self.children):
			for w in self.children:
				w.onRequestSize.disconnect(self.onChildRequestSize.emit)
			self.setActiveChild(None)
			self.children = []
			self.onModifyChildren.emit()
		
	def pack(self):
		for w in self.children:
			w.pack()
			
	def center(self, w):
		w.position = self.size / 2 - w.size / 2
					
