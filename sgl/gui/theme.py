from sgl.gui.enums import Align, Stretch

class Theme:
	Global = None
	def __init__(self, font_name='sans', font_size=16):
		from sgl.video.font import Font
		self.font = Font(font_name, font_size)
		self.cursor = None
		self.colors = {
			'button': {
				'background':(0.5,0.5,1),
				'border':(0.7,0.7,1),
			},
			'button:pressed': {
				'background':(0.45,0.45,1),
				'border':(0.7,0.7,1),
			},
			'button:active': {
				'background':(0.6,0.6,1),
				'border':(0.7,0.7,1),
			},
			'listbox': {
				'background':(0.4,0.4,1),
				'border':(0.7,0.7,1),
				'selected': {
					'background':(0.6,0.6,1),
					'border':(0.8,0.8,1),
				},
			},
		}
		
	def drawInterface(self, widget, display):
		display.pushMatrix()
		display.pushOrtho(left=0, right=display.width, top=0, bottom=display.height)
		#display.translate((0.375,0.375))
		self.draw(widget, display)
		display.popOrtho()
		display.popMatrix()
		
	def draw(self, widget, display):
		if hasattr(widget, 'draw'):
			widget.draw(display)
		else:
			fn = 'draw_%s' % widget.__class__.__name__
			if hasattr(self, fn):
				getattr(self, fn)(widget, display)
			elif hasattr(widget, 'child'):
				if widget.child:
					self.draw(widget.child, display)
			elif hasattr(widget, 'children'):	
				for c in widget.children:
					self.draw(c, display)
		
	def draw_Button(self, w, display):
		if w.pressed:
			style = self.colors['button:pressed']
		elif w.active:
			style = self.colors['button:active']
		else:
			style = self.colors['button']
		display.color(style['background'])
		display.drawFilledRoundedRectangle(*w.bounds, radius=10)
		display.color(style['border'])
		display.drawRoundedRectangle(*w.bounds, radius=10)
		
		if w.child:
			self.draw(w.child, display)
		
	def draw_Box(self, w, display):
		for c in w.children:
			self.draw(c, display)
			
	def draw_Image(self, w, display):
		if w.texture:
			display.color((1,1,1))
			display.pushMatrix()
			if w.align == Stretch.BOTH:
				display.translate((w.x + w.width / 2, w.y + w.height / 2))
				display.drawTexture(w.texture, w.size)
			elif w.align == Stretch.FILL:
				display.translate((w.x + w.width / 2, w.y + w.height / 2))
				scale = min(w.width / w.texture.width, w.height / w.texture.height)
				display.scale(scale, scale)
				display.drawTexture(w.texture)
			else:
				display.translate(w.position)
				if w.align & Align.RIGHT:
					display.translate((w.width - w.texture.width / 2, 0))
				elif w.align & Align.CENTER:
					display.translate((w.width / 2, 0))
				else: # Align.LEFT
					display.translate((w.texture.width / 2, 0))
				if w.align & Align.BOTTOM:
					display.translate((0, w.height - w.texture.height / 2))
				if w.align & Align.MIDDLE:
					display.translate((0, w.height / 2))
				else:
					display.translate((0, w.texture.height / 2))
				display.drawTexture(w.texture)
			display.popMatrix()
		
	def draw_ListBox(self, w, display):
		style = self.colors['listbox']
		selected = style['selected']
		display.color(style['background'])
		display.drawFilledRectangle(*w.bounds)
		display.color(style['border'])
		display.drawRectangle(*w.bounds)
		if w.selected:
			items = w.multiselect and w.selected or [w.selected]
			for item in items:
				display.color(selected['background'])
				display.drawFilledRectangle(*item.bounds)
				display.color(selected['border'])
				display.drawRectangle(*item.bounds)
		if w.child:
			self.draw(w.child, display)
		
	draw_Label = draw_Image
			
	draw_VBox = draw_Box
	draw_HBox = draw_Box
	draw_Entry = draw_Label
	
	def draw_Entry(self, w, d):
		d.color((1,1,1))
		d.drawRectangle(*w.bounds)
		self.draw_Label(w, d)
		
	draw_PasswordEntry = draw_Entry
