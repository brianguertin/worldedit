from sgl.gui.widget import Widget
from sgl.video.texture import Texture
from sgl.video.framebuffer import FrameBuffer

class Canvas(Widget):
	def __init__(self, width, height):
		super(Canvas, self).__init__()
		self.texture = Texture.createBuffer(width, height)
		self.context = FrameBuffer(self.texture)
		self.size = (width, height)
		
	def draw(self, display):
		display.pushMatrix()
		display.translate(self.position)
		display.translate(self.size / 2)
		display.color((1,1,1))
		display.drawTexture(self.texture, self.texture.size)
		display.popMatrix()
