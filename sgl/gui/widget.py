from sgl.core.signals import Signal
from sgl.core.tuples import *
from sgl.gui.theme import Theme

class Widget(object):
	def __init__(self, theme = None):
		self.theme = theme or Theme.Global
		self.x, self.y = 0.0, 0.0
		self.width, self.height = 0.0, 0.0
		self.requestedWidth, self.requestedHeight = 0.0, 0.0
		self.expand = True
		
		# Data Signals
		self.onRequestSize = Signal()
		self.onSetBounds = Signal()
		
		# Event Signals
		self.onMouseEnter = Signal()
		self.onMouseExit = Signal()
		self.onFocus = Signal()
		self.onBlur = Signal()
		
		# Input Signals
		self.onMouseMotion = Signal()
		self.onMouseDown = Signal()
		self.onMouseUp = Signal()
		self.onKeyDown = Signal()
		self.onKeyUp = Signal()
		
	def get_position(self):
		return Point(self.x, self.y)
		
	def set_position(self, v):
		self.x, self.y = v[0], v[1]
		
	position = property(get_position, set_position)
		
	def get_size(self):
		return Size(self.width, self.height)
		
	def set_size(self, v):
		self.width, self.height = v[0], v[1]
		
	size = property(get_size, set_size)
	
	def set_requested_size(self, size):
		size = map(float, size)
		if size != self.requestedSize:
			self.requestedWidth, self.requestedHeight = size
			self.onRequestSize.emit()
		
	def get_requested_size(self):
		return self.requestedWidth, self.requestedHeight
		
	requestedSize = property(get_requested_size, set_requested_size)
		
	@property
	def x2(self):
		return self.x + self.width
		
	@property
	def y2(self):
		return self.y + self.height
		
	def get_bounds(self):
		return (self.x, self.y), (self.x2, self.y2)
		
	def set_bounds(self, bounds):
		if bounds != self.bounds:
			self.position = bounds[0]
			self.size = bounds[1][0] - self.x, bounds[1][1] - self.y
			self.onSetBounds.emit()
		
	bounds = property(get_bounds, set_bounds)
		
	def pack(self):
		pass
		
	def update(self, ticks):
		pass
		
	def contains(self, p):
		return p[0] > self.x and p[0] < self.x2 and p[1] > self.y and p[1] < self.y2
		
	# Event stubs
	def handleKeyDown(self, e):
		self.onKeyDown.emit(e)
		return False
		
	def handleKeyUp(self, e):
		self.onKeyUp.emit(e)
		return False
		
	def handleMouseUp(self, e):
		self.onMouseUp.emit(e.translate(-self.position))
		return False
		
	def handleMouseDown(self, e):
		self.onMouseDown.emit(e.translate(-self.position))
		return False
		
	def handleMouseMotion(self, e):
		self.onMouseMotion.emit(e.translate(-self.position))
		return False
