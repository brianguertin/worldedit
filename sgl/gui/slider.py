from sgl.gui.widget import Widget
from sgl.core.signals import Signal
from sgl.core.tuples import Rect
from sgl.app.input import Mouse

SLIDER_WIDTH = 10 # TODO: move to self.theme

class Slider(Widget):
	def __init__(self, value = 0.0, minValue = 0.0, maxValue = 1.0, onEdit = None):
		super(Slider, self).__init__()
		self.active = self.pressed = False
		self.slider = Rect()
		self._value = value
		self.minValue = minValue
		self.maxValue = maxValue
		self.reverse = False
		
		self.onEdit = Signal()
		if onEdit:
			self.onEdit(onEdit)

	def draw(self, display):
		display.pushMatrix()
		display.color((0.3,0.3,0.5))
		display.drawFilledRectangle((self.x, self.y + 0.4 * self.height), (self.x2, self.y2 - 0.4 * self.height))
		if self.active:
			display.color((0.7,0.7,1.0))
		else:
			display.color((0.4,0.4,1.0))
		display.translate(self.position + self.slider.position)
		display.drawFilledRoundedRectangle((0,0), self.slider.size, radius=5)
		display.color((0.2,0.2,1.0))
		display.drawRoundedRectangle((0,0), self.slider.size, radius=5)
		display.popMatrix()
	
	def get_value(self):
		if self.reverse:
			return self.maxValue - self._value
		else:
			return self._value
			
	def set_value(self, val):
		if val != self._value:
			if val <= self.minValue:
				self._value = self.minValue
			elif val >= self.maxValue:
				self._value = self.maxValue
			else:
				self._value = val
			self.positionSlider()
			self.onEdit.emit()
	
	value = property(get_value, set_value)
	
	"""
	public void setMinValue(float val) {
		self.minValue = val
		if (self._value < self.minValue)
			self._value = self.minValue
	}
	public void setMaxValue(float val) {
		self.maxValue = val
		if (self._value > self.maxValue)
			self._value = self.maxValue
	}
	
	public float getMinValue() {
		return self.minValue
	}
	public float getMaxValue() {
		return self.maxValue
	}
	"""
	
	def handleMouseMotion(self, event):
		x, y = event.pos
		if self.pressed:
			self.slider.x = x - self.x - self.slider.w/2
			if self.slider.x < 0.0:
				self.slider.x = 0.0
			elif self.slider.x + self.slider.w > self.width:
				self.slider.x = self.width - self.slider.w
			self.value = self.minValue + ((self.slider.x) / (self.width - self.slider.w)) * (self.maxValue - self.minValue)
			return True
		
		else:
			self.active = self.slider.contains((x - self.x, y - self.y))
			return (self.active or self.pressed)

	def handleMouseDown(self, event):
		print 'Slider:', self.slider
		x, y = event.pos
		if self.contains(event.pos):
			if x > self.x + self.slider.x and x < self.x + self.slider.x + self.slider.w and Mouse.LEFT == event.button:
				self.pressed = True
			else:
				self.slider.x = x - self.x - self.slider.w/2
				if self.slider.x < 0.0:
					self.slider.x = 0.0
				elif self.slider.x + self.slider.w > self.width:
					self.slider.x = self.width - self.slider.w
				self.value = self.minValue + ((self.slider.x) / (self.width - self.slider.w)) * (self.maxValue - self.minValue)
				self.pressed = True
			
			return True
		
		else:
			return False
	
	def handleMouseUp(self, event):
		if Mouse.LEFT == event.button:
			self.pressed = False
		return self.contains(event.pos)

	def positionSlider(self):
		diff = self.maxValue - self.minValue
		if diff > 0:
			self.slider.x = ((self._value - self.minValue)/diff) * (self.width - self.slider.w)
		else:
			self.slider.x = self.width / 2 - self.slider.w / 2
		print 'Slider:', self.slider
	
	def pack(self):
		self.positionSlider()
		self.slider.h = self.height
		self.slider.w = min(SLIDER_WIDTH, self.width)
