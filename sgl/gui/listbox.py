from sgl.gui.bin import Bin
from sgl.core.signals import Signal
from sgl.gui.label import Label
from sgl.gui.box import VBox, HBox
from sgl.core.containers import TwoWayDict

class ListBox(Bin):
	def __init__(self, items = [], multiselect = False, horizontal = False):
		super(ListBox, self).__init__()
		
		if horizontal:
			self.child = HBox()
		else:
			self.child = VBox()
			
		self.choices = {}
		self.onSelect = Signal()
		self.multiselect = multiselect
		if self.multiselect:
			self.selected = []
		else:
			self.selected = None
			
		for item in items:
			self.add(item)
			
	def select(self, index):
		self.selectedIndex = index
		self.selected = self.child.children[index]
		self.onSelect.emit()
			
	@property
	def value(self):
		if self.multiselect:
			return [self.choices[w] for w in self.selected]
		else:
			return self.selected and self.choices[self.selected]
		
	def add(self, text='', value=None, widget=None):
		if not widget:
			widget = Label(text)
		self.choices[widget] = value or text
		self.child.add(widget)
		
	def remove(self, value=None, widget=None):
		for k, v in self.choices.copy().items():
			if k == widget or v == value:
				self.child.remove(k)
				del self.choices[k]
		
	def handleMouseDown(self, event):
		for i, widget in zip(range(len(self.child.children)), self.child.children):
			if widget.contains(event.pos):
				self.selected = widget
				self.selectedIndex = i
				break
		else:
			self.selected = None
			self.selectedIndex = None
		self.onSelect.emit()
		if self.selected:
			return True

