__all__ = ['app']

from sgl.core.log import log as _log_function

try:
	# Python 2.x
	import __builtin__
	__builtin__.__dict__['log'] = _log_function
except ImportError:
	# Python 3
	import builtins
	builtins.log = _log_function


