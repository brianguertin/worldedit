from sgl.video.rendertarget import RenderTarget
from OpenGL.GL import *
from OpenGL.GLU import *
from OpenGL.GL.EXT.framebuffer_object import *
from pygame.locals import *
from sgl.core.tuples import *

class FrameBuffer(RenderTarget):
	""" Off-screen rendering frame buffer """
	
	def __init__(self, texture):
		super(FrameBuffer, self).__init__()
		if not glInitFramebufferObjectEXT():
			raise Exception('Framebuffer extension not available')
		
		self.tex = texture
		
		self._fbo = glGenFramebuffersEXT(1)
		glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, self._fbo)
		glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT0_EXT, GL_TEXTURE_2D, self.tex.texture, 0)
		glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0)
	
	def __del__(self):
		glDeleteFramebuffersEXT(1, [self._fbo])
		
	def __enter__(self):
		self.bind()
		return self
		
	def __exit__(self, type, value, traceback):
		self.unbind()
	
	def bind(self):
		if glCheckFramebufferStatusEXT(GL_FRAMEBUFFER_EXT) != GL_FRAMEBUFFER_COMPLETE_EXT:
			raise Exception('Framebuffer error')
		glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, self._fbo)
		
		glPushAttrib(GL_VIEWPORT)
		glViewport(0, 0, self.tex.width, self.tex.height)
		
		glMatrixMode(GL_PROJECTION)
		glPushMatrix()
		glLoadIdentity()
		glOrtho(0, self.tex.width, 0, self.tex.height, -1, 1)
		
		glMatrixMode(GL_MODELVIEW)
		glLoadIdentity()
		glPushMatrix()
		
	def unbind(self):
		glPopMatrix()
		glPopAttrib()
		glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0)
		
		glMatrixMode(GL_PROJECTION)
		glPopMatrix()
		glMatrixMode(GL_MODELVIEW)
		glLoadIdentity()
		
	@property
	def width(self):
		return self.tex.width
		
	@property
	def height(self):
		return self.tex.height
