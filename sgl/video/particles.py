from sgl.core.tuples import Point
import random

FADE_DURATION = 250.0

def _is_alive(p):
	return p.life > 0

class ParticleSystem(object):
	def __init__(self):
		self.particles = []
		
	def new(self):
		p = Particle()
		self.particles.append(p)
		return p
		
	def add(self, p):
		if p.life > 0:
			self.particles.append(p)
			
	def text(self, text, *args, **kargs):
		from sgl.video.font import get_default_font
		p = self.new()
		kargs['flip_y'] = False
		p.texture = get_default_font().render(text, *args, **kargs)
		p.size = Point(30, 1)
		return p
		
	def update(self, ticks):
		def upd(p):
			p.update(ticks)
			p.life -= ticks
			
		map(upd, self.particles)
		
		self.particles = filter(_is_alive, self.particles)
		
	def draw(self, display, scale=1):
		def draw(p):
			if p.life < FADE_DURATION:
				display.setAlpha(p.life / FADE_DURATION)
			display.drawTextureScaledAt(p.texture, p.pos, p.size * scale)
			
		display.pushAlpha()
		display.color((1,1,1))
		map(draw, self.particles)
		display.popAlpha()
			
class Particle(object):
	def __init__(self, **kargs):
		self.pos = Point(0,0)
		self.vel = Point(0,0)
		self.life = 0
		self.texture = None
		self.size = Point(1, 1)
		
		for k,v in kargs.items():
			if not hasattr(self, k):
				raise AttributeError('%s has no attribute %s' % (self.__class__.__name__, k))
			setattr(self, k, v)
		#self.mod = 0
		
	def LINEAR(self, ticks):
		self.pos.x += self.vel.x * ticks
		self.pos.y += self.vel.y * ticks
		
	def RANDOM(self, ticks):
		self.pos.x += random.uniform(-1,1) * self.vel.x
		self.pos.y += random.uniform(-1,1) * self.vel.y

	update = LINEAR
