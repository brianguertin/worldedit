from sgl.video.texture import Texture
from pygame import font
import pygame

DEFAULT_FONT = None

def _sdl_color(color):
	r, g, b = color
	return map(int, (r*255, g*255, b*255))

def get_default_font():
	global DEFAULT_FONT
	if not DEFAULT_FONT:
		DEFAULT_FONT = Font('sans')
	return DEFAULT_FONT

class Font:
	def __init__(self, name, bold=False, italic=False, size=16):
		font.init()
		# FIXME: search local folders first
		f = font.match_font(name, bold, italic)
		if f:
			self.font = font.Font(f, size)
		else:
			raise Exception('Font not found: %s' % name)
			
	def render(self, text, color=(1,1,1), background=None, antialias=True, flip_y=True):
		color = _sdl_color(color)
		if background:
			s = self.font.render(str(text), antialias, color, background)
		else:
			s = self.font.render(str(text), antialias, color)
		return Texture(s, flip_y=flip_y)
