from __future__ import division
from sgl.video.rendertarget import RenderTarget
import pygame
from OpenGL.GL import *
from OpenGL.GLU import *
from pygame.locals import *
from sgl.core.tuples import *

class Display(RenderTarget):
	def __init__(self):
		super(Display, self).__init__()
		self.fullscreen = False
		self.resizable = False
		self.framed = True
		self.width = 640
		self.height = 480
		self.motion_blur = 0 #.25
		self.orthoStack = []
		
	def readOptions(self, x):
		x.query(self, 'width')
		x.query(self, 'height')
		x.query(self, 'fullscreen')
		x.query(self, 'resizable')
		x.query(self, 'framed')
		x.query(self, 'motion_blur')
		
	def init(self, options=None):
		if options is not None:
			self.readOptions(options)
		flags = OPENGL | DOUBLEBUF
		if self.fullscreen:
			flags |= FULLSCREEN
		else:
			if self.resizable:
				flags |= RESIZABLE
			if not self.framed:
				flags |= NOFRAME
		pygame.display.set_mode((self.width, self.height), flags)
		self.setup()
		
	def setup(self):
		glViewport(0, 0, self.width, self.height)
		glClearColor(0.0, 0.0, 0.0, 0.0)
		glClearDepth(1.0)
		glEnable(GL_TEXTURE_2D)
		glEnable(GL_BLEND)
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA)
		glColor4f(1.0,1.0,1.0,1.0)
		self.setOrtho(left=0, right=self.width, bottom=0, top=self.height)
		glClear(GL_ACCUM_BUFFER_BIT)
		
	def pushOrtho(self, **kargs):
		self.orthoStack.append(kargs)
		self.setOrtho(**kargs)
		
	def popOrtho(self):
		if len(self.orthoStack):
			self.orthoStack.pop()
		if len(self.orthoStack):
			self.setOrtho(**self.orthoStack[-1])
		else:
			self.setOrtho(left=0, right=self.width, bottom=0, top=self.height)
		
	def setOrtho(self, left, right, bottom, top):
		glMatrixMode(GL_PROJECTION)
		glLoadIdentity()
		glOrtho(left, right, bottom, top, -1, 1)
		glMatrixMode(GL_MODELVIEW)
		glLoadIdentity()
		
	def update(self, flip=True):
		if self.motion_blur > 0:
			glAccum(GL_MULT, self.motion_blur)
			glAccum(GL_ACCUM, 1-self.motion_blur)
			glAccum(GL_RETURN, 1.0)
		
		if flip:
			pygame.display.flip()
		#pygame.time.wait(1000/framerate)	
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
		glLoadIdentity()
		glColor4f(1.0,1.0,1.0,1.0)
