import pygame
from OpenGL.GL import *
from pygame.locals import *
from sgl.core import path
from sgl.core.tuples import Point, Size

def _power_of_two(num):
	value = 1
	while value < num:
		value <<= 1
	return value
	
class Texture(object):
	_cache = {}
	def __new__(cls, filename, flip_y=False):
		if isinstance(filename, basestring):
			f = path.find(filename)
			if f in Texture._cache:
				return Texture._cache[f]
			else:
				self = object.__new__(Texture)
				log('Loading: %s' % f, type='Texture')
				tex = pygame.image.load(f)
				self._init(tex)
				Texture._cache[f] = self
				return self
		else:
			self = object.__new__(Texture)
			self._init(filename, flip_y)
			return self
			
	def __deepcopy__(self, memo={}):
		memo[id(self)] = self
		return self
		
	@classmethod
	def fromFilename(cls, filename, colorkey=None):
		tex = pygame.image.load(filename)
		if colorkey:
			tex.set_colorkey(colorkey)
		self = object.__new__(cls)
		self._init(tex)
		return self
			
	def reload(self, filename):
		tex = pygame.image.load(filename)
		self._init(tex)
			
	def _init(self, textureSurface, flip_y=False):
		if flip_y:
			textureSurface = pygame.transform.flip(textureSurface, False, True)
		#textureSurface.set_colorkey(colorkey, RLEACCEL)
		
		self.width = textureSurface.get_width()
		self.height = textureSurface.get_height()
		self.fragment_lists = []
		
		# OpenGL textures must be a power of two

		width = _power_of_two(textureSurface.get_width())
		height = _power_of_two(textureSurface.get_height())
		self.xmod = float(self.width)/width
		self.ymod = float(self.height)/height
		
		surface = pygame.Surface((width,height),SWSURFACE|SRCALPHA,32)
		surface.blit(textureSurface,textureSurface.get_rect())
		textureData = pygame.image.tostring(surface, 'RGBA', 0)
		
		# Generate our texture from the power_of_two surface
		self.texture = glGenTextures(1)
		glBindTexture(GL_TEXTURE_2D, self.texture)
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST)
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST)
		#gluBuild2DMipmaps(GL_TEXTURE_2D, 4, surface.get_width(), surface.get_height(), GL_RGBA, GL_UNSIGNED_BYTE, textureData)
		glTexImage2D( GL_TEXTURE_2D, 0, 4, surface.get_width(), surface.get_height(), 0, GL_RGBA, GL_UNSIGNED_BYTE, textureData);
		glBindTexture(GL_TEXTURE_2D, 0)
		
		# Generate main display which draws the whole texture
		self.lst = None
		"""
		self.lst = glGenLists(1)
		glNewList(self.lst,GL_COMPILE)
		self.drawStretched(self.size)
		glEndList()
		"""
		
		#print 'Texture created from',file,'-',str(self.width) + 'x' + str(self.height),len(self.fragment_lists),'fragments'
		
	def __del__(self):
		pass
		#glDeleteLists(self.list,1)
		#glDeleteTextures(1,self.texture)
		#print 'Texture deleted'

	def draw(self, display=None):
		if self.lst:
			glCallList(self.lst)
		else:
			self.drawStretched(self.size)
		
	def drawStretched(self, size):
		hw = size[0] / 2
		hh = size[1] / 2
		glBindTexture(GL_TEXTURE_2D,self.texture);
 
		box = [(-hw, hh),   (hw, hh),     (hw, -hh),   (-hw, -hh)]
		tex = [(0,0), (self.xmod,0), (self.xmod,self.ymod), (0,self.ymod)]
	 
		glEnableClientState(GL_VERTEX_ARRAY)
		glEnableClientState(GL_TEXTURE_COORD_ARRAY)
	 
		glVertexPointer(2, GL_FLOAT, 0,box)
		glTexCoordPointer(2, GL_FLOAT, 0, tex)
	 
		glDrawArrays(GL_QUADS,0,4)
	 
		glDisableClientState(GL_VERTEX_ARRAY)
		glDisableClientState(GL_TEXTURE_COORD_ARRAY)
		
		glBindTexture(GL_TEXTURE_2D, 0)
		
	def _drawStretched(self, size):
		glBindTexture(GL_TEXTURE_2D, self.texture)
		glBegin(GL_QUADS)
		glTexCoord2f(0.0, 0.0);             glVertex2f(0.0, 0.0)	# Bottom Left Of The Texture and Quad
		glTexCoord2f(self.xmod, 0.0);       glVertex2f(size[0], 0.0)	# Bottom Right Of The Texture and Quad
		glTexCoord2f(self.xmod, self.ymod); glVertex2f(size[0],  size[1])	# Top Right Of The Texture and Quad
		glTexCoord2f(0.0, self.ymod);       glVertex2f(0.0, size[1])	# Top Left Of The Texture and Quad
		glEnd()
		glBindTexture(GL_TEXTURE_2D, 0)
		
	def drawFragment(self,x1,y1,x2,y2):
		if False:
			if x2 > self.width:
				x2 = self.width
			if y2 > self.height:
				y2 = self.height
			if x1 > x2 or y1 > y2:
				return

		tex_left = float(x1)/self.width*self.xmod
		tex_right = float(x2)/self.width*self.xmod
		tex_top = float(y1)/self.height*self.ymod
		tex_bottom = float(y2)/self.height*self.ymod
		
		hw = (x2 - x1) / 2
		hh = (y2 - y1) / 2
		
		glBindTexture(GL_TEXTURE_2D, self.texture)
		glBegin(GL_QUADS)
		glTexCoord2f(tex_left, tex_top); glVertex2f(-hw, -hh)	# Bottom Left Of The Texture and Quad
		glTexCoord2f(tex_right, tex_top); glVertex2f(hw, -hh)	# Bottom Right Of The Texture and Quad
		glTexCoord2f(tex_right, tex_bottom); glVertex2f(hw,  hh)	# Top Right Of The Texture and Quad
		glTexCoord2f(tex_left, tex_bottom); glVertex2f(-hw,  hh)	# Top Left Of The Texture and Quad
		glEnd()
		glBindTexture(GL_TEXTURE_2D, 0)
		
	@property
	def size(self):
		return Size(self.width, self.height)
		
	@property
	def filename(self):
		for name, tex in Texture._cache.items():
			if tex == self:
				return name
				
	@classmethod
	def createBuffer(cls, width, height):
		self = object.__new__(cls)
		self.lst = None
		self.width = width
		self.height = height
		width = _power_of_two(self.width)
		height = _power_of_two(self.height)
		self.xmod = float(self.width)/width
		self.ymod = float(self.height)/height

		self.texture = glGenTextures(1)
		glBindTexture(GL_TEXTURE_2D, self.texture)
		glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER, GL_NEAREST)
		glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER, GL_NEAREST)
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, None)
		glBindTexture(GL_TEXTURE_2D, 0)
		
		return self
		
	def region(self, bottom_left, top_right):
		x1, y1 = bottom_left
		x2, y2 = top_right
		if x1 is None:
			x1 = 0
		if y1 is None:
			y1 = 0
		if x2 is None:
			x2 = self.width
		elif x2 < 0:
			x2 = self.width + x2
		if y2 is None:
			y2 = self.height
		elif y2 < 0:
			y2 = self.height + y2
		return SubTexture(self, Point((x1,y1)), Point((x2,y2)))
		
class SubTexture(object):
	def __init__(self, parent, p1, p2):
		
		# Validate coords
		x1, y1 = p1
		x2, y2 = p2
		if x1 < 0 or y1 < 0 or x2 > parent.width or y2 > parent.height or x2 <= x1 or y2 <= y1:
			raise Exception('Invalid texture region %r with texture size %r' % (((x1,y1),(x2,y2)), (parent.width, parent.height)))
			
		self.parent = parent
		self.p1 = (p1[0] / parent.width * parent.xmod, p1[1] / parent.height * parent.ymod)
		self.p2 = (p2[0] / parent.width * parent.xmod, p2[1] / parent.height * parent.ymod)
		self.offset = p1
		self.size = Size(p2[0] - p1[0], p2[1] - p1[1])
		
	def draw(self, display=None):
		self.drawStretched(self.size)
		
	def drawStretched(self, size):
		hw = size[0] / 2
		hh = size[1] / 2
		glBindTexture(GL_TEXTURE_2D, self.parent.texture)
		glBegin(GL_QUADS)
		glTexCoord2f(self.p1[0], self.p1[1]); glVertex2f(-hw,  hh)	# Bottom Left Of The Texture and Quad
		glTexCoord2f(self.p2[0], self.p1[1]); glVertex2f( hw,  hh)	# Bottom Right Of The Texture and Quad
		glTexCoord2f(self.p2[0], self.p2[1]); glVertex2f( hw, -hh)	# Top Right Of The Texture and Quad
		glTexCoord2f(self.p1[0], self.p2[1]); glVertex2f(-hw, -hh)	# Top Left Of The Texture and Quad
		glEnd()
		glBindTexture(GL_TEXTURE_2D, 0)
	
	@property
	def width(self):
		return self.size[0]
		
	@property
	def height(self):
		return self.size[1]

	def region(self, bottom_left, top_right):
		x1, y1 = bottom_left
		x2, y2 = top_right
		if x1 is None:
			x1 = 0
		if y1 is None:
			y1 = 0
		if x2 is None:
			x2 = self.width
		elif x2 < 0:
			x2 = self.width + x2
		if y2 is None:
			y2 = self.height
		elif y2 < 0:
			y2 = self.height + y2
		p1 = (x1 + self.offset[0], y1 + self.offset[1])
		p2 = (x2 + self.offset[0], y2 + self.offset[1])
		return SubTexture(self.parent, Point(p1), Point(p2))
