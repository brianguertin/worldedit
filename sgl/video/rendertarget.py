from OpenGL.GL import *
from OpenGL.GLU import *
from sgl.core.tuples import *
from math import cos, sin, degrees, pi

class Viewport:
	def __init__(self):
		self.viewport = self.modelview = None
		
	def transform(self, d):
		d.translate(self.modelview[3][:2])
		d.scale(self.modelview[0][0], self.modelview[1][1])
		
	def scale(self, val):
		return Point(val[0] / self.modelview[0][0], -val[1] / self.modelview[1][1])
		
	def unProject(self, pos):
		xmod, ymod = self.modelview[3][:2]
		xscale, yscale = self.modelview[0][0], self.modelview[1][1]
		return Point((pos[0] - xmod) / xscale, ((self.viewport[3] - pos[1]) - ymod) / yscale)
	
	def _unProject(self, pos):
		screenx, screeny = pos
		
		winX = screenx
		winY = self.viewport[3] - screeny
		winZ = glReadPixels(int(winX), int(winY), 1, 1, GL_DEPTH_COMPONENT, GL_FLOAT)

		posX, posY, posZ = gluUnProject(winX, winY, winZ, self.modelview, self.projection, self.viewport)
		return Point(posX, posY)
		
class RenderTarget(object):
	def __init__(self):
		self._increment = 0.1
		self._color = (1.0, 1.0, 1.0)
		self.alphaStack = [1.0]
		
	def translate(self, p):
		p = Point(p)
		glTranslatef(p[0], p[1], 0)
		
	def scale(self, x, y=None):
		if y is None:
			y = x
		glScalef(x, y, 1.0)
		
	def rotate(self, a):
		glRotatef(degrees(a), 0, 0, 1)
		
	def color(self, c, alpha=None):
		self._color = c
		glColor4f(c[0], c[1], c[2], alpha or self.alphaStack[-1])
		
	def pushAlpha(self, a = 1.0):
		alpha = self.alphaStack[-1] * a
		self.alphaStack.append(alpha)
		self.color(self._color, alpha)
		
	def setAlpha(self, a):
		self.color(self._color, a)
		
	def popAlpha(self):
		self.alphaStack.pop()
		self.color(self._color, self.alphaStack[-1])
		

	def pushMatrix(self):
		glPushMatrix()
		
	def popMatrix(self):
		glPopMatrix()
		
	def drawPoint(self, p):
		glBegin(GL_POINTS)
		glVertex2f(p[0], p[1])
		glEnd()
		
	def drawLine(self, p):
		glBegin(GL_LINES)
		glVertex2f(0, 0)
		glVertex2f(p[0], p[1])
		glEnd()
		
	def drawBox(self, w, h = None):
		""" w and h are horizontal and vertical extents (half-width and half-height) """
		h = h or w
		glBegin(GL_LINE_LOOP)
		glVertex2f(-w, -h)
		glVertex2f(-w, h)
		glVertex2f(w, h)
		glVertex2f(w, -h)
		glEnd()
		
	def drawFilledBox(self, w, h = None):
		""" w and h are horizontal and vertical extents (half-width and half-height) """
		h = h or w
		glBegin(GL_QUADS)
		glVertex2f(-w, -h)
		glVertex2f(-w, h)
		glVertex2f(w, h)
		glVertex2f(w, -h)
		glEnd()
		
	def drawRectangle(self, p1, p2):
		glBegin(GL_LINE_LOOP)
		glVertex2f(p1[0], p1[1])
		glVertex2f(p1[0], p2[1])
		glVertex2f(p2[0], p2[1])
		glVertex2f(p2[0], p1[1])
		glEnd()
		
	def drawFilledRectangle(self, p1, p2):
		glBegin(GL_QUADS)
		glVertex2f(p1[0], p1[1])
		glVertex2f(p1[0], p2[1])
		glVertex2f(p2[0], p2[1])
		glVertex2f(p2[0], p1[1])
		glEnd()
		
	def _circlePoints(self, x, y, start, end, radius, increment=None):
		inc = increment or self._increment
		i = start
		while i > end:
			glVertex2f(x + cos(i) * radius, y + sin(i) * radius)
			i -= inc
		
	def drawFilledRoundedRectangle(self, p1, p2, radius):
		glBegin(GL_POLYGON)
		
		# Bottom Left
		self._circlePoints(p1[0] + radius, p1[1] + radius, start = pi * 1.5, end = pi, radius=radius)
		
		# Top Left
		self._circlePoints(p1[0] + radius, p2[1] - radius, start = pi, end = pi * 0.5, radius=radius)
		
		# Top Right
		self._circlePoints(p2[0] - radius, p2[1] - radius, start = pi * 0.5, end = 0, radius=radius)

		# Bottom Right
		self._circlePoints(p2[0] - radius, p1[1] + radius, start = pi * 2, end = pi * 1.5, radius=radius)
		
		glEnd()
		
	def drawRoundedRectangle(self, p1, p2, radius):
		glBegin(GL_LINE_LOOP)
		
		# Bottom Left
		self._circlePoints(p1[0] + radius, p1[1] + radius, start = pi * 1.5, end = pi, radius=radius)
		
		# Top Left
		self._circlePoints(p1[0] + radius, p2[1] - radius, start = pi, end = pi * 0.5, radius=radius)
		
		# Top Right
		self._circlePoints(p2[0] - radius, p2[1] - radius, start = pi * 0.5, end = 0, radius=radius)

		# Bottom Right
		self._circlePoints(p2[0] - radius, p1[1] + radius, start = pi * 2, end = pi * 1.5, radius=radius)
		
		glEnd()
		
	def drawPolygon(self, vertices):
		glBegin(GL_LINE_LOOP)
		for v in vertices:
			glVertex2f(v[0], v[1])
		glEnd()
		
	def drawFilledPolygon(self, vertices):
		glBegin(GL_POLYGON)
		for v in vertices:
			glVertex2f(v[0], v[1])
		glEnd()
		
	def drawCircle(self, radius, pos=(0,0)):
		glBegin(GL_LINE_LOOP)
		i = 0.0
		while i < pi * 2:
			glVertex2f(cos(i) * radius + pos[0], sin(i) * radius + pos[1])
			i += self._increment
		glEnd()
		
	def drawObject(self, tex, size=None):
		if size:
			self.drawObjectStretched(tex, size)
		else:
			tex.draw(self)
	drawTexture = drawObject
		
	def drawObjectStretched(self, tex, size):
		""" Stretch object to specific size and shape, object requires width, height, and draw() """
		size = Size(size)
		self.pushMatrix()
		self.scale(size.x / tex.width, size.y / tex.height)
		tex.draw(self)
		self.popMatrix()
	drawTextureStretched = drawObjectStretched
		
	def drawObjectScaled(self, tex, size):
		""" Scale object to within a certain size, object requires width, height, and draw() """
		size = Size(size)
		ratio = min(size.x / tex.width, size.y / tex.height)
		self.pushMatrix()
		self.scale(ratio)
		tex.draw(self)
		self.popMatrix()
	drawTextureScaled = drawObjectScaled
		
	def drawObjectAt(self, tex, point):
		p = Point(point)
		self.pushMatrix()
		self.translate(p)
		tex.draw(self)
		self.popMatrix()
	drawTextureAt = drawObjectAt
		
	def drawObjectScaledAt(self, tex, point, size):
		self.pushMatrix()
		self.translate(point)
		self.drawObjectScaled(tex, size)
		self.popMatrix()
	drawTextureScaledAt = drawObjectScaledAt
			
	"""
	def drawCircle(self, radius, size = 1.0):
		inner_r = radius - (size / 2)
		outer_r = radius + (size / 2)
		glBegin(GL_QUAD_STRIP)
		i = 0.0
		while i < pi * 2:
			glVertex2f(cos(i) * outer_r, sin(i) * outer_r)
			glVertex2f(cos(i) * inner_r, sin(i) * inner_r)
			i += self._increment
			
		glVertex2f(cos(0) * outer_r, sin(0) * outer_r)
		glVertex2f(cos(0) * inner_r, sin(0) * inner_r)
		glEnd()
	"""
		
	def drawFilledCircle(self, radius):
		glBegin(GL_POLYGON)
		i = 0.0
		while i < pi * 2:
			glVertex2f(cos(i) * radius, sin(i) * radius)
			i += self._increment
		glEnd()
		
	def fill(self):
		self.drawFilledRectangle((0,0), self.size)
		
	@property
	def size(self):
		return Size(self.width, self.height)
		
	@property
	def viewport(self):
		v = Viewport()
		v.modelview = glGetDoublev(GL_MODELVIEW_MATRIX)
		#v.projection = glGetDoublev(GL_PROJECTION_MATRIX)
		v.viewport = glGetIntegerv(GL_VIEWPORT)
		return v
