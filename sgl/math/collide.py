from math import sqrt,pi,cos,sin
import vector

def polygons(poly1, poly2):
	return collide(poly1, poly2)

def circles(pos1, radius1, pos2, radius2):
	x1, y1 = pos1
	x2, y2 = pos2
	overlap = (radius1 + radius2) - sqrt((x2 - x1)**2 + (y2 - y1)**2)
	# TODO: return vector!
	return max(overlap, 0.0)
	
def polygon_point(poly, point):
	x, y = point
	n = len(poly)
	inside =False

	p1x,p1y = poly[0]
	for i in range(n+1):
		p2x,p2y = poly[i % n]
		if y > min(p1y,p2y):
			if y <= max(p1y,p2y):
				if x <= max(p1x,p2x):
					if p1y != p2y:
						xinters = (y-p1y)*(p2x-p1x)/(p2y-p1y)+p1x
					if p1x == p2x or x <= xinters:
						inside = not inside
		p1x,p1y = p2x,p2y

	return inside

def calculate_interval(poly, axis):
	min = max = vector.dot(poly[0], axis)

	for i in range(1, len(poly)):
		d = vector.dot(poly[i], axis)
		if (d < min):
			min = d
		elif (d > max):
			max = d
	return min, max
	
def simplecollide(poly1, poly2):
	if len(poly1) == 0 or len(poly2) == 0:
		return False
	def intervalsSeperated(mina, maxa, minb, maxb):
		return (mina > maxb or minb > maxa)
		
	def separatedByAxis(poly1, poly2, axis):
		mina, maxa = calculate_interval(poly1, axis)
		minb, maxb = calculate_interval(poly2, axis)
		return intervalsSeperated(mina, maxa, minb, maxb)
		
	# test separation axes of current polygon
	j = -1
	for i in range(0, len(poly1)):
		v0 = poly1[j]
		v1 = poly1[i]
		edge = vector.subtract(v1,v0) # v1 - v0
		axis = vector.perp(edge) # sep axis is perpendicular ot the edge

		if separatedByAxis(poly1, poly2, axis):
			return False
		j = i

	# test separation axes of other polygon
	j = -1
	for i in range(0, len(poly2)):
		v0 = poly2[j]
		v1 = poly2[i]
		edge = vector.subtract(v1,v0) # v1 - v0
		axis = vector.perp(edge) # sep axis is perpendicular ot the edge

		if separatedByAxis(poly1, poly2, axis):
			return False
		j = i
		
	return True
	
def collide(self, other):
	class CollisionInfo(object):
		def __init__(self):
			self.mtd = 0,0
			self.mtdLengthSquared = 0.0
			self.overlapped = False
			
	def separatedByAxis(self, other, axis, info):
		mina, maxa = calculate_interval(self, axis)
		minb, maxb = calculate_interval(other, axis)
		d0 = maxb - mina
		d1 = minb - maxa
		if d0 < 0.0 or d1 > 0.0:
			return True
		overlap = (d1, d0)[d0 < -d1]
		axis_length_squared = vector.dot(axis,axis)
		assert (axis_length_squared > 0.00001)
		
		ax, ay = axis
		m = overlap / axis_length_squared
		sep = ax * m, ay * m
		sep_length_squared = vector.dot(sep,sep)
		if sep_length_squared < info.mtdLengthSquared or info.mtdLengthSquared < 0.0:
			info.mtdLengthSquared = sep_length_squared
			info.mtd = sep
		return False
	
	if len(self) == 0 or len(other) == 0:
		return False
	info = CollisionInfo()
	info.mtdLengthSquared = -1.0
	j = -1
	for i in range(0, len(self)):
		v0 = self[j]
		v1 = self[i]
		edge = vector.subtract(v1,v0) # v1 - v0
		axis = vector.perp(edge) # sep axis is perpendicular ot the edge

		if separatedByAxis(self, other, axis, info):
			return None
		j = i

	# test separation axes of other polygon
	j = -1
	for i in range(0, len(other)):
		v0 = other[j]
		v1 = other[i]
		edge = vector.subtract(v1,v0) # v1 - v0
		axis = vector.perp(edge) # sep axis is perpendicular ot the edge

		if separatedByAxis(self, other, axis, info):
			return None
		j = i
	info.overlapped = True
	return info
	
def collide2(self, other, delta):
	class CollisionInfo(object):
		def __init__(self):
			# sweep results
			self.tenter = 0.0
			self.tleave = 0.0
			self.Nenter = 0,0
			self.Nleave = 0,0
			self.collided = False
			# overlap results
			self.mtd = 0,0
			self.mtdLengthSquared = 0.0
			self.overlapped = False
			
	def seperatedByAxis_overlap(self, axis, d0, d1, info):
		if not info.overlapped:
			return True
		if d0 < 0.0 or d1 > 0.0:
			info.overlapped = False
			return True
		overlap = (d1, d0)[d0 < -d1]
		axis_length_squared = vector.dot(axis, axis)
		assert (axis_length_squared > 0.00001)
		m = overlap / axis_length_squared
		sep = axis[0] * m, axis[1] * m
		sep_length_squared = vector.dot(sep,sep)
		if sep_length_squared < info.mtdLengthSquared or info.mtdLengthSquared < 0.0:
			info.mtdLengthSquared = sep_length_squared
			info.mtd = sep
		return False
	def seperatedByAxis_swept(self, axis, d0, d1, v, info):
		if not info.collided:
			return True
		if abs(v) < 0.0000001:
			return True
		N0 = axis
		N1 = vector.neg(axis)
		t0 = d0 / v
		t1 = d1 / v
		if t0 > t1:
			t0, t1 = t1, t0
			N0, N1 = N1, N0
			
		if t0 > 1.0 or t1 < 0.0:
			info.collided = False
			return True
		if info.tenter > info.tleave:
			info.tenter = t0
			info.tleave = t1
			info.Nenter = N0
			info.Nleave = N1
			return False
		else:
			if t0 > info.tleave or t1 < info.tenter:
				info.collided = False
				return True
			if t0 > info.tenter:
				info.tenter = t0
				info.Nenter = N0
			if t1 < info.tleave:
				info.tleave = t1
				info.Nleave = N1
			return False
	def separatedByAxis(self, other, axis, delta, info):
		mina, maxa = calculate_interval(self, axis)
		minb, maxb = calculate_interval(other, axis)
		d0 = maxb - mina
		d1 = minb - maxa
		v = vector.dot(axis,delta)
		sep_overlap = seperatedByAxis_overlap(self, axis, d0, d1, info)
		sep_swept = seperatedByAxis_swept(self, axis, d0, d1, v, info)
		return (sep_overlap and sep_swept)
	
	info = CollisionInfo()
	info.overlapped = True
	info.collided = True
	info.mtdLengthSquared = -1.0
	info.tenter - 1.0
	info.tleave = 0.0
	j = -1
	for i in range(0, len(self)):
		v0 = self[j]
		v1 = self[i]
		edge = vector.subtract(v1,v0) # v1 - v0
		axis = vector.perp(edge) # sep axis is perpendicular to the edge

		if separatedByAxis(self, other, axis, delta, info):
			return None
		j = i
	j = -1
	for i in range(0, len(other)):
		v0 = other[j]
		v1 = other[i]
		edge = vector.subtract(v1,v0) # v1 - v0
		axis = vector.perp(edge) # sep axis is perpendicular to the edge

		if separatedByAxis(self, other, axis, delta, info):
			return None
		j = i
	assert (info.overlapped or info.mtdLengthSquared >= 0.0)
	assert (info.collided or info.tenter <= info.tleave)
	
	# sanity checks?
	info.overlapped &= info.mtdLengthSquared >= 0.0
	info.collided &= info.tenter <= info.tleave
	
	info.Nenter = vector.normal(info.Nenter)
	info.Nleave = vector.normal(info.Nleave)
	return info
