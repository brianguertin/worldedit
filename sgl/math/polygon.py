from math import pi,cos,sin,degrees, acos
import vector

def transform(poly, vec, rot = 0):
	return [vector.transform(p, vec, rot) for p in poly]

def create(count, radius, pos = (0,0)):
	vertices = []
	for i in range(0, count):
		a = 2.0 * pi * (i / float(count))
		vertices.append((cos(a) * radius + pos[0], sin(a) * radius + pos[1]))
	return vertices
	
def isConvexPolygon(points):
	""" Check if set of points represents a convex polygon, must be counter-clockwise """
	if len(points) < 3:
		raise Exception('Polygon must contain at least 3 points')
	p1 = points[-2]
	p2 = points[-1]
	for p3 in points:
		a1, a2 = (p2 - p1).angle, (p3 - p2).angle
		if a2 < a1:
			a2 += pi*2
		diff = a2 - a1
		if diff > pi:
			return False
		p1 = p2
		p2 = p3
	return True
