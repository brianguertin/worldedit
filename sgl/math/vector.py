from __future__ import division
from math import cos,sin,hypot,atan2

class Vector(object): pass

def add(v1, v2):
	return v1[0] + v2[0], v1[1] + v2[1]
	
def subtract(v1, v2):
	return v1[0] - v2[0], v1[1] - v2[1]
	
def multiply(v1, v2):
	return v1[0] * v2[0], v1[1] * v2[1]
	
def divide(v1, v2):
	return v1[0] / v2[0], v1[1] / v2[1]
	
def neg(v):
	return -v[0], -v[1]
	
def scale(v, length):
	l = hypot(v[0],v[1])
	s = l / length
	return v[0] / s, v[1] / s
	
def rotate(v, angle):
	x = v[0] * cos(angle) - v[1] * sin(angle)
	y = v[0] * sin(angle) + v[1] * cos(angle)
	return x, y
	
def transform(v1, v2, rot):
	return add(rotate(v1,rot), v2)
	
def perp(v):
	return -v[1], v[0]

def dot(v1, v2):
	return (v1[0] * v2[0]) + (v1[1] * v2[1])
	
def cross(v1, v2):
	return (v1[0] * v2[1]) - (v1[1] * v2[0])
	
def length(v):
	return hypot(v[0], v[1])
	
def angle(v):
	return atan2(v[1], v[0])
	
def normal(v):
	l = length(v)
	if l == 0.0:
		return 0.0
	else:
		return v[0] / l, v[1] / l
	
