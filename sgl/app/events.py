
class StopEvent(Exception):
	pass
	
class EventBase(object):
	def stop(self):
		raise StopEvent
		
class DumbEvent(EventBase):
	def __init__(self, base):
		self.base = base
		
	def __getattr__(self, v):
		return getattr(self.base, v)
