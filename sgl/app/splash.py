import sgl.core.path as path

def do_splash(filename = None, text = 'Loading...'):
	import pygame, os
	os.environ['SDL_VIDEO_CENTERED'] = '1'
	pygame.mixer.pre_init(44100, -16, 2, 4096)
	pygame.init()
	if filename:
		img = pygame.image.load(path.find(filename))
		screen = pygame.display.set_mode(img.get_size(),pygame.NOFRAME)
		screen.blit(img, (0,0))
	else:
		pygame.font.init()
		screen = pygame.display.set_mode((500,80),pygame.NOFRAME)
		background = pygame.Surface(screen.get_size())
		background.fill((2,24,244))
		screen.blit(background, (0,0))
		screen.blit(pygame.font.Font(None, 72).render(text, 1, (255,255,255)), (90,10))
	pygame.display.update()
