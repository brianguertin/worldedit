from sgl.video.font import Font
#from shlex import split

class Console(object):
	""" Console
	"""
	def __init__(self):
		self.font = Font('sans', size=14)
		self.lines = []
		self.textures = []
		self.hidden = True
		self.commands = {} # map of command strings to 2-tuples of (function, args)
		
	def log(self, msg):
		self.lines.append(msg)
		tex = self.font.render(msg)
		self.textures.append(tex)
		if len(self.textures) > 10:
			self.textures.pop(0)
		print '** ', msg
		
	"""
		
	def bind(self, cmd, func, args=None):
		self.commands[cmd] = (func, args)
		
	def do_string(self, cmd_string):
		parts = split(cmd_string)
		cmd = parts[0]
		if cmd in self.commands:
			fn, arg_types = self.commands[cmd]
			args = parts[1:]
			
			if len(args) != len(arg_types):
				self.log('Command "%s" expects %s arguments (%s given)' % (cmd, len(arg_types), len(args)))
				
			else:
				args = [cls(val) for cls, val in zip(arg_types, args)]
				fn(*args)
		else:
			self.log('Unrecognized command: "%s"' % cmd)
	"""
		
	def draw(self, d):
		if self.hidden:
			return
		d.pushMatrix()
		
		d.pushOrtho(left=0, right=d.width, top=0, bottom=d.height)
		
		d.color((1,1,1))
		
		for t in self.textures:
			d.pushMatrix()
			d.translate(t.size / 2)
			t.draw()
			d.popMatrix()
			d.translate((0, t.size.y))
			
		d.popOrtho()
			
		d.popMatrix()
