import pygame as _pygame
from pygame import locals as _locals

class Key:
	@classmethod
	def repeat(cls, enabled):
		if enabled:
			cls.repeat_enable()
		else:
			cls.repeat_disable()
			
	@staticmethod
	def repeat_enable():
		_pygame.key.set_repeat(500,40)
		
	@staticmethod
	def repeat_disable():
		_pygame.key.set_repeat()
		
	BACKSPACE = _locals.K_BACKSPACE
	TAB = _locals.K_TAB
	CLEAR = _locals.K_CLEAR
	RETURN = _locals.K_RETURN
	PAUSE = _locals.K_PAUSE
	ESCAPE = _locals.K_ESCAPE
	SPACE = _locals.K_SPACE
	EXCLAIM = _locals.K_EXCLAIM
	QUOTEDBL = _locals.K_QUOTEDBL
	HASH = _locals.K_HASH
	DOLLAR = _locals.K_DOLLAR
	AMPERSAND = _locals.K_AMPERSAND
	QUOTE = _locals.K_QUOTE
	LEFTPAREN = _locals.K_LEFTPAREN
	RIGHTPAREN = _locals.K_RIGHTPAREN
	ASTERISK = _locals.K_ASTERISK
	PLUS = _locals.K_PLUS
	COMMA = _locals.K_COMMA
	MINUS = _locals.K_MINUS
	PERIOD = _locals.K_PERIOD
	SLASH = _locals.K_SLASH
	_0 = _locals.K_0
	_1 = _locals.K_1
	_2 = _locals.K_2
	_3 = _locals.K_3
	_4 = _locals.K_4
	_5 = _locals.K_5
	_6 = _locals.K_6
	_7 = _locals.K_7
	_8 = _locals.K_8
	_9 = _locals.K_9
	COLON = _locals.K_COLON
	SEMICOLON = _locals.K_SEMICOLON
	LESS = _locals.K_LESS
	EQUALS = _locals.K_EQUALS
	GREATER = _locals.K_GREATER
	QUESTION = _locals.K_QUESTION
	AT = _locals.K_AT
	LEFTBRACKET = _locals.K_LEFTBRACKET
	BACKSLASH = _locals.K_BACKSLASH
	RIGHTBRACKET = _locals.K_RIGHTBRACKET
	CARET = _locals.K_CARET
	UNDERSCORE = _locals.K_UNDERSCORE
	BACKQUOTE = _locals.K_BACKQUOTE
	A = _locals.K_a
	B = _locals.K_b
	C = _locals.K_c
	D = _locals.K_d
	E = _locals.K_e
	F = _locals.K_f
	G = _locals.K_g
	H = _locals.K_h
	I = _locals.K_i
	J = _locals.K_j
	K = _locals.K_k
	L = _locals.K_l
	M = _locals.K_m
	N = _locals.K_n
	O = _locals.K_o
	P = _locals.K_p
	Q = _locals.K_q
	R = _locals.K_r
	S = _locals.K_s
	T = _locals.K_t
	U = _locals.K_u
	V = _locals.K_v
	W = _locals.K_w
	X = _locals.K_x
	Y = _locals.K_y
	Z = _locals.K_z
	DELETE = _locals.K_DELETE
	KP0 = _locals.K_KP0
	KP1 = _locals.K_KP1
	KP2 = _locals.K_KP2
	KP3 = _locals.K_KP3
	KP4 = _locals.K_KP4
	KP5 = _locals.K_KP5
	KP6 = _locals.K_KP6
	KP7 = _locals.K_KP7
	KP8 = _locals.K_KP8
	KP9 = _locals.K_KP9
	KP_PERIOD = _locals.K_KP_PERIOD
	KP_DIVIDE = _locals.K_KP_DIVIDE
	KP_MULTIPLY = _locals.K_KP_MULTIPLY
	KP_MINUS = _locals.K_KP_MINUS
	KP_PLUS = _locals.K_KP_PLUS
	KP_ENTER = _locals.K_KP_ENTER
	KP_EQUALS = _locals.K_KP_EQUALS
	UP = _locals.K_UP
	DOWN = _locals.K_DOWN
	RIGHT = _locals.K_RIGHT
	LEFT = _locals.K_LEFT
	INSERT = _locals.K_INSERT
	HOME = _locals.K_HOME
	END = _locals.K_END
	PAGEUP = _locals.K_PAGEUP
	PAGEDOWN = _locals.K_PAGEDOWN
	F1 = _locals.K_F1
	F2 = _locals.K_F2
	F3 = _locals.K_F3
	F4 = _locals.K_F4
	F5 = _locals.K_F5
	F6 = _locals.K_F6
	F7 = _locals.K_F7
	F8 = _locals.K_F8
	F9 = _locals.K_F9
	F10 = _locals.K_F10
	F11 = _locals.K_F11
	F12 = _locals.K_F12
	F13 = _locals.K_F13
	F14 = _locals.K_F14
	F15 = _locals.K_F15
	NUMLOCK = _locals.K_NUMLOCK
	CAPSLOCK = _locals.K_CAPSLOCK
	SCROLLOCK = _locals.K_SCROLLOCK
	RSHIFT = _locals.K_RSHIFT
	LSHIFT = _locals.K_LSHIFT
	RCTRL = _locals.K_RCTRL
	LCTRL = _locals.K_LCTRL
	RALT = _locals.K_RALT
	LALT = _locals.K_LALT
	RMETA = _locals.K_RMETA
	LMETA = _locals.K_LMETA
	LSUPER = _locals.K_LSUPER
	RSUPER = _locals.K_RSUPER
	MODE = _locals.K_MODE
	HELP = _locals.K_HELP
	PRINT = _locals.K_PRINT
	SYSREQ = _locals.K_SYSREQ
	BREAK = _locals.K_BREAK
	MENU = _locals.K_MENU
	POWER = _locals.K_POWER
	EURO = _locals.K_EURO
	
class KeyMod:
	LSHIFT = _locals.KMOD_LSHIFT
	RSHIFT = _locals.KMOD_RSHIFT
	SHIFT = _locals.KMOD_SHIFT
	CAPS =  _locals.KMOD_CAPS
	LCTRL = _locals.KMOD_LCTRL
	RCTRL = _locals.KMOD_RCTRL
	CTRL = _locals.KMOD_CTRL
	LALT = _locals.KMOD_LALT
	RALT = _locals.KMOD_RALT
	ALT = _locals.KMOD_ALT
	LMETA = _locals.KMOD_LMETA
	RMETA = _locals.KMOD_RMETA
	META = _locals.KMOD_META
	NUM = _locals.KMOD_NUM
	MODE = _locals.KMOD_MODE
	
class Event:
	from pygame.locals import QUIT, KEYDOWN, KEYUP, MOUSEMOTION, MOUSEBUTTONUP, MOUSEBUTTONDOWN, JOYAXISMOTION, JOYBALLMOTION, JOYHATMOTION, JOYBUTTONUP, JOYBUTTONDOWN, VIDEORESIZE, VIDEOEXPOSE, USEREVENT
	
class Mouse:
	LEFT = 1
	MIDDLE = 2
	RIGHT = 3
	WHEELUP = 4
	WHEELDOWN = 5
