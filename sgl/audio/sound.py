import pygame
from pygame.mixer import Sound as _Sound
from sgl.core import path

ENABLED = True

class Sound(object):
	_cache = {}
	_channels = {}
	def __new__(cls, filename):
		f = path.find(filename)
		if f in Sound._cache:
			return Sound._cache[f]
		else:
			self = object.__new__(Sound)
			log('Loading: "%s"' % f, type='Sound')
			self._init(f)
			Sound._cache[f] = self
			return self
	
	def _init(self, filename):
		self.sound = _Sound(filename)
		
	reload = _init
		
	def play(self, channel=None):
		if not ENABLED:
			return
		if channel:
			if channel in Sound._channels:
				ch = Sound._channels[channel]
				ch.play(self.sound)
			else:
				Sound._channels[channel] = self.sound.play()
		else:
			return self.sound.play()
		
