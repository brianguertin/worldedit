from pygame.mixer import music as _music
from sgl.core import path

def play(filename):
	_music.load(path.find(filename))
	_music.play()
