import math
		
class Point(list):
	""" Represents a 2D Point, implemented as a python list with a length of 2. """
	def __init__(self, x=0, y=0):
		super(Point, self).__init__((0,0))
		try:
			self.x = x[0]
			self.y = x[1]
		except:
			try:
				self.x = x.x
				self.y = x.y
			except:
				try:
					self.x = x
					self.y = y
				except:
					self.x, self.y = x.strip('()').split(',')
					
	def copy(self):
		return Point(self)
		
	def set(self, x, y):
		self.x = x
		self.y = y
				
	def get_x(self):
		return self[0]
		
	def get_y(self):
		return self[1]
		
	def set_x(self, v):
		self[0] = float(v)
		
	def set_y(self, v):
		self[1] = float(v)
		
	x = property(get_x, set_x)
	y = property(get_y, set_y)
			
	#def __getitem__(self, key):
	#	return (self.x, self.y)[bool(key)]
			
	def __str__(self):
		return '(%s, %s)' % (self.x, self.y)
		
	def __repr__(self):
		return '<Point (%s, %s)>' % (self.x, self.y)
	
	def get_length(self):
		return math.hypot(self.x, self.y)
		
	def set_length(self, v):
		p = self.scale(v)
		self[0] = p[0]
		self[1] = p[1]
		
	length = property(get_length, set_length)
		
	@property
	def angle(self):
		return math.atan2(self.y, self.x)
		
	@property
	def normal(self):
		l = self.length
		if l == 0:
			return Point(0,0)
		else:
			return Point(self.x / l, self.y / l)
			
	@property
	def aspect(self):
		if self.y == 0:
			return 0
		else:
			return self.x / self.y
			
	@property
	def floor(self):
		return Point(math.floor(self.x), math.floor(self.y))
		
	@property
	def ceil(self):
		return Point(math.ceil(self.x), math.ceil(self.y))
			
	def dot(self, other):
		return (self.x * other.x + self.y * other.y)
			
	def distance(self, other):
		return (other - self).length
		
	def scale(self, l):
		return self.normal * l
		
	def rotate(self, r):
		return Point.fromAngle(self.angle + r, length=self.length)
		
	"""
	def __getitem__(self, key):
		if key == 0:
			return self.x
		elif key == 1:
			return self.y
			
	def __setitem__(self, key, value):
		if key == 0:
			self.x = value
		elif key == 1:
			self.y = value
	"""
			
	def __neg__(self):
		return Point(-self.x, -self.y)
			
	def __add__(self, o):
		p = Point(o)
		return Point(self.x + p.x, self.y + p.y)
		
	def __sub__(self, o):
		p = Point(o)
		return Point(self.x - p.x, self.y - p.y)
		
	def __mul__(self, v):
		return Point(self.x * v, self.y * v)
		
	def __div__(self, v):
		return Point(self.x / v, self.y / v)
	__floordiv__ = __truediv__ = __div__
	
	def __iadd__(self, o):
		p = Point(o)
		self[0] += p.x
		self[1] += p.y
		return self
		
	@staticmethod
	def fromAngle(a, length=None):
		p = Point(math.cos(a), math.sin(a))
		if length:
			return p.scale(length)
		else:
			return p

Size = Point
Vertex = Point
Vector = Point

class Color(object):
	def __init__(self, r=0, g=0, b=0):
		try:
			self.r = float(r[0])
			self.g = float(r[1])
			self.b = float(r[2])
		except:
			try:
				self.r = float(r.r)
				self.g = float(r.g)
				self.b = float(r.b)
			except:
				self.r = float(r)
				self.g = float(g)
				self.b = float(b)
			
	def __str__(self):
		return '(%s, %s, %s)' % (self.r, self.g, self.b)
		
	def __repr__(self):
		return '<Color (%s, %s, %s)>' % (self.r, self.g, self.b)
			
	def __getitem__(self, key):
		if key == 0:
			return self.r
		if key == 1:
			return self.g
		if key == 2:
			return self.b
		else:
			raise IndexError('list index out of range (Color)')
			
class AABB(list):
	def __init__(self):
		super(AABB, self).__init__([Point(), Point()])
		
	def get_lowerBound(self):
		return self[0]
		
	def set_lowerBound(self, v):
		self[0] = Point(v)
		
	def get_upperBound(self):
		return self[1]
		
	def set_upperBound(self, v):
		self[1] = Point(v)
			
	lowerBound = property(get_lowerBound, set_lowerBound)
	upperBound = property(get_upperBound, set_upperBound)
	
class Rect(object):
	def __init__(self, position = (0,0), size = (0,0)):
		self.x, self.y = Point(position)
		self.w, self.h = Point(size)
		
	def __str__(self):
		return '(%s, %s, %s, %s)' % (self.x, self.y, self.w, self.h)
		
	def __repr__(self):
		return '<Rect (%s, %s, %s, %s)>' % (self.x, self.y, self.w, self.h)
		
	def contains(self, p):
		p = Point(p)
		return p.x >= self.x and p.y >= self.y and p.x <= self.x + self.w and p.y <= self.y + self.h
		
	def get_position(self):
		return Point(self.x, self.y)
		
	def set_position(self, p):
		self.x, self.y = p
		
	def get_size(self):
		return Point(self.w, self.h)
		
	def set_size(self, p):
		self.w, self.h = p
		
	position = property(get_position, set_position)
	size = property(get_size, set_size)
