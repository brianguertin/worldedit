
class enum(object):
	def __init__(self, s, start = 0, object=None):
		obj = object or self.__dict__
		for num, val in enumerate(s.split()):
			val = val.strip()
			if val:
				obj[val] = num+start
