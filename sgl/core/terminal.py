import os

class TerminalBase(object):
	_instance = None
	def __init__(self):
		if TerminalBase._instance:
			raise Exception('Only one terminal object can be created')
		TerminalBase._instance = self
		
	def __del__(self):
		TerminalBase._instance = None
		
	@property
	def events(self):
		while self.hit():
			yield self.get()
			
	def get(self):
		return None
		
	def hit(self):
		return False
		
	def write(self, text):
		sys.stdout.write(text)
		
	def flush(self):
		sys.stdout.flush()

if os.name == 'nt':
	import msvcrt
	
	class Terminal(TerminalBase):
		def get(self):
			return msvcrt.getch()
			
		def hit(self):
			return msvcrt.kbhit()
			
		def clear(self):
			os.system('cls')
else:
	import sys, select, tty, termios, atexit
	
	class Terminal(TerminalBase):
		
		def __init__(self):
			super(Terminal, self).__init__()
			
			self.old_settings = termios.tcgetattr(sys.stdin)
			tty.setcbreak(sys.stdin.fileno())
			atexit.register(self._reset)
			
		def get(self):
			return sys.stdin.read(1)
				
		def hit(self):
			return select.select([sys.stdin], [], [], 0) == ([sys.stdin], [], [])
				
		def clear(self):
			os.system('clear')
			
		def _reset(self):
			import sys, termios
			termios.tcsetattr(sys.stdin, termios.TCSADRAIN, self.old_settings)

if __name__ == '__main__':
	import string
	""" Echos back keys pressed. Quits on escape """
	t = Terminal()
	while 1:
		c = t.get()
		if c:
			if c == '\x1b':	 # x1b is ESC
				break
			elif c in string.printable:
				print c
