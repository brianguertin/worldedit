
def compile_literal(text, src='<literal>', target='exec'):
	# Remove extra indentation
	indent = 0
	lines = text.replace('\r\n', '\n').split('\n')
	for line in lines:
		l = len(line.rstrip())
		if l:
			indent = l - len(line.strip())
			break
	if indent:
		lines = [line[indent:] for line in lines]
	text = '\n'.join(lines)
	
	# Compile
	return compile(text, src, target)
