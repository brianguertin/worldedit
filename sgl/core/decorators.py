
def coerce(*types):
	def deco(fn):
		def inner(*args, **kargs):
			args = [t(a) for t, a in zip(types, args)]
			fn(*args, **kargs)
		return inner
	return deco
	
class _Deferred(object):
	def __init__(self, fn):
		self.fn = fn
		
	def __call__(self):
		if not hasattr(self, 'value'):
			self.value = self.fn()
			del self.fn
		return self.value
		
def deferred(fn):
	return _Deferred(fn)
	
	
	
