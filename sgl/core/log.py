from sgl.core.signals import Signal

onLog = Signal()

_level = 2
_print = None
try:
	_print = eval("print") # python 3.0 case
except SyntaxError:
	try:
		D = dict()
		exec("from __future__ import print_function\np=print", D)
		_print = D["p"] # 2.6 case
		del D
	except:
		import sys
		def Print(*args, **kwd): # 2.4, 2.5, define our own Print function
			fout = kwd.get("file", sys.stdout)
			w = fout.write
			if args:
				w(str(args[0]))
				sep = kwd.get("sep", " ")
				for a in args[1:]:
					w(sep)
					w(str(a))
			w(kwd.get("end", "\n"))
		_print = Print
		
g_enabled = set()
g_disabled = set()

def log(msg, type=None, level=2):
	onLog.emit(msg, type, level)
	if type and type in g_disabled:
		return
	if level > _level:
		return
	if level == 0:
		msg = '** ERROR ** %s' % msg
	if level == 1:
		msg = '** WARNING ** %s' % msg
	if type:
		_print('[%s] %s' % (type, msg))
	else:
		_print(msg)
		
def enable(*types):
	global g_enabled
	for t in types:
		g_enabled.add(t)
		
def disable(*types):
	global g_disabled
	for t in types:
		g_disabled.add(t)
		
def set_logger(fn):
	global _print
	_print = fn
	
def set_level(lvl):
	global _level
	_level = lvl
