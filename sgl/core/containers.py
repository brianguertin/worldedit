import collections

KEY, PREV, NEXT = range(3)

class OrderedSet(collections.MutableSet):

	def __init__(self, iterable=None):
		self.end = end = [] 
		end += [None, end, end]		 # sentinel node for doubly linked list
		self.map = {}				   # key --> [key, prev, next]
		if iterable is not None:
			self |= iterable

	def __len__(self):
		return len(self.map)

	def __contains__(self, key):
		return key in self.map

	def add(self, key):
		if key not in self.map:
			end = self.end
			curr = end[PREV]
			curr[NEXT] = end[PREV] = self.map[key] = [key, curr, end]

	def discard(self, key):
		if key in self.map:		
			key, prev, next = self.map.pop(key)
			prev[NEXT] = next
			next[PREV] = prev

	def __iter__(self):
		end = self.end
		curr = end[NEXT]
		while curr is not end:
			yield curr[KEY]
			curr = curr[NEXT]

	def __reversed__(self):
		end = self.end
		curr = end[PREV]
		while curr is not end:
			yield curr[KEY]
			curr = curr[PREV]

	def pop(self, last=True):
		if not self:
			raise KeyError('set is empty')
		key = next(reversed(self)) if last else next(iter(self))
		self.discard(key)
		return key

	def __repr__(self):
		if not self:
			return '%s()' % (self.__class__.__name__,)
		return '%s(%r)' % (self.__class__.__name__, list(self))

	def __eq__(self, other):
		if isinstance(other, OrderedSet):
			return len(self) == len(other) and list(self) == list(other)
		return not self.isdisjoint(other)

	def __del__(self):
		self.clear()					# remove circular references


class AttrDict(dict):
	""" A regular dict in which contents can be accessed as attributes """
	def __init__(self, *args, **kargs):
		dict.__init__(self, *args, **kargs)
		
	def __getattr__(self, name):
		return self[name]
		
	def __setattr__(self, name, val):
		self[name] = val
		
class TwoWayDict(dict): 
	def __init__(self, inverse = None):
		if inverse is not None:
			self.inverse = inverse
		else:
			self.inverse = TwoWayDict(self)
		
	def __setitem__(self, key, value):
		try:
			oldval = self[key]
			del self.inverse[oldval]
		except KeyError:
			pass
		dict.__setitem__(self, key, value)
		dict.__setitem__(self.inverse, value, key)
		
	def __delitem__(self, index):
		value = self[index]
		dict.__delitem__(self, index)
		dict.__delitem__(self.inverse, value)
		
	def key4value(self, v):
		return self.__inverse_mapping[v]

class UniqueIndexList(object):
	def __init__(self, *args, **kargs):
		self.map = TwoWayDict()
		self.uid_count = 0
		
	def uid(self, index):
		return self.map.inverse[index]
		
	def __repr__(self):
		return repr(self.map.values())
		
	def __getitem__(self, index):
		return self.map[index]
		
	def __setitem__(self, index, value):
		self.map[index] = value
		
	def __delitem__(self, index):
		del self.map[index]
	
	def __iter__(self):
		return iter(self.map.values())
		
	def add(self, value):
		uid = self.uid_count + 1
		self.map[uid] = value
		self.uid_count = uid
		return uid
		
	def remove(self, value):
		del self.map.inverse[value]
		
	def items(self):
		return self.map.items()
