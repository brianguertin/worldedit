import sgl.core.path as path
from sgl.core import xml

class CacheError(Exception):
	pass

class StaticCache(object):
	cache = {}
		
	@classmethod
	def load_objects(cls, source):
		p = path.find(source)
		x = xml.read(p) #xml.ElementTree(file=p)
		for xE in x.findall(cls.__name__.lower()):
			name = xE.get('name')
			if not name:
				continue
			try:
				obj = object.__new__(cls)
				if hasattr(obj, 'init'):
					obj.init()
				if hasattr(obj, 'load'):
					obj.load(xE)
				
				cls.cache[name] = obj
				log('Loaded: %s' % name, type=cls.__name__)
			except Exception, e:
				log('%s error: %s' % (name, e), type=cls.__name__)
			
	def __new__(cls, name):
		try:
			return cls.cache[name]
		except KeyError:
			raise CacheError('%s %s not found' % (name, cls.__name__))

class DynamicCache(object):
	def __init__(self, dir = '.'):
		self.cache = {}
		self.dir = dir
		if not isinstance(self.dir, basestring):
			self.dir = path.join(*self.dir)

	def new(self, cls, name):
		if name in self.cache:
			return self.cache[name]
		else:
			filename = path.find(path.join(self.dir, '%s.xml' % name))
			
			log('Loading: %s' % name, type=cls.__name__)
			
			obj = object.__new__(cls)
			if hasattr(obj, 'init'):
				obj.init()
			if hasattr(obj, 'load'):
				x = xml.read(filename)
				obj.load(x)
			
			self.cache[name] = obj
			return obj
			
Cache = DynamicCache # deprecated
