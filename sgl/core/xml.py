from __future__ import absolute_import
import xml.etree.ElementTree as xml
from xml.etree.ElementTree import Element, ElementTree, _ElementInterface
from sgl.core import path

def read(obj):
	""" Takes filename, raw xml, or Element and always returns an Element """
	if xml.iselement(obj):
		return obj
	elif obj.lstrip().startswith('<') and obj.rstrip().endswith('>'):
		return xml.fromstring(obj)
	else:
		return xml.ElementTree(file=path.find(obj)).getroot()

def _stringify(e):
	for k, v in e.attrib.copy().items():
		if not isinstance(v, basestring):
			if v.__class__ == bool:
				v = ('false','true')[v]
			e.set(k, str(v))
	for child in e.getchildren():
		_stringify(child)

def _attr(self, var):
	v = self.get(var)
	if v:
		return v
	else:
		el = self.find(var)
		if el is not None:
			return el.text
			
def _query(self, obj, var, attr=None, inverse=False):
	v = self.attr(var)
	if v is not None:
		t = getattr(obj, attr or var).__class__
		if t == bool:
			v = (False,True)[v.lower() in ['1','true','yes']]
			if inverse:
				v = not v
		if t in (tuple,list,dict):
			v = eval(v)
		setattr(obj, attr or var, t(v))
		
def _record(self, object, var, attr=None, inverse=False):
	value = getattr(object, attr or var)
	if value.__class__ == bool and inverse:
		value = not value
	self.set(var, value)
		
def _makeelement(self, name, attrs=None):
	return self._makeelement(name, attrs or {})
	
def _append(self, obj):
	if obj is self:
		raise Exception('You cannot add an element to itself')
	else:
		self._append(obj)
		
def _get(self, attr):
	el, sep, at = attr.partition('.')
	if sep:
		x = self.find(el)
		if x is not None:
			return x.get(at)
	else:
		return self._get(el)
		
def _set(self, attr, val):
	el, sep, at = attr.partition('.')
	if sep:
		x = self.find(el)
		if x is None:
			x = Element(el)
			self.append(x)
		return x.set(at, val)
	else:
		return self._set(el, val)

_ElementInterface._get = _ElementInterface.get
_ElementInterface.get = _get
_ElementInterface._set = _ElementInterface.set
_ElementInterface.set = _set
	
_ElementInterface.query = _query
_ElementInterface.record = _record
_ElementInterface.attr = _attr
_ElementInterface.cleanup = _stringify

_ElementInterface._makeelement = _ElementInterface.makeelement
_ElementInterface.makeelement = _makeelement

_ElementInterface._append = _ElementInterface.append
_ElementInterface.append = _append
