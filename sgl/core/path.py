import os, sys
from os import makedirs
from os.path import dirname, basename, join, isdir, isfile, expanduser, normpath, exists

os.chdir(dirname(sys.argv[0]) or '.')

g_dirs = ['.', 'data']

def listdir(path, ext=''):
	r = []
	for p in findall(path):
		if isdir(p):
			r += [join(p,f) for f in os.listdir(p) if f.endswith(ext)]
	return _unique(map(normpath, r))

def find(path):
	if os.path.exists(path):
		return path
	for dir in reversed(g_dirs):
		p = os.path.join(dir, path)
		if os.path.exists(p):
			return normpath(p)
	else:
		raise Exception('File "%s" not found, searched %s' % (path, g_dirs))
		
def findall(path):
	r = []
	for dir in reversed(g_dirs):
		p = os.path.join(dir, path)
		if os.path.exists(p):
			r.append(normpath(p))
	return _unique(r)

def add(dir):
	g_dirs.append(dir)
	
def add_user_dir(dir):
	if os.name not in ['nt','windows']:
		dir = '.%s' % dir
	add(join(expanduser('~'), dir))
	
def pop():
	g_dirs.pop()
	
	
def _unique(seq): # f10 but simpler
	# Order preserving
	return list(_unique_gen(seq))

def _unique_gen(seq):
	seen = set()
	for x in seq:
		if x in seen:
			continue
		seen.add(x)
		yield x
		
def relative(src, dest=None):
	""" Return a relative path from src to dest(default=cwd).

	>>> base_path_relative("/usr/bin", "/tmp/foo/bar")
	../../tmp/foo/bar

	>>> base_path_relative("/usr/bin", "/usr/lib")
	../lib

	>>> base_path_relative("/tmp", "/tmp/foo/bar")
	foo/bar
	"""
	from os import getcwd, sep 
	from os.path import abspath

	if dest is None:
		dest = getcwd()

	srclist = abspath(src).split(sep)
	destlist = abspath(dest).split(sep)
	loc = [spath == dpath for spath, dpath in zip(srclist, destlist)].index(False)
	rellist = ([ ".." ] * (len(srclist) - loc)) + destlist[loc:]
	return sep.join(rellist)
