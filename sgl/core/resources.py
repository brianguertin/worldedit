from sgl.core import path
from pyinotify import *

class ResourceMonitor:
	def __init__(self, cls, dir='.'):
		self.cls = cls
		self.dir = dir
		self.wm = WatchManager()
		self.wd = self.wm.add_watch(dir, EventsCodes.ALL_FLAGS['IN_CLOSE_WRITE'], rec=True)
		self.notifier = Notifier(self.wm, self.handle_update, timeout=5)
		
	def handle_update(self, r):
		p = path.join(r.path, r.name)
		if p in self.cls._cache:
			self.cls._cache[p].reload(p)

	def update(self):
		if self.notifier.check_events():
			self.notifier.read_events()
			self.notifier.process_events()
