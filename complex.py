#!/usr/bin/env python
from __future__ import division
from sgl.app.splash import do_splash
do_splash('splash.png')

from sgl.app.app import App, Event, Key, Mouse, KeyMod
from sgl.video.texture import Texture
from sgl.game.world import World, ActorNotFound
from sgl.game.actor import ActorDef
from sgl.gui.button import Button
from sgl.gui.theme import Theme
from sgl.gui.image import Image
from sgl.gui.label import Label
from sgl.gui.panel import Panel
from sgl.gui.box import Box, VBox, HBox
from sgl.gui.controller import Controller as GuiController
from sgl.gui.listbox import ListBox
from sgl.gui.canvas import Canvas
from sgl.core import path
from sgl.game.controller import Controller
from sgl.core.resources import ResourceMonitor
from sgl.core.tuples import *
import xml.etree.ElementTree as xml

from OpenGL.GL import *
from OpenGL.GLU import *
from math import pi, cos, sin

SELECT_RANGE = 0.2

class Tool(object):
	def __init__(self):
		pass
	def input(self, e, world):
		pass
	def draw(self, d, world):
		pass
	def reset(self):
		pass
		
class EntityTool(Tool):
	def __init__(self, world):
		super(EntityTool, self).__init__()
		self.name = 'Entities'
		self.selectedActor = self.hoveredActor = None
		self.dragging = False
		
		self.entityList = ListBox()
		path.add('entities')
		for f in path.listdir('entities', ext='.xml'):
			try:
				df = world.loadActorDef(f)
				widget = None
				if df.thumb:
					widget = HBox()
					widget.add(Image(df.thumb))
					widget.add(Label(df.name))
				self.entityList.add(df.name or path.basename(f), df, widget=widget)
			except Exception, e:
				log('ERROR: Failed loading ActorDef "%s": %s' % (f,e), type='Main')
		for name, adef in world.actordefs.items():
			if name.startswith('@'):
				self.entityList.add(name[1:], adef)
				
		self.widget = self.entityList
		
	def input(self, e, world):
		if e.type == Event.KEYDOWN:
			k = e.key
			if k == Key.DELETE:
				if self.selectedActor:
					self.selectedActor.destroy()
					self.selectedActor = self.hoveredActor = None
			else:
				return False
		if e.type == Event.MOUSEBUTTONDOWN:
			b = e.button
			if b == Mouse.LEFT:
				# Select actor
				p = world.view.unProject(e.pos)
				for actor in world.actors:
					try:
						pos = actor.get('position')
						if p.distance(pos) < SELECT_RANGE:
							self.selectedActor = actor
							self.dragging = True
							break
					except AttributeError:
						pass
				
				else:
					self.selectedActor = None
			elif b == Mouse.RIGHT:
				if self.entityList.value:
					info = world.spawn(self.entityList.value)
					info.set('position', world.view.unProject(e.pos))
					world.doSpawns()
			else:
				return False
		
		elif e.type == Event.MOUSEBUTTONUP:
			if e.button == Mouse.LEFT:
				self.dragging = False
			else:
				return False
				
		elif e.type == Event.MOUSEMOTION:
			if self.dragging:
				p = self.selectedActor.get('position')
				self.selectedActor.set('position', p + world.view.scale(e.rel))#(e.rel[0] / self.z, -e.rel[1] / self.z))
			
			p = world.view.unProject(e.pos)
			for actor in world.actors:
				try:
					pos = actor.get('position')
					if p.distance(pos) < SELECT_RANGE:
						self.hoveredActor = actor
						break
				except AttributeError:
					pass
			else:
				self.hoveredActor = None
			return False
		else:
			return False
		return True
		
	def draw(self, d, world):
		if self.selectedActor:
			d.color((0, 1, 0))
			d.drawCircle(SELECT_RANGE, self.selectedActor.get('position'))
		
		if self.hoveredActor and self.hoveredActor != self.selectedActor:
			d.color((0, 0, 1))
			d.drawCircle(SELECT_RANGE, self.hoveredActor.get('position'))
			
	def reset(self):
		self.selectedActor = self.hoveredActor = None
		self.dragging = False
		
class TileTool(Tool):
	def __init__(self, world):
		super(TileTool, self).__init__()
		self.name = 'Tiles'
		self.dragging = False
		self.erasing = False
		self.selection = None
		self.selecting = False
		self.selectStart = (0,0)
		self.selectEnd = (0,0)
		self.target = (0,0)
		
		self.widget = ListBox()
		self.widget.onSelect(self.clearSelection)
		if world.tilemap and world.tilemap.tilesets and world.tilemap.layers:
			for i, tile in zip(range(len(world.tilemap.tilesets[0].tiles)), world.tilemap.tilesets[0].tiles):
				id = (0,i)
				widget = HBox()
				widget.add(Image(tile.texture))
				widget.add(Label(str(id)))
				self.widget.add(str(id), value=id, widget=widget)
		
	def input(self, e, world):
		if e.type == Event.MOUSEBUTTONDOWN:
			if e.button == Mouse.LEFT:
				if e.mod & KeyMod.CTRL:
					self.selecting = True
					p = world.view.unProject(e.pos)
					x, y = world.tilemap.locate(p)
					self.selectStart = (x, y+1)
					self.selectEnd = (x+1,y)
				else:
					self.paint(e.pos, world)
					self.dragging = True
			elif e.button == Mouse.RIGHT:
				self.erase(e.pos, world)
				self.erasing = True
			else:
				return False
		elif e.type == Event.MOUSEBUTTONUP:
			if e.button == Mouse.LEFT:
				if self.selecting:
					self.saveSelection(world)
					self.selecting = False
				self.dragging = False
			elif e.button == Mouse.RIGHT:
				self.erasing = False
			else:
				return False
		elif e.type == Event.MOUSEMOTION:
			p = world.view.unProject(e.pos)
			self.target = world.tilemap.locate(p)
			if self.selecting:
				x, y = self.target
				if x >= self.selectStart[0]:
					x += 1
				if y >= self.selectStart[1]:
					y += 1
				self.selectEnd = (x, y)
			elif self.dragging:
				self.paint(e.pos, world)
			elif self.erasing:
				self.erase(e.pos, world)
			else:
				return False
		else:
			return False
		return True
		
	def draw(self, d, world):
		cw, ch = world.tilemap.cellwidth, world.tilemap.cellheight
		d.pushAlpha(0.6)
		d.pushMatrix()
		if self.selecting:
			d.drawRectangle((self.selectStart[0] * cw, self.selectStart[1] * ch), (self.selectEnd[0] * cw, self.selectEnd[1] * ch))
		elif self.selection:
			d.translate((self.target[0] * cw, self.target[1] * ch))
			
			d.color((1,1,1))
			tilemap = world.tilemap
			d.pushMatrix()
			for row in self.selection:
				d.pushMatrix()
				for tile in row:
					if tile:
						tex = tilemap.tilesets[tile[0]].tiles[tile[1]].texture
						h_ratio = cw / tex.width
						size = tex.size * h_ratio
						d.pushMatrix()
						d.translate((cw/2, size.y/2))
						d.drawTextureStretched(tex, size)
						d.popMatrix()
					d.translate((cw, 0))
				d.popMatrix()
				d.translate((0,ch))
			d.popMatrix()
			d.color((0,1,0))
			d.drawRectangle((0,0), (cw * len(self.selection[0]),ch * len(self.selection)))
		else:
			d.translate((self.target[0] * cw, self.target[1] * ch))
			v = self.widget.value
			if v:
				d.color((1,1,1))
				tex = world.tilemap.tilesets[v[0]].tiles[v[1]].texture
				h_ratio = cw / tex.width
				size = tex.size * h_ratio
				d.pushMatrix()
				# TODO: simplify?
				d.translate((cw/2, size.y/2))
				d.drawTextureStretched(tex, size)
				d.popMatrix()
				
			layer = world.tilemap.layers[0]
			if self.target[1] < len(layer) and self.target[0] < len(layer[self.target[1]]):
				d.color((0,1,0))
			else:
				d.color((1,0,0))
			d.drawRectangle((0,0), (cw,ch))
		d.popMatrix()
		d.popAlpha()
		
	def saveSelection(self, world):
		self.selection = []
		xmin = min(self.selectStart[0], self.selectEnd[0])
		xmax = max(self.selectStart[0], self.selectEnd[0])
		ymin = min(self.selectStart[1], self.selectEnd[1])
		ymax = max(self.selectStart[1], self.selectEnd[1])
		
		layer = world.tilemap.layers[0]
		for y in range(ymin, ymax):
			row = []
			for x in range(xmin, xmax):
				if y < len(layer) and x < len(layer[y]) and layer[y][x]:
					row.append(layer[y][x])
				else:
					row.append(None)
			self.selection.append(row)
			
	def clearSelection(self):
		self.selection = None
		
	def reset(self):
		self.selection = None
		self.selecting = False
		self.dragging = False
		self.erasing = False
		
	def getCollisionActor(self, world):
		try:
			return world.getActorById('_tilemap')
		except:
			adef = ActorDef()
			adef.world = world
			s = '<actordef><component type="Physics"></component></actordef>'
			adef.load(xml.fromstring(s))
			info = world.spawn(adef)
			info.set('id', '_tilemap')
			a = world.doSpawn()
			return a
		
	def erase(self, pos, world):
		p = world.view.unProject(pos)
		pos = world.tilemap.locate(p)
		layer = world.tilemap.layers[0]
		self.eraseTile(pos, layer, world)
		
	def eraseTile(self, pos, layer, world):
		x, y = pos
		if y < len(layer) and x < len(layer[y]) and layer[y][x]:
			tile = layer[y][x]
			layer[y][x] = None
			
			if world.tilemap.tilesets[tile[0]].tiles[tile[1]].isSolid:
				a = self.getCollisionActor(world)
				a.do('remove_body_by_id', '%s:%s' % (x,y))
			
	def placeTile(self, pos, tile, layer, world):
		x, y = pos
		if y < len(layer) and x < len(layer[y]) and layer[y][x] != tile:
			self.eraseTile(pos, layer, world)
			layer[y][x] = tile
			
			tile = world.tilemap.tilesets[tile[0]].tiles[tile[1]]
			if tile.isSolid:
				id = '%s:%s' % (x,y)
				
				cellwidth = world.tilemap.cellwidth
				cellheight = world.tilemap.cellheight
				
				x1 = x * cellwidth
				y1 = y * cellheight
				vertices = ''
				
				if tile.vertices:
					for x,y in tile.vertices:
						vertices += '<vertex x="%s" y="%s" />' % (x1 + x * cellwidth, y1 + y * cellheight)
					
				else:
					x2 = x1 + cellwidth
					y2 = y1 + cellheight
					vertices = """
						<vertex x="%(x1)s" y="%(y1)s" />
						<vertex x="%(x2)s" y="%(y1)s" />
						<vertex x="%(x2)s" y="%(y2)s" />
						<vertex x="%(x1)s" y="%(y2)s" />
					""" % {'x1':x1,'x2':x2,'y1':y1,'y2':y2}
					
				s = """
				<body id="%(id)s" dynamic="false" isPlatform="%(platform)s">
					<polygon friction="0.8" restitution="0.05">
						%(vertices)s
					</polygon>
				</body>
				""" % {'id':id, 'vertices':vertices, 'platform':(0,1)[tile.isPlatform]}
				
				a = self.getCollisionActor(world)
				a.do('add_body_from_xml', xml.fromstring(s))
		
	def paint(self, pos, world):
		layer = world.tilemap.layers[0]
		p = world.view.unProject(pos)
		x, y = world.tilemap.locate(p)
		if self.selection:
			for row_num, row in zip(range(len(self.selection)), self.selection):
				for col_num, tile in zip(range(len(row)), row):
					p = (x + col_num, y + row_num)
					if tile:
						self.placeTile(p, tile, layer, world)
					else:
						self.eraseTile(p, layer, world)
		else:
			v = self.widget.value
			if v:
				self.placeTile((x,y), v, layer, world)
		

class Basic:
	def runner(self):
		self.run()
		while 1:
			yield
			self.app.run(self.main_loop)
			
	def update_hud(self):
		log('HP: %s' % self.a.get('hp'))
		
	def tool_motion(self, e):
		if self.panning:
			self.camera = (self.camera[0] - e.rel[0] / self.z, self.camera[1] + e.rel[1] / self.z)
		self.tool.input(e, self.w)
						
	def tool_activate(self, e):
		b = e.button
		if b == Mouse.MIDDLE:
			self.panning = True
		elif b == Mouse.WHEELUP:
			self.z *= 1.1
		elif b == Mouse.WHEELDOWN:
			self.z *= 0.9
		else:
			self.tool.input(e, self.w)
		
	def tool_deactivate(self, e):
		b = e.button
		if b == Mouse.MIDDLE:
			self.panning = False
		else:
			self.tool.input(e, self.w)
			
	def tool_keydown(self, e):
		self.tool.input(e, self.w)
		
	def tool_keyup(self, e):
		self.tool.input(e, self.w)
			
	def select_tool(self):
		self.tool = self.toolMenu.value
		self.tool.reset()
		self.gui.clear()
		self.gui.add(self.tool.widget)
		self.gui.pack()
			
	def run(self, world='world.xml'):
		self.resmon = ResourceMonitor(Texture)
		self.app = App()
		self.app.options.find('display').set('width', 800)
		self.app.options.find('display').set('height', 600)
		self.app.init()
		self.selectedActor = self.hoveredActor = None
		self.camera = (0,0)
		self.panning = False
		self.dragging = False
		self.tool = None
		self.tools = []
		self.canvas = Canvas(650,600)
		self.canvas.onMouseMotion(self.tool_motion)
		self.canvas.onMouseDown(self.tool_activate)
		self.canvas.onMouseUp(self.tool_deactivate)
		self.canvas.onKeyDown(self.tool_keydown)
		self.canvas.onKeyUp(self.tool_keyup)
		
		self.load(world)
		
		self.z = 115
		
		Theme.Global = self.theme = Theme('sans')
		
		self.gui = VBox()
		
		self.tool = EntityTool(self.w)
		self.tools.append(self.tool)
		self.tools.append(TileTool(self.w))
		
		self.toolMenu = toolMenu = ListBox()
		for t in self.tools:
			toolMenu.add(t.name, t)
		toolMenu.onSelect(self.select_tool)
		toolMenu.size = 150, 60
		toolMenu.x = self.app.display.width - toolMenu.width
		toolMenu.pack()
		
		path.pop()
		#self.gui.add(self.tool.widget)
		#self.gui.x, self.gui.y = 50, 50
		self.gui.size = 150, 450
		self.gui.x = self.app.display.width - self.gui.width
		self.gui.y = 80
		self.gui.pack()
		
		menu = HBox()
		menu.add(Button('Load', onClick=self.load))
		menu.add(Button('Save', onClick=self.save))
		menu.size = 150, 30
		menu.x = self.app.display.width - menu.width
		menu.y = self.app.display.height - menu.height
		menu.pack()
		
		panel = Panel()
		panel.size = self.app.display.size
		panel.add(self.gui)
		panel.add(menu)
		panel.add(self.canvas)
		panel.add(toolMenu)
		self.root = GuiController(panel)
		self.panel = panel
		
		self.app.run(self.main_loop)
		
	def load(self, world='world_saved.xml'):
		self.w = World(world)
		self.a = self.w.getActorById('player')
		self.a.isDestroyable = False
		self.a.connect('heal', self.update_hud)
		self.a.connect('damage', self.update_hud)
		self.controller = Controller(self.a)
		
	def save(self):
		self.w.save('world_saved.xml')
		
	def game_loop(self, frame):
		for e in frame.events:
			if not self.controller.input(e):
				if e.type == Event.QUIT:
					frame.exit = True
				elif e.type == Event.KEYDOWN:
					k = e.key
					if k in [Key.ESCAPE, Key.P]:
						frame.exit = True
				elif e.type == Event.MOUSEBUTTONDOWN:
					b = e.button
					if b == Mouse.WHEELUP:
						self.z *= 1.1
					elif b == Mouse.WHEELDOWN:
						self.z *= 0.9
				
		self.w.update(frame.ticks)
		self.camera = self.a.get('position')
		
		self.w.draw(self.app.display, camera=self.camera, scale=self.z)
		
	def main_loop(self, frame):
		# TODO: seperate logic for editing and testing (input handling especially)
		self.resmon.update()
		for e in frame.events:
			if not self.root.input(e):
				if e.type == Event.QUIT:
					frame.exit = True
				elif e.type == Event.KEYDOWN:
					k = e.key
					if k == Key.ESCAPE:
						frame.exit = True
					elif k == Key.P:
						self.app.run(self.game_loop)
						self.tool.reset()
					elif k == Key.I:
						log('camera %s scaled %s' % (self.camera, self.z), level=0)

		# Render to canvas
		with self.canvas.context as c:
			# Clear
			c.color((0,0,0))
			c.fill()
			
			# Draw world
			self.w.draw(c, camera=self.camera, scale=self.z)
			
			# Draw tool
			c.pushMatrix()
			self.w.view.transform(c)
			self.tool.draw(c, self.w)
			c.popMatrix()
		
		# Render interface
		self.theme.drawInterface(self.panel, self.app.display)

if __name__ == '__main__':
	try:
		import psyco
		psyco.full()
	except ImportError:
		pass
	import sys
	app = Basic()
	if len(sys.argv) > 1:
		app.run(sys.argv[1])
	else:
		app.run()
else:
	b = Basic()
	r = b.runner()
